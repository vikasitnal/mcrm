'use strict';

/**
 * @ngdoc overview
 * @name profilerApp
 * @description
 * # profilerApp
 *
 * Main module of the application.
 */
angular
  .module('profilerApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap',
    'ui.sortable',
    'angularResizable',
    'nvd3',
    'ng-mfb',
    'lbServices',
    'dndLists',
    'angularjs-dropdown-multiselect',
    'ui.tree',
    'ng-scroll-bar',
    'ngMaterial',
    'ngMessages',
    'md.data.table',
    'ngJSONPath',
	'gm.datepickerMultiSelect',
	'angular.drag.resize'

  ]).filter('capitalize', function() {
  return function(input, scope) {
    if (input!=null){
      input = input.toLowerCase();
      return input.substring(0,1).toUpperCase()+input.substring(1);
    }
  }
})
  .config(["$routeProvider", "$stateProvider", "$urlRouterProvider", "$mdAriaProvider", function ($routeProvider, $stateProvider, $urlRouterProvider, $mdAriaProvider ) {
    $mdAriaProvider.disableWarnings();

      $urlRouterProvider.otherwise('/?page=1&query=&siteId=10&userId=1');
      $stateProvider
          .state('home', {
              url: '/home/{id}/{accountId}',
              templateUrl: 'views/main.html',
              controller:'MainCtrl',
              controllerAs: 'main'
          })
          .state('admin', {
              url: '/admin?siteId&userId',
              templateUrl: 'views/admin.html',
              controller:'AdminCtrl',
              controllerAs: 'admin'
          })
          .state('home.tabs', {
              url: '/tabs?siteId&userId&type',
              templateUrl: 'views/tabs.html',
              controller:'TabsCtrl',
              controllerAs: 'tabs'
          })
          .state('search', {
              url: '/?query&page&siteId&userId',
              templateUrl: 'views/search.html',
              controller:'SearchCtrl',
              controllerAs: 'search'
          });


  }])
  .run(["$rootScope", "$state", "Settings", "Tab", "Widget", function($rootScope, $state, Settings, Tab, Widget) {
     $rootScope.search = function(query, page){
       console.log(query, page);
      $state.go('search', {query: query, page: page});
     }

     $rootScope.saveSettings = function(){
        Settings.prototype$updateAttributes($rootScope.settings.id, $rootScope.settings, function(settings){
          $rootScope.settings = settings;
        });
     }

     $rootScope.savetab = function(tab){
      console.log("save tab")
       if(tab.id)
       {

       Tab.prototype$updateAttributes(tab.id,tab , function(tab){
       });
       }
       else
       {
          tab.settingsId = $rootScope.settings.id;
       Tab.create(tab, function(tab){
         tab.widgets = [];
         $rootScope.alltabs.push(tab);
       });
       }

     }

     $rootScope.saveWidget = function(widget){
       if(widget.id){
         Widget.prototype$updateAttributes(widget.id, widget, function(widget){
           console.log('widget saved');
         });
       } else {
         widget.tabId = $rootScope.alltabs[$rootScope.tabindex].id;
         widget.order = $rootScope.alltabs[$rootScope.tabindex].widgets.length;
         Widget.create(widget, function(widget){
           $rootScope.alltabs[$rootScope.tabindex].widgets.push(widget);
         });
       }
     }


  }]);

// CommonJS package manager support
if (typeof module !== 'undefined' && typeof exports !== 'undefined' &&
  module.exports === exports) {
  // Export the *name* of this Angular module
  // Sample usage:
  //
  //   import lbServices from './lb-services';
  //   angular.module('app', [lbServices]);
  //
  module.exports = "lbServices";
}

(function(window, angular, undefined) {
  'use strict';

  var urlBase = "/api";
  var authHeader = 'authorization';

  function getHost(url) {
    var m = url.match(/^(?:https?:)?\/\/([^\/]+)/);
    return m ? m[1] : null;
  }
  // need to use the urlBase as the base to handle multiple
  // loopback servers behind a proxy/gateway where the host
  // would be the same.
  var urlBaseHost = getHost(urlBase) ? urlBase : location.host;

/**
 * @ngdoc overview
 * @name lbServices
 * @module
 * @description
 *
 * The `lbServices` module provides services for interacting with
 * the models exposed by the LoopBack server via the REST API.
 *
 */
  var module = angular.module("lbServices", ['ngResource']);

/**
 * @ngdoc object
 * @name lbServices.User
 * @header lbServices.User
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `User` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "User",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/Users/:id",
          { 'id': '@id' },
          {

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$__findById__accessTokens
             * @methodOf lbServices.User
             *
             * @description
             *
             * Find a related item by id for accessTokens.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - User id
             *
             *  - `fk` – `{*}` - Foreign key for accessTokens
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "prototype$__findById__accessTokens": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/Users/:id/accessTokens/:fk",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$__destroyById__accessTokens
             * @methodOf lbServices.User
             *
             * @description
             *
             * Delete a related item by id for accessTokens.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - User id
             *
             *  - `fk` – `{*}` - Foreign key for accessTokens
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "prototype$__destroyById__accessTokens": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/Users/:id/accessTokens/:fk",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$__updateById__accessTokens
             * @methodOf lbServices.User
             *
             * @description
             *
             * Update a related item by id for accessTokens.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - User id
             *
             *  - `fk` – `{*}` - Foreign key for accessTokens
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "prototype$__updateById__accessTokens": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/Users/:id/accessTokens/:fk",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$__get__accessTokens
             * @methodOf lbServices.User
             *
             * @description
             *
             * Queries accessTokens of User.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - User id
             *
             *  - `filter` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "prototype$__get__accessTokens": {
              isArray: true,
              url: urlBase + "/Users/:id/accessTokens",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$__create__accessTokens
             * @methodOf lbServices.User
             *
             * @description
             *
             * Creates a new instance in accessTokens of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - User id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "prototype$__create__accessTokens": {
              url: urlBase + "/Users/:id/accessTokens",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$__delete__accessTokens
             * @methodOf lbServices.User
             *
             * @description
             *
             * Deletes all accessTokens of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - User id
             *
             *  - `where` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "prototype$__delete__accessTokens": {
              url: urlBase + "/Users/:id/accessTokens",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$__count__accessTokens
             * @methodOf lbServices.User
             *
             * @description
             *
             * Counts accessTokens of User.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - User id
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "prototype$__count__accessTokens": {
              url: urlBase + "/Users/:id/accessTokens/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#create
             * @methodOf lbServices.User
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/Users",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#upsert
             * @methodOf lbServices.User
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "upsert": {
              url: urlBase + "/Users",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#replaceOrCreate
             * @methodOf lbServices.User
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/Users/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#upsertWithWhere
             * @methodOf lbServices.User
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/Users/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#exists
             * @methodOf lbServices.User
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/Users/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#findById
             * @methodOf lbServices.User
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/Users/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#replaceById
             * @methodOf lbServices.User
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/Users/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#find
             * @methodOf lbServices.User
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/Users",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#findOne
             * @methodOf lbServices.User
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/Users/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#updateAll
             * @methodOf lbServices.User
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/Users/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#deleteById
             * @methodOf lbServices.User
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/Users/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#count
             * @methodOf lbServices.User
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/Users/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$updateAttributes
             * @methodOf lbServices.User
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - User id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "prototype$updateAttributes": {
              url: urlBase + "/Users/:id",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#createChangeStream
             * @methodOf lbServices.User
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/Users/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#login
             * @methodOf lbServices.User
             *
             * @description
             *
             * Login a user with username/email and password.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `include` – `{string=}` - Related objects to include in the response. See the description of return value for more details.
             *   Default value: `user`.
             *
             *  - `rememberMe` - `boolean` - Whether the authentication credentials
             *     should be remembered in localStorage across app/browser restarts.
             *     Default: `true`.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * The response body contains properties of the AccessToken created on login.
             * Depending on the value of `include` parameter, the body may contain additional properties:
             *   - `user` - `U+007BUserU+007D` - Data of the currently logged in user. (`include=user`)
             *
             */
            "login": {
              params: {
                include: 'user',
              },
              interceptor: {
                response: function(response) {
                  var accessToken = response.data;
                  LoopBackAuth.setUser(
                    accessToken.id, accessToken.userId, accessToken.user);
                  LoopBackAuth.rememberMe =
                    response.config.params.rememberMe !== false;
                  LoopBackAuth.save();
                  return response.resource;
                },
              },
              url: urlBase + "/Users/login",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#logout
             * @methodOf lbServices.User
             *
             * @description
             *
             * Logout a user with access token.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `access_token` – `{string=}` - Do not supply this argument, it is automatically extracted from request headers.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "logout": {
              interceptor: {
                response: function(response) {
                  LoopBackAuth.clearUser();
                  LoopBackAuth.clearStorage();
                  return response.resource;
                },
                responseError: function(responseError) {
                  LoopBackAuth.clearUser();
                  LoopBackAuth.clearStorage();
                  return responseError.resource;
                },
              },
              url: urlBase + "/Users/logout",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#confirm
             * @methodOf lbServices.User
             *
             * @description
             *
             * Confirm a user registration with email verification token.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `uid` – `{string}` -
             *
             *  - `token` – `{string}` -
             *
             *  - `redirect` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "confirm": {
              url: urlBase + "/Users/confirm",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#resetPassword
             * @methodOf lbServices.User
             *
             * @description
             *
             * Reset password for a user with email.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "resetPassword": {
              url: urlBase + "/Users/reset",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#createMany
             * @methodOf lbServices.User
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/Users",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#getCurrent
             * @methodOf lbServices.User
             *
             * @description
             *
             * Get data of the currently logged user. Fail with HTTP result 401
             * when there is no user logged in.
             *
             * @param {function(Object,Object)=} successCb
             *    Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *    `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             */
            'getCurrent': {
              url: urlBase + "/Users" + '/:id',
              method: 'GET',
              params: {
                id: function() {
                  var id = LoopBackAuth.currentUserId;
                  if (id == null) id = '__anonymous__';
                  return id;
                },
              },
              interceptor: {
                response: function(response) {
                  LoopBackAuth.currentUserData = response.data;
                  return response.resource;
                },
                responseError: function(responseError) {
                  LoopBackAuth.clearUser();
                  LoopBackAuth.clearStorage();
                  return $q.reject(responseError);
                },
              },
              __isGetCurrentUser__: true,
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.User#patchOrCreate
             * @methodOf lbServices.User
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R["patchOrCreate"] = R["upsert"];

            /**
             * @ngdoc method
             * @name lbServices.User#updateOrCreate
             * @methodOf lbServices.User
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R["updateOrCreate"] = R["upsert"];

            /**
             * @ngdoc method
             * @name lbServices.User#patchOrCreateWithWhere
             * @methodOf lbServices.User
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.User#update
             * @methodOf lbServices.User
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.User#destroyById
             * @methodOf lbServices.User
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.User#removeById
             * @methodOf lbServices.User
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$patchAttributes
             * @methodOf lbServices.User
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - User id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R["prototype$patchAttributes"] = R["prototype$updateAttributes"];

        /**
         * @ngdoc method
         * @name lbServices.User#getCachedCurrent
         * @methodOf lbServices.User
         *
         * @description
         *
         * Get data of the currently logged user that was returned by the last
         * call to {@link lbServices.User#login} or
         * {@link lbServices.User#getCurrent}. Return null when there
         * is no user logged in or the data of the current user were not fetched
         * yet.
         *
         * @returns {Object} A User instance.
         */
        R.getCachedCurrent = function() {
          var data = LoopBackAuth.currentUserData;
          return data ? new R(data) : null;
        };

        /**
         * @ngdoc method
         * @name lbServices.User#isAuthenticated
         * @methodOf lbServices.User
         *
         * @returns {boolean} True if the current user is authenticated (logged in).
         */
        R.isAuthenticated = function() {
          return this.getCurrentId() != null;
        };

        /**
         * @ngdoc method
         * @name lbServices.User#getCurrentId
         * @methodOf lbServices.User
         *
         * @returns {Object} Id of the currently logged-in user or null.
         */
        R.getCurrentId = function() {
          return LoopBackAuth.currentUserId;
        };

        /**
        * @ngdoc property
        * @name lbServices.User#modelName
        * @propertyOf lbServices.User
        * @description
        * The name of the model represented by this $resource,
        * i.e. `User`.
        */
        R.modelName = "User";



        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Settings
 * @header lbServices.Settings
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Settings` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Settings",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/settings/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Settings.account() instead.
            "prototype$__get__account": {
              url: urlBase + "/settings/:id/account",
              method: "GET",
            },

            // INTERNAL. Use Settings.tabs.findById() instead.
            "prototype$__findById__tabs": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/settings/:id/tabs/:fk",
              method: "GET",
            },

            // INTERNAL. Use Settings.tabs.destroyById() instead.
            "prototype$__destroyById__tabs": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/settings/:id/tabs/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Settings.tabs.updateById() instead.
            "prototype$__updateById__tabs": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/settings/:id/tabs/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Settings.tabs() instead.
            "prototype$__get__tabs": {
              isArray: true,
              url: urlBase + "/settings/:id/tabs",
              method: "GET",
            },

            // INTERNAL. Use Settings.tabs.create() instead.
            "prototype$__create__tabs": {
              url: urlBase + "/settings/:id/tabs",
              method: "POST",
            },

            // INTERNAL. Use Settings.tabs.destroyAll() instead.
            "prototype$__delete__tabs": {
              url: urlBase + "/settings/:id/tabs",
              method: "DELETE",
            },

            // INTERNAL. Use Settings.tabs.count() instead.
            "prototype$__count__tabs": {
              url: urlBase + "/settings/:id/tabs/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#create
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/settings",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#upsert
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
            "upsert": {
              url: urlBase + "/settings",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#replaceOrCreate
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/settings/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#upsertWithWhere
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/settings/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#exists
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/settings/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#findById
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/settings/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#replaceById
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/settings/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#find
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/settings",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#findOne
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/settings/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#updateAll
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/settings/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#deleteById
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/settings/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#count
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/settings/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#prototype$updateAttributes
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - settings id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
            "prototype$updateAttributes": {
              url: urlBase + "/settings/:id",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#createChangeStream
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/settings/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#reset
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "reset": {
              url: urlBase + "/settings/:id/reset",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#revert
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "revert": {
              url: urlBase + "/settings/:id/revert",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Settings#createMany
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/settings",
              method: "POST",
            },

            // INTERNAL. Use Account.settings.findById() instead.
            "::findById::Account::settings": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/accounts/:id/settings/:fk",
              method: "GET",
            },

            // INTERNAL. Use Account.settings.destroyById() instead.
            "::destroyById::Account::settings": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/accounts/:id/settings/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Account.settings.updateById() instead.
            "::updateById::Account::settings": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/accounts/:id/settings/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Account.settings() instead.
            "::get::Account::settings": {
              isArray: true,
              url: urlBase + "/accounts/:id/settings",
              method: "GET",
            },

            // INTERNAL. Use Account.settings.create() instead.
            "::create::Account::settings": {
              url: urlBase + "/accounts/:id/settings",
              method: "POST",
            },

            // INTERNAL. Use Account.settings.createMany() instead.
            "::createMany::Account::settings": {
              isArray: true,
              url: urlBase + "/accounts/:id/settings",
              method: "POST",
            },

            // INTERNAL. Use Account.settings.destroyAll() instead.
            "::delete::Account::settings": {
              url: urlBase + "/accounts/:id/settings",
              method: "DELETE",
            },

            // INTERNAL. Use Account.settings.count() instead.
            "::count::Account::settings": {
              url: urlBase + "/accounts/:id/settings/count",
              method: "GET",
            },

            // INTERNAL. Use Tab.settings() instead.
            "::get::Tab::settings": {
              url: urlBase + "/tabs/:id/settings",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Settings#patchOrCreate
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
        R["patchOrCreate"] = R["upsert"];

            /**
             * @ngdoc method
             * @name lbServices.Settings#updateOrCreate
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
        R["updateOrCreate"] = R["upsert"];

            /**
             * @ngdoc method
             * @name lbServices.Settings#patchOrCreateWithWhere
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Settings#update
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Settings#destroyById
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Settings#removeById
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Settings#prototype$patchAttributes
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - settings id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
        R["prototype$patchAttributes"] = R["prototype$updateAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Settings#modelName
        * @propertyOf lbServices.Settings
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Settings`.
        */
        R.modelName = "Settings";


            /**
             * @ngdoc method
             * @name lbServices.Settings#account
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Fetches belongsTo relation account.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - settings id
             *
             *  - `refresh` – `{boolean=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
        R.account = function() {
          var TargetResource = $injector.get("Account");
          var action = TargetResource["::get::Settings::account"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Settings.tabs
     * @header lbServices.Settings.tabs
     * @object
     * @description
     *
     * The object `Settings.tabs` groups methods
     * manipulating `Tab` instances related to `Settings`.
     *
     * Call {@link lbServices.Settings#tabs Settings.tabs()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Settings#tabs
             * @methodOf lbServices.Settings
             *
             * @description
             *
             * Queries tabs of settings.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - settings id
             *
             *  - `filter` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
        R.tabs = function() {
          var TargetResource = $injector.get("Tab");
          var action = TargetResource["::get::Settings::tabs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Settings.tabs#count
             * @methodOf lbServices.Settings.tabs
             *
             * @description
             *
             * Counts tabs of settings.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - settings id
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.tabs.count = function() {
          var TargetResource = $injector.get("Tab");
          var action = TargetResource["::count::Settings::tabs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Settings.tabs#create
             * @methodOf lbServices.Settings.tabs
             *
             * @description
             *
             * Creates a new instance in tabs of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - settings id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
        R.tabs.create = function() {
          var TargetResource = $injector.get("Tab");
          var action = TargetResource["::create::Settings::tabs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Settings.tabs#createMany
             * @methodOf lbServices.Settings.tabs
             *
             * @description
             *
             * Creates a new instance in tabs of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - settings id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
        R.tabs.createMany = function() {
          var TargetResource = $injector.get("Tab");
          var action = TargetResource["::createMany::Settings::tabs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Settings.tabs#destroyAll
             * @methodOf lbServices.Settings.tabs
             *
             * @description
             *
             * Deletes all tabs of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - settings id
             *
             *  - `where` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.tabs.destroyAll = function() {
          var TargetResource = $injector.get("Tab");
          var action = TargetResource["::delete::Settings::tabs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Settings.tabs#destroyById
             * @methodOf lbServices.Settings.tabs
             *
             * @description
             *
             * Delete a related item by id for tabs.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - settings id
             *
             *  - `fk` – `{*}` - Foreign key for tabs
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.tabs.destroyById = function() {
          var TargetResource = $injector.get("Tab");
          var action = TargetResource["::destroyById::Settings::tabs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Settings.tabs#findById
             * @methodOf lbServices.Settings.tabs
             *
             * @description
             *
             * Find a related item by id for tabs.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - settings id
             *
             *  - `fk` – `{*}` - Foreign key for tabs
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
        R.tabs.findById = function() {
          var TargetResource = $injector.get("Tab");
          var action = TargetResource["::findById::Settings::tabs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Settings.tabs#updateById
             * @methodOf lbServices.Settings.tabs
             *
             * @description
             *
             * Update a related item by id for tabs.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - settings id
             *
             *  - `fk` – `{*}` - Foreign key for tabs
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
        R.tabs.updateById = function() {
          var TargetResource = $injector.get("Tab");
          var action = TargetResource["::updateById::Settings::tabs"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Account
 * @header lbServices.Account
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Account` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Account",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/accounts/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Account.settings.findById() instead.
            "prototype$__findById__settings": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/accounts/:id/settings/:fk",
              method: "GET",
            },

            // INTERNAL. Use Account.settings.destroyById() instead.
            "prototype$__destroyById__settings": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/accounts/:id/settings/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Account.settings.updateById() instead.
            "prototype$__updateById__settings": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/accounts/:id/settings/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Account.settings() instead.
            "prototype$__get__settings": {
              isArray: true,
              url: urlBase + "/accounts/:id/settings",
              method: "GET",
            },

            // INTERNAL. Use Account.settings.create() instead.
            "prototype$__create__settings": {
              url: urlBase + "/accounts/:id/settings",
              method: "POST",
            },

            // INTERNAL. Use Account.settings.destroyAll() instead.
            "prototype$__delete__settings": {
              url: urlBase + "/accounts/:id/settings",
              method: "DELETE",
            },

            // INTERNAL. Use Account.settings.count() instead.
            "prototype$__count__settings": {
              url: urlBase + "/accounts/:id/settings/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#create
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/accounts",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#upsert
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "upsert": {
              url: urlBase + "/accounts",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#replaceOrCreate
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/accounts/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#upsertWithWhere
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/accounts/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#exists
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/accounts/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#findById
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/accounts/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#replaceById
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/accounts/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#find
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/accounts",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#findOne
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/accounts/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#updateAll
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/accounts/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#deleteById
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/accounts/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#count
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/accounts/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#prototype$updateAttributes
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - account id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "prototype$updateAttributes": {
              url: urlBase + "/accounts/:id",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#createChangeStream
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/accounts/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#search
             * @methodOf lbServices.Account
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "search": {
              url: urlBase + "/accounts/:id/search",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#searchusers
             * @methodOf lbServices.Account
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "searchusers": {
              url: urlBase + "/accounts/:id/searchusers",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#enable
             * @methodOf lbServices.Account
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "enable": {
              url: urlBase + "/accounts/enable",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#savetoken
             * @methodOf lbServices.Account
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "savetoken": {
              url: urlBase + "/accounts/savetoken",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#accountBasedAnalytics
             * @methodOf lbServices.Account
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{string=}` -
             *
             *  - `contactId` – `{string=}` -
             *
             *  - `source` – `{string=}` -
             *
             *  - `object` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "accountBasedAnalytics": {
              url: urlBase + "/accounts/:id/accountBasedAnalytics/:source/:object/:contactId",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#accountBasedContacts
             * @methodOf lbServices.Account
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{string=}` -
             *
             *  - `contactId` – `{string=}` -
             *
             *  - `source` – `{string=}` -
             *
             *  - `object` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "accountBasedContacts": {
              url: urlBase + "/accounts/:id/accountBasedContacts/:source/:object/:contactId",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#secondarydata
             * @methodOf lbServices.Account
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{string=}` -
             *
             *  - `contactId` – `{string=}` -
             *
             *  - `source` – `{string=}` -
             *
             *  - `object` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "secondarydata": {
              url: urlBase + "/accounts/:id/secondarydata/:source/:object/:contactId",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#chartdata
             * @methodOf lbServices.Account
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{string=}` -
             *
             *  - `source` – `{string=}` -
             *
             *  - `object` – `{string=}` -
             *
             *  - `key` – `{string=}` -
             *
             *  - `value` – `{string=}` -
             *
             *  - `groupby` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "chartdata": {
              url: urlBase + "/accounts/:id/chartdata/:source/:object/:key/:value/:groupby",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#secondaryproperty
             * @methodOf lbServices.Account
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{string=}` -
             *
             *  - `source` – `{string=}` -
             *
             *  - `object` – `{string=}` -
             *
             *  - `key` – `{string=}` -
             *
             *  - `value` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "secondaryproperty": {
              url: urlBase + "/accounts/:id/secondaryproperty/:source/:object/:key/:value",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#findcontact
             * @methodOf lbServices.Account
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{string=}` -
             *
             *  - `contactId` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "findcontact": {
              url: urlBase + "/accounts/:id/findcontact/:contactId",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#eloquaobjectfields
             * @methodOf lbServices.Account
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{string=}` -
             *
             *  - `object` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "eloquaobjectfields": {
              url: urlBase + "/accounts/:id/eloqua/objectfields/:object",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#contactfields
             * @methodOf lbServices.Account
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{string=}` -
             *
             *  - `objname` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "contactfields": {
              url: urlBase + "/accounts/:id/eloqua/contactfields/:objname",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#salesforcecontactfields
             * @methodOf lbServices.Account
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "salesforcecontactfields": {
              url: urlBase + "/accounts/:id/salesforce/contactfields",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#salesforceobjectfields
             * @methodOf lbServices.Account
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{string=}` -
             *
             *  - `object` – `{string=}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "salesforceobjectfields": {
              url: urlBase + "/accounts/:id/salesforce/:object/fields",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Account#createMany
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/accounts",
              method: "POST",
            },

            // INTERNAL. Use Settings.account() instead.
            "::get::Settings::account": {
              url: urlBase + "/settings/:id/account",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Account#patchOrCreate
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
        R["patchOrCreate"] = R["upsert"];

            /**
             * @ngdoc method
             * @name lbServices.Account#updateOrCreate
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
        R["updateOrCreate"] = R["upsert"];

            /**
             * @ngdoc method
             * @name lbServices.Account#patchOrCreateWithWhere
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Account#update
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Account#destroyById
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Account#removeById
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Account#prototype$patchAttributes
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - account id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Account` object.)
             * </em>
             */
        R["prototype$patchAttributes"] = R["prototype$updateAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Account#modelName
        * @propertyOf lbServices.Account
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Account`.
        */
        R.modelName = "Account";

    /**
     * @ngdoc object
     * @name lbServices.Account.settings
     * @header lbServices.Account.settings
     * @object
     * @description
     *
     * The object `Account.settings` groups methods
     * manipulating `Settings` instances related to `Account`.
     *
     * Call {@link lbServices.Account#settings Account.settings()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Account#settings
             * @methodOf lbServices.Account
             *
             * @description
             *
             * Queries settings of account.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - account id
             *
             *  - `filter` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
        R.settings = function() {
          var TargetResource = $injector.get("Settings");
          var action = TargetResource["::get::Account::settings"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Account.settings#count
             * @methodOf lbServices.Account.settings
             *
             * @description
             *
             * Counts settings of account.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - account id
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.settings.count = function() {
          var TargetResource = $injector.get("Settings");
          var action = TargetResource["::count::Account::settings"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Account.settings#create
             * @methodOf lbServices.Account.settings
             *
             * @description
             *
             * Creates a new instance in settings of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - account id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
        R.settings.create = function() {
          var TargetResource = $injector.get("Settings");
          var action = TargetResource["::create::Account::settings"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Account.settings#createMany
             * @methodOf lbServices.Account.settings
             *
             * @description
             *
             * Creates a new instance in settings of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - account id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
        R.settings.createMany = function() {
          var TargetResource = $injector.get("Settings");
          var action = TargetResource["::createMany::Account::settings"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Account.settings#destroyAll
             * @methodOf lbServices.Account.settings
             *
             * @description
             *
             * Deletes all settings of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - account id
             *
             *  - `where` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.settings.destroyAll = function() {
          var TargetResource = $injector.get("Settings");
          var action = TargetResource["::delete::Account::settings"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Account.settings#destroyById
             * @methodOf lbServices.Account.settings
             *
             * @description
             *
             * Delete a related item by id for settings.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - account id
             *
             *  - `fk` – `{*}` - Foreign key for settings
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.settings.destroyById = function() {
          var TargetResource = $injector.get("Settings");
          var action = TargetResource["::destroyById::Account::settings"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Account.settings#findById
             * @methodOf lbServices.Account.settings
             *
             * @description
             *
             * Find a related item by id for settings.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - account id
             *
             *  - `fk` – `{*}` - Foreign key for settings
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
        R.settings.findById = function() {
          var TargetResource = $injector.get("Settings");
          var action = TargetResource["::findById::Account::settings"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Account.settings#updateById
             * @methodOf lbServices.Account.settings
             *
             * @description
             *
             * Update a related item by id for settings.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - account id
             *
             *  - `fk` – `{*}` - Foreign key for settings
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
        R.settings.updateById = function() {
          var TargetResource = $injector.get("Settings");
          var action = TargetResource["::updateById::Account::settings"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Tab
 * @header lbServices.Tab
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Tab` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Tab",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/tabs/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Tab.settings() instead.
            "prototype$__get__settings": {
              url: urlBase + "/tabs/:id/settings",
              method: "GET",
            },

            // INTERNAL. Use Tab.widgets.findById() instead.
            "prototype$__findById__widgets": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/tabs/:id/widgets/:fk",
              method: "GET",
            },

            // INTERNAL. Use Tab.widgets.destroyById() instead.
            "prototype$__destroyById__widgets": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/tabs/:id/widgets/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Tab.widgets.updateById() instead.
            "prototype$__updateById__widgets": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/tabs/:id/widgets/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Tab.widgets() instead.
            "prototype$__get__widgets": {
              isArray: true,
              url: urlBase + "/tabs/:id/widgets",
              method: "GET",
            },

            // INTERNAL. Use Tab.widgets.create() instead.
            "prototype$__create__widgets": {
              url: urlBase + "/tabs/:id/widgets",
              method: "POST",
            },

            // INTERNAL. Use Tab.widgets.destroyAll() instead.
            "prototype$__delete__widgets": {
              url: urlBase + "/tabs/:id/widgets",
              method: "DELETE",
            },

            // INTERNAL. Use Tab.widgets.count() instead.
            "prototype$__count__widgets": {
              url: urlBase + "/tabs/:id/widgets/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#create
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/tabs",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#upsert
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
            "upsert": {
              url: urlBase + "/tabs",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#replaceOrCreate
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/tabs/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#upsertWithWhere
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/tabs/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#exists
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/tabs/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#findById
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/tabs/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#replaceById
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/tabs/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#find
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/tabs",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#findOne
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/tabs/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#updateAll
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/tabs/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#deleteById
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/tabs/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#count
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/tabs/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#prototype$updateAttributes
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - tab id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
            "prototype$updateAttributes": {
              url: urlBase + "/tabs/:id",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#createChangeStream
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/tabs/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Tab#createMany
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/tabs",
              method: "POST",
            },

            // INTERNAL. Use Settings.tabs.findById() instead.
            "::findById::Settings::tabs": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/settings/:id/tabs/:fk",
              method: "GET",
            },

            // INTERNAL. Use Settings.tabs.destroyById() instead.
            "::destroyById::Settings::tabs": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/settings/:id/tabs/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Settings.tabs.updateById() instead.
            "::updateById::Settings::tabs": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/settings/:id/tabs/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Settings.tabs() instead.
            "::get::Settings::tabs": {
              isArray: true,
              url: urlBase + "/settings/:id/tabs",
              method: "GET",
            },

            // INTERNAL. Use Settings.tabs.create() instead.
            "::create::Settings::tabs": {
              url: urlBase + "/settings/:id/tabs",
              method: "POST",
            },

            // INTERNAL. Use Settings.tabs.createMany() instead.
            "::createMany::Settings::tabs": {
              isArray: true,
              url: urlBase + "/settings/:id/tabs",
              method: "POST",
            },

            // INTERNAL. Use Settings.tabs.destroyAll() instead.
            "::delete::Settings::tabs": {
              url: urlBase + "/settings/:id/tabs",
              method: "DELETE",
            },

            // INTERNAL. Use Settings.tabs.count() instead.
            "::count::Settings::tabs": {
              url: urlBase + "/settings/:id/tabs/count",
              method: "GET",
            },

            // INTERNAL. Use Widget.tab() instead.
            "::get::Widget::tab": {
              url: urlBase + "/widgets/:id/tab",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Tab#patchOrCreate
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
        R["patchOrCreate"] = R["upsert"];

            /**
             * @ngdoc method
             * @name lbServices.Tab#updateOrCreate
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
        R["updateOrCreate"] = R["upsert"];

            /**
             * @ngdoc method
             * @name lbServices.Tab#patchOrCreateWithWhere
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Tab#update
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Tab#destroyById
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Tab#removeById
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Tab#prototype$patchAttributes
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - tab id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
        R["prototype$patchAttributes"] = R["prototype$updateAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Tab#modelName
        * @propertyOf lbServices.Tab
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Tab`.
        */
        R.modelName = "Tab";


            /**
             * @ngdoc method
             * @name lbServices.Tab#settings
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Fetches belongsTo relation settings.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - tab id
             *
             *  - `refresh` – `{boolean=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Settings` object.)
             * </em>
             */
        R.settings = function() {
          var TargetResource = $injector.get("Settings");
          var action = TargetResource["::get::Tab::settings"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Tab.widgets
     * @header lbServices.Tab.widgets
     * @object
     * @description
     *
     * The object `Tab.widgets` groups methods
     * manipulating `Widget` instances related to `Tab`.
     *
     * Call {@link lbServices.Tab#widgets Tab.widgets()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Tab#widgets
             * @methodOf lbServices.Tab
             *
             * @description
             *
             * Queries widgets of tab.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - tab id
             *
             *  - `filter` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
        R.widgets = function() {
          var TargetResource = $injector.get("Widget");
          var action = TargetResource["::get::Tab::widgets"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Tab.widgets#count
             * @methodOf lbServices.Tab.widgets
             *
             * @description
             *
             * Counts widgets of tab.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - tab id
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.widgets.count = function() {
          var TargetResource = $injector.get("Widget");
          var action = TargetResource["::count::Tab::widgets"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Tab.widgets#create
             * @methodOf lbServices.Tab.widgets
             *
             * @description
             *
             * Creates a new instance in widgets of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - tab id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
        R.widgets.create = function() {
          var TargetResource = $injector.get("Widget");
          var action = TargetResource["::create::Tab::widgets"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Tab.widgets#createMany
             * @methodOf lbServices.Tab.widgets
             *
             * @description
             *
             * Creates a new instance in widgets of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - tab id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
        R.widgets.createMany = function() {
          var TargetResource = $injector.get("Widget");
          var action = TargetResource["::createMany::Tab::widgets"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Tab.widgets#destroyAll
             * @methodOf lbServices.Tab.widgets
             *
             * @description
             *
             * Deletes all widgets of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - tab id
             *
             *  - `where` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.widgets.destroyAll = function() {
          var TargetResource = $injector.get("Widget");
          var action = TargetResource["::delete::Tab::widgets"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Tab.widgets#destroyById
             * @methodOf lbServices.Tab.widgets
             *
             * @description
             *
             * Delete a related item by id for widgets.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - tab id
             *
             *  - `fk` – `{*}` - Foreign key for widgets
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.widgets.destroyById = function() {
          var TargetResource = $injector.get("Widget");
          var action = TargetResource["::destroyById::Tab::widgets"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Tab.widgets#findById
             * @methodOf lbServices.Tab.widgets
             *
             * @description
             *
             * Find a related item by id for widgets.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - tab id
             *
             *  - `fk` – `{*}` - Foreign key for widgets
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
        R.widgets.findById = function() {
          var TargetResource = $injector.get("Widget");
          var action = TargetResource["::findById::Tab::widgets"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Tab.widgets#updateById
             * @methodOf lbServices.Tab.widgets
             *
             * @description
             *
             * Update a related item by id for widgets.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - tab id
             *
             *  - `fk` – `{*}` - Foreign key for widgets
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
        R.widgets.updateById = function() {
          var TargetResource = $injector.get("Widget");
          var action = TargetResource["::updateById::Tab::widgets"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Widget
 * @header lbServices.Widget
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Widget` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Widget",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/widgets/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Widget.tab() instead.
            "prototype$__get__tab": {
              url: urlBase + "/widgets/:id/tab",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#create
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/widgets",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#upsert
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
            "upsert": {
              url: urlBase + "/widgets",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#replaceOrCreate
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/widgets/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#upsertWithWhere
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/widgets/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#exists
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/widgets/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#findById
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/widgets/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#replaceById
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/widgets/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#find
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/widgets",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#findOne
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/widgets/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#updateAll
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/widgets/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#deleteById
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/widgets/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#count
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/widgets/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#prototype$updateAttributes
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - widget id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
            "prototype$updateAttributes": {
              url: urlBase + "/widgets/:id",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#createChangeStream
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/widgets/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Widget#createMany
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/widgets",
              method: "POST",
            },

            // INTERNAL. Use Tab.widgets.findById() instead.
            "::findById::Tab::widgets": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/tabs/:id/widgets/:fk",
              method: "GET",
            },

            // INTERNAL. Use Tab.widgets.destroyById() instead.
            "::destroyById::Tab::widgets": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/tabs/:id/widgets/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Tab.widgets.updateById() instead.
            "::updateById::Tab::widgets": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/tabs/:id/widgets/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Tab.widgets() instead.
            "::get::Tab::widgets": {
              isArray: true,
              url: urlBase + "/tabs/:id/widgets",
              method: "GET",
            },

            // INTERNAL. Use Tab.widgets.create() instead.
            "::create::Tab::widgets": {
              url: urlBase + "/tabs/:id/widgets",
              method: "POST",
            },

            // INTERNAL. Use Tab.widgets.createMany() instead.
            "::createMany::Tab::widgets": {
              isArray: true,
              url: urlBase + "/tabs/:id/widgets",
              method: "POST",
            },

            // INTERNAL. Use Tab.widgets.destroyAll() instead.
            "::delete::Tab::widgets": {
              url: urlBase + "/tabs/:id/widgets",
              method: "DELETE",
            },

            // INTERNAL. Use Tab.widgets.count() instead.
            "::count::Tab::widgets": {
              url: urlBase + "/tabs/:id/widgets/count",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Widget#patchOrCreate
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
        R["patchOrCreate"] = R["upsert"];

            /**
             * @ngdoc method
             * @name lbServices.Widget#updateOrCreate
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
        R["updateOrCreate"] = R["upsert"];

            /**
             * @ngdoc method
             * @name lbServices.Widget#patchOrCreateWithWhere
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Widget#update
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Widget#destroyById
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Widget#removeById
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Widget#prototype$patchAttributes
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - widget id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Widget` object.)
             * </em>
             */
        R["prototype$patchAttributes"] = R["prototype$updateAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Widget#modelName
        * @propertyOf lbServices.Widget
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Widget`.
        */
        R.modelName = "Widget";


            /**
             * @ngdoc method
             * @name lbServices.Widget#tab
             * @methodOf lbServices.Widget
             *
             * @description
             *
             * Fetches belongsTo relation tab.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - widget id
             *
             *  - `refresh` – `{boolean=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Tab` object.)
             * </em>
             */
        R.tab = function() {
          var TargetResource = $injector.get("Tab");
          var action = TargetResource["::get::Widget::tab"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Objectjson
 * @header lbServices.Objectjson
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Objectjson` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Objectjson",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/objectjsons/:id",
          { 'id': '@id' },
          {

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#create
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/objectjsons",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#upsert
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
            "upsert": {
              url: urlBase + "/objectjsons",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#replaceOrCreate
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/objectjsons/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#upsertWithWhere
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/objectjsons/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#exists
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/objectjsons/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#findById
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/objectjsons/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#replaceById
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/objectjsons/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#find
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/objectjsons",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#findOne
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string ({"something":"value"})
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/objectjsons/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#updateAll
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/objectjsons/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#deleteById
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/objectjsons/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#count
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/objectjsons/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#prototype$updateAttributes
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - objectjson id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
            "prototype$updateAttributes": {
              url: urlBase + "/objectjsons/:id",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#createChangeStream
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/objectjsons/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#createMany
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/objectjsons",
              method: "POST",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Objectjson#patchOrCreate
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
        R["patchOrCreate"] = R["upsert"];

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#updateOrCreate
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
        R["updateOrCreate"] = R["upsert"];

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#patchOrCreateWithWhere
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#update
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#destroyById
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#removeById
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Objectjson#prototype$patchAttributes
             * @methodOf lbServices.Objectjson
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - objectjson id
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Objectjson` object.)
             * </em>
             */
        R["prototype$patchAttributes"] = R["prototype$updateAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Objectjson#modelName
        * @propertyOf lbServices.Objectjson
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Objectjson`.
        */
        R.modelName = "Objectjson";



        return R;
      }]);


  module
  .factory('LoopBackAuth', function() {
    var props = ['accessTokenId', 'currentUserId', 'rememberMe'];
    var propsPrefix = '$LoopBack$';

    function LoopBackAuth() {
      var self = this;
      props.forEach(function(name) {
        self[name] = load(name);
      });
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.save = function() {
      var self = this;
      var storage = this.rememberMe ? localStorage : sessionStorage;
      props.forEach(function(name) {
        save(storage, name, self[name]);
      });
    };

    LoopBackAuth.prototype.setUser = function(accessTokenId, userId, userData) {
      this.accessTokenId = accessTokenId;
      this.currentUserId = userId;
      this.currentUserData = userData;
    };

    LoopBackAuth.prototype.clearUser = function() {
      this.accessTokenId = null;
      this.currentUserId = null;
      this.currentUserData = null;
    };

    LoopBackAuth.prototype.clearStorage = function() {
      props.forEach(function(name) {
        save(sessionStorage, name, null);
        save(localStorage, name, null);
      });
    };

    return new LoopBackAuth();

    // Note: LocalStorage converts the value to string
    // We are using empty string as a marker for null/undefined values.
    function save(storage, name, value) {
      try {
        var key = propsPrefix + name;
        if (value == null) value = '';
        storage[key] = value;
      } catch (err) {
        console.log('Cannot access local/session storage:', err);
      }
    }

    function load(name) {
      var key = propsPrefix + name;
      return localStorage[key] || sessionStorage[key] || null;
    }
  })
  .config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('LoopBackAuthRequestInterceptor');
  }])
  .factory('LoopBackAuthRequestInterceptor', ['$q', 'LoopBackAuth',
    function($q, LoopBackAuth) {
      return {
        'request': function(config) {
          // filter out external requests
          var host = getHost(config.url);
          if (host && config.url.indexOf(urlBaseHost) === -1) {
            return config;
          }

          if (LoopBackAuth.accessTokenId) {
            config.headers[authHeader] = LoopBackAuth.accessTokenId;
          } else if (config.__isGetCurrentUser__) {
            // Return a stub 401 error for User.getCurrent() when
            // there is no user logged in
            var res = {
              body: { error: { status: 401 }},
              status: 401,
              config: config,
              headers: function() { return undefined; },
            };
            return $q.reject(res);
          }
          return config || $q.when(config);
        },
      };
    }])

  /**
   * @ngdoc object
   * @name lbServices.LoopBackResourceProvider
   * @header lbServices.LoopBackResourceProvider
   * @description
   * Use `LoopBackResourceProvider` to change the global configuration
   * settings used by all models. Note that the provider is available
   * to Configuration Blocks only, see
   * {@link https://docs.angularjs.org/guide/module#module-loading-dependencies Module Loading & Dependencies}
   * for more details.
   *
   * ## Example
   *
   * ```js
   * angular.module('app')
   *  .config(function(LoopBackResourceProvider) {
   *     LoopBackResourceProvider.setAuthHeader('X-Access-Token');
   *  });
   * ```
   */
  .provider('LoopBackResource', function LoopBackResourceProvider() {
    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#setAuthHeader
     * @methodOf lbServices.LoopBackResourceProvider
     * @param {string} header The header name to use, e.g. `X-Access-Token`
     * @description
     * Configure the REST transport to use a different header for sending
     * the authentication token. It is sent in the `Authorization` header
     * by default.
     */
    this.setAuthHeader = function(header) {
      authHeader = header;
    };

    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#getAuthHeader
     * @methodOf lbServices.LoopBackResourceProvider
     * @description
     * Get the header name that is used for sending the authentication token.
     */
    this.getAuthHeader = function() {
      return authHeader;
    };

    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#setUrlBase
     * @methodOf lbServices.LoopBackResourceProvider
     * @param {string} url The URL to use, e.g. `/api` or `//example.com/api`.
     * @description
     * Change the URL of the REST API server. By default, the URL provided
     * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
     */
    this.setUrlBase = function(url) {
      urlBase = url;
      urlBaseHost = getHost(urlBase) || location.host;
    };

    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#getUrlBase
     * @methodOf lbServices.LoopBackResourceProvider
     * @description
     * Get the URL of the REST API server. The URL provided
     * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
     */
    this.getUrlBase = function() {
      return urlBase;
    };

    this.$get = ['$resource', function($resource) {
      var LoopBackResource = function(url, params, actions) {
        var resource = $resource(url, params, actions);

        // Angular always calls POST on $save()
        // This hack is based on
        // http://kirkbushell.me/angular-js-using-ng-resource-in-a-more-restful-manner/
        resource.prototype.$save = function(success, error) {
          // Fortunately, LoopBack provides a convenient `upsert` method
          // that exactly fits our needs.
          var result = resource.upsert.call(this, {}, this, success, error);
          return result.$promise || result;
        };
        return resource;
      };

      LoopBackResource.getUrlBase = function() {
        return urlBase;
      };

      LoopBackResource.getAuthHeader = function() {
        return authHeader;
      };

      return LoopBackResource;
    }];
  });
})(window, window.angular);

'use strict';

/**
 * @ngdoc function
 * @name profilerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the profilerApp
 */
angular.module('profilerApp')
  .controller('MainCtrl', ["$scope", "$rootScope", "$uibModal", "$uibModalStack", "Account", "$state", "$stateParams", "Settings", "$mdSidenav", "$location", function ($scope, $rootScope, $uibModal, $uibModalStack, Account, $state, $stateParams, Settings , $mdSidenav, $location) {
   $scope.toggleLeft = buildToggler('left');
   $scope.toggleRight = buildToggler('right');
   $scope.toggleMain = buildToggler('mainView');

    $scope.resetdefault = function(){
        console.log("in main.js ")
        $scope.$broadcast ('resetdefault');

    }

     $scope.onDragstart = function(list, event) {
           var img = new Image();
         img.src = "images/groupbox1_chart.svg";
         event.dataTransfer.setDragImage(img, 0, 0);
    };
    function buildToggler(componentId) {
      return function() {
        $mdSidenav(componentId).toggle();
      };
    }
    $scope.expand = function (scope) {
      scope.expand();
    };
    $scope.collapse = function (scope) {
      console.log('collapse');
      scope.collapse();
    };

    $scope.showList = false;
     $scope.contactOpen = true;
     $scope.toggleList = function () {
       $scope.showList = !$scope.showList;
       $scope.contactOpen = !$scope.contactOpen;
     }



     $scope.clickItem = function(node) {
       node.open = !node.open;
      }

     $scope.edit = false;
     $scope.delete = false;
     $scope.deletePanel = false;
     $scope.editName = false;

     $scope.showHideIcon = function () {

        $scope.delete = !$scope.delete;
        $scope.deletePanel = !$scope.deletePanel;
        $scope.editName  = !$scope.editName;
     }
    $scope.collapseAll = function () {
  $scope.$broadcast('angular-ui-tree:collapse-all');
};

$scope.expandAll = function () {
  $scope.$broadcast('angular-ui-tree:expand-all');
};
    $scope.urlquery = $location.search();
    $scope.contactId = $stateParams.id;
    $scope.accountId = $stateParams.accountId;
    $scope.userId = $scope.urlquery.userId;
    $scope.siteId = $scope.urlquery.siteId;
    $scope.sortableOptions = {

     };
}]);

'use strict';

/**
 * @ngdoc function
 * @name profilerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the profilerApp
 */
angular.module('profilerApp')
  .controller('AdminCtrl', ["$scope", "$rootScope", "$uibModal", "$uibModalStack", "Account", "$state", "$stateParams", "Settings", "$mdSidenav", "$location", "Objectjson", function ($scope, $rootScope, $uibModal, $uibModalStack, Account, $state, $stateParams, Settings , $mdSidenav, $location, Objectjson) {
   $scope.connectiontypes = ['eloqua', 'salesforce']
   $scope.urlquery = $location.search();
   $scope.userId = $scope.urlquery.userId;
   $scope.siteId = $scope.urlquery.siteId;

   $scope.geteloquafields = function(){
     Account.contactfields({id: $scope.accountId, page: 1,  query: "",objname: 'contact'},function(result){
       $scope.eloquacontactfileds = result.elements;
     });
   }
   $scope.getAccount = function(siteId){
     Account.findOne({where:{ siteId: siteId}}, function(account){
        $scope.account = account;
        $scope.accountId = account.id;
        $scope.getEloquaUsers();
        $scope.geteloquafields();
     });
   }




   $scope.getAccount($scope.siteId);

   $scope.alreadyAdded = false;
   $scope.loadingEloqua = true;

   $scope.saveAccount = function(account){
   Account.prototype$updateAttributes($scope.account.id, $scope.account, function(account){
       $scope.account = account;
        });
   }

   $scope.query = {
     query: "",
     page: 1,
     limit: 10,
     order: "email"
   }

   $scope.eloquaquery = {
     query: "",
     page: 1,
     limit: 10
   }

   $scope.getAcl = function(){

     var skip = ($scope.query.page - 1) * $scope.query.limit;
     var query = {"email": {"regexp": $scope.query.query }};
     Settings.find({ filter: {skip: skip, limit: $scope.query.limit, order: $scope.query.order, where: query }}, function(result){
       Settings.count({ skip: skip, limit: $scope.query.limit, order: $scope.query.order }, function(result){
         $scope.aclcount = result.count;
       });
       $scope.acldata = result;
       $scope.acldata.forEach( function (data)
       {
          data.email = data.email;
          data.title = data.email;
          if(data.email.length >= 24)
          {
            data.email = data.email.substr(0,22) + '...'
          }
       });
     });

   }

   $scope.getAcl();

   $scope.searchusers = function(query)
   {
     if(query.length > 3){
       $scope.getEloquaUsers();

     }

   }

   $scope.searchacl = function(query){
     if(query.length > 3 || query.length == 0){
       $scope.getAcl();
     }
   }

   $scope.adduser = function(user){
    $scope.flag = 0;
    $scope.alreadyAdded = false;
     $scope.acldata.forEach( function (data)
     {
      console.log(data)
       if(data.userId == user.id)
       {
          $scope.flag = 1;
          $scope.alreadyAdded = true;
       }
     });
    if($scope.flag == 0)
    {
     var newsettings = {
       email: user.emailAddress,
       userId: user.id,
       siteId: 1234,
       created: new Date(),
       accountId: $scope.accountId
     };
     Settings.create(newsettings, function(added){
       console.log(user.emailAddress + " added");
        $scope.getAcl();
       Settings.reset({id: added.id }, function(){
         $scope.getAcl();
         console.log("default view created");
       });
     });
    }
    else
    {
      console.log(user.emailAddress , " already added");
    }
   }

   $scope.removeuser = function(settings){
     console.log(settings);
     Settings.removeById({id: settings.id}, function(removed){
       console.log(settings.email + "removed");
       $scope.getAcl();
     });
   }


   $scope.getEloquaUsers = function(){
      $scope.loadingEloqua = true;
     Account.searchusers({id: $scope.accountId, query: $scope.eloquaquery.query, page: $scope.eloquaquery.page}, function(users){
       $scope.eloquausers = users.elements;
       $scope.eloquauserscount  = users.total;
       $scope.loadingEloqua = false;
     });
   }



   $scope.fields = {
     "salesforce": [
       {"name": "username", "type" : "text"},
       {"name": "password", "type": "password"},
       {"name": "secret", "type": "text"}
     ]
   };



}]);

'use strict';

/**
 * @ngdoc function
 * @name profilerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the profilerApp
 */
angular.module('profilerApp')
  .controller('TabsCtrl', ["$scope", "$rootScope", "$uibModal", "$uibModalStack", "Account", "$state", "$stateParams", "Settings", "Tab", "Widget", "Objectjson", "$location", "$mdDialog", "jsonPath", function ($scope, $rootScope, $uibModal, $uibModalStack, Account, $state, $stateParams, Settings, Tab, Widget, Objectjson, $location, $mdDialog, jsonPath) {

    $scope.sortBy = function(propertyName)
    {
    $scope.reverse = ($scope.sortType === propertyName) ? !$scope.reverse : false;
    $scope.sortType = propertyName;
  };
  $rootScope.salesforceobjectfields = {};
  $rootScope.secondarydata = {};
  $rootScope.salesforcefields = [];
  $rootScope.salesforcechilds = {};
  var month = new Array();
  month[0] = "Jan";
  month[1] = "Feb";
  month[2] = "Mar";
  month[3] = "Apr";
  month[4] = "May";
  month[5] = "Jun";
  month[6] = "Jul";
  month[7] = "Aug";
  month[8] = "Sep";
  month[9] = "Oct";
  month[10] = "Nov";
  month[11] = "Dec";

	$scope.urlquery = $location.search();
    $scope.urltype = $scope.urlquery.type;
    if($scope.urltype == 'id'){
      $scope.contactId = $stateParams.id;
    } else if($scope.urltype == 'email') {
      $scope.email = $stateParams.id;
    } else if ($scope.urltype == 'sfdcaccountId') {
      $scope.sfdcaccountId = $stateParams.id;
    }
    $scope.accountId = $stateParams.accountId;

	$scope.getSettings = function(accountId){
      Account.findById({"id": accountId}, function(account){
        Settings.findOne({ "filter": {"where": { "siteId": $scope.siteId, "userId": $scope.userId}}}, function(settings){
          $scope.account = account;
          if(settings && settings.id){
            $rootScope.settings = settings;
          }
        }, function(error){
          var newsettings = {
             "properties": [
               {}
             ],
             "accountId": $scope.accountId,
             "userId": $scope.userId,
             "siteId": $scope.siteId
           }
          Settings.create(newsettings, function(settings){
            $rootScope.settings = settings;
            $scope.resetdefault();
          });
        });
      });
    }

    $scope.getAccount = function(accountId){
      Account.findById({"id": accountId}, function(account){
         $scope.account = account;
         $scope.getSettings($scope.accountId);
      });
    }


    $scope.getAccount($scope.accountId);
    $scope.userId = $scope.urlquery.userId;
    $scope.siteId = $scope.urlquery.siteId;
    $rootScope.tabindex = 0;
    $rootScope.parentobjects = ['Contact', 'Lead'];
    $rootScope.parentobjectsReference = {
      'Contact': 'Email',
      'Lead': 'Email'
    };
    $rootScope.loadingSalesForce = true;
    $rootScope.loadingEloqua = true;
    $rootScope.loadingTabs = true;
    $scope.parentdataavailable = false;
    $scope.dataLoaded = false;

    $rootScope.eloquaParentobjects = ['Contact', 'Account' , 'Analytics' , 'Contact Activities' , 'Account Activities'];

    $rootScope.eloquaActivityTypes = ["emailOpen","emailSend","emailClickThrough","webVisit","formSubmit",
                "campaignMembership"
            ] ;

    $scope.filters = [
      {
        filterBy: 'By Type',
        type: [
          { name: 'Email Opens' , type : 'emailOpens'},
          { name: 'Email Sends' , type : 'emailSends'},
          { name: 'Email Click', type: 'emailClick'},
          { name: 'Form Submition', type:'formSubmited'},
          { name: 'Web Visit' , type:'webVisites'},
          { name: 'Campaign Membership',type:'campaignMemberships'},
        ]
      }
    ];


    $scope.customDate = false;
    $scope.nameSearch = false;
    $scope.emailOpens = true;
    $scope.emailSends = true;
    $scope.emailClick= true;
    $scope.formSubmited = true;
    $scope.campaignMemberships = true;
    $scope.webVisites = true;
    $scope.customDateForLatestActivitis = false;
    $scope.nameSearchForLatestActivities = false;
    $scope.customDateForActivitiesGraph = false;
    $scope.nameSearchForActivitiesGraph = false;
    var endDate = new Date();

	console.log('month ',month[endDate.getMonth()]);
    var startDate = new Date(endDate.getTime() - (365 * 24 * 60 * 60 * 1000));

    $scope.todate = Math.floor(endDate.getTime() / 1000);
    $scope.fromdate = Math.floor(startDate.getTime() / 1000);

	$scope.activityStartDate = $scope.fromdate;
	$scope.activityEndDate = $scope.todate;
	$rootScope.filterByCustomDate = function(startDate , endDate)
    {

      if(!angular.isUndefined(startDate) && !angular.isUndefined(endDate))
      {
		  $rootScope.showAccountCustomDate = false;
		  $scope.todate = endDate/1000;
		  $scope.fromdate =startDate/1000;
		  $rootScope.fromdate = $scope.fromdate;
		  $rootScope.todate = $scope.todate;
          if(startDate/1000 >= $scope.activityStartDate)
		  {
		   $scope.accountActivityGraph = [];
           $rootScope.eloquaActivityTypes.forEach(function(type,index)
			{
				 var monthdata = new Array();
				 monthdata[0] = { x: "January", y: 0 };
				 monthdata[1] = { x:"February", y: 0};
				 monthdata[2] = { x: "March", y: 0 };
				 monthdata[3] = { x:"April", y: 0 };
				 monthdata[4] = { x:"May", y: 0 };
				 monthdata[5] = { x:"June", y: 0 };
				 monthdata[6] = { x:"July", y: 0 };
				 monthdata[7] = { x:"August", y: 0 };
				 monthdata[8] = { x:"September", y: 0 };
				 monthdata[9] = { x:"October", y: 0 };
				 monthdata[10] = { x:"November", y: 0 };
				 monthdata[11] = { x:"December", y: 0 };
				 $scope.accountActivityGraph.push({key: type, values: monthdata });
			});
             console.log('in loacal controller');
			 if($scope.accountWidgetPresent)
			 {
	           $scope.filterActivityData();
			 }
			 if( $scope.contactWidgetPresent)
			 {
			    $scope.FilterContactActivity('Custom date');
			 }
		  }
		  else
		  {
			if($scope.globalAccountWidget != '')
			{
			   $scope.flagForaccountBasedActivity = true;
			   $scope.accountActivityGraph = [];
			   $rootScope.eloquaActivityTypes.forEach(function(type,index)
				{
					 var monthdata = new Array();
					 monthdata[0] = { x: "January", y: 0 };
					 monthdata[1] = { x:"February", y: 0};
					 monthdata[2] = { x: "March", y: 0 };
					 monthdata[3] = { x:"April", y: 0 };
					 monthdata[4] = { x:"May", y: 0 };
					 monthdata[5] = { x:"June", y: 0 };
					 monthdata[6] = { x:"July", y: 0 };
					 monthdata[7] = { x:"August", y: 0 };
					 monthdata[8] = { x:"September", y: 0 };
					 monthdata[9] = { x:"October", y: 0 };
					 monthdata[10] = { x:"November", y: 0 };
					 monthdata[11] = { x:"December", y: 0 };
					 $scope.accountActivityGraph.push({key: type, values: monthdata });
				});
			  $scope.getSecondaryData($scope.globalAccountWidget);
			  console.log('in loop');
			}
			if($scope.globalContactWidget != '')
			{
				console.log('in api call');
				$scope.flagForcontactBasedActivity = true;
				$scope.getSecondaryData($scope.globalContactWidget);
			}
		  }
      }
    }

	$scope.getMaxDate = function () {
        return new Date(
            endDate.getFullYear(),
            endDate.getMonth(),
            endDate.getDate());
    }

    $rootScope.maxDate = $scope.getMaxDate();

	$scope.eloquaAccountDataOnLoad = [];
	$rootScope.fromdate = $scope.fromdate;
	$rootScope.todate = $scope.todate;
	$rootScope.showDateFilter = false;

    $scope.filterByDate = function(type , widget)
    {
		console.log('in date function ');
        if(type == 'Custom Date' && widget.json.internalName == 'Activity Breakdown')
        {
          $scope.customDate = true;
          $scope.nameSearch = false;
        }
        else if(type == 'Custom Date' && widget.json.internalName == 'Latest Activities')
        {
           $scope.customDateForLatestActivitis = true;
           $scope.nameSearchForLatestActivities = false;
        }
        else if(type == 'Custom Date' && widget.json.internalName == 'Activities Graph')
        {
           $scope.customDateForActivitiesGraph = true;
           $scope.nameSearchForActivitiesGraph = false;
        }
        else if(type == '30 Days')
        {
             endDate = new Date();
             startDate = new Date(endDate.getTime() - (30 * 24 * 60 * 60 * 1000));
             $scope.todate = Math.floor(endDate.getTime() / 1000);
             $scope.fromdate = Math.floor(startDate.getTime() / 1000);

            filterByDateContectDate(widget);
        }
        else if(type == '6 Months')
        {
            endDate = new Date();
            startDate = new Date(endDate.getTime() - (182 * 24 * 60 * 60 * 1000));
            $scope.todate = Math.floor(endDate.getTime() / 1000);
            $scope.fromdate = Math.floor(startDate.getTime() / 1000);
            filterByDateContectDate(widget);
        }
        else if(type == '1 Year')
        {
            endDate = new Date();
            startDate = new Date(endDate.getTime() - (365 * 24 * 60 * 60 * 1000));
            $scope.todate = Math.floor(endDate.getTime() / 1000);
            $scope.fromdate = Math.floor(startDate.getTime() / 1000);
            filterByDateContectDate(widget);

        }
    }

	$scope.globalAccountWidget = '';
	$scope.globalContactWidget = '';
	$rootScope.showAccountCustomDate = false;
	$rootScope.filterByDate = function(type)
    {
	  $scope.flagForaccountBasedActivity = true;

      if(type == '30 Days')
        {
		   $scope.accountActivityGraph = [];

		      $scope.accountActivityGraph = [];
			   $rootScope.eloquaActivityTypes.forEach(function(type,index)
				{
					 var monthdata = new Array();
					 monthdata[0] = { x: "January", y: 0 };
					 monthdata[1] = { x:"February", y: 0};
					 monthdata[2] = { x: "March", y: 0 };
					 monthdata[3] = { x:"April", y: 0 };
					 monthdata[4] = { x:"May", y: 0 };
					 monthdata[5] = { x:"June", y: 0 };
					 monthdata[6] = { x:"July", y: 0 };
					 monthdata[7] = { x:"August", y: 0 };
					 monthdata[8] = { x:"September", y: 0 };
					 monthdata[9] = { x:"October", y: 0 };
					 monthdata[10] = { x:"November", y: 0 };
					 monthdata[11] = { x:"December", y: 0 };
					 $scope.accountActivityGraph.push({key: type, values: monthdata });
				});
             endDate = new Date();
			 startDate = new Date(endDate.getTime() - (30 * 24 * 60 * 60 * 1000));
			 $scope.todate = Math.floor(endDate.getTime() / 1000);
             $scope.fromdate = Math.floor(startDate.getTime() / 1000);
			 $rootScope.fromdate = $scope.fromdate;
	         $rootScope.todate = $scope.todate;
			 var getAllDays = [];
			 for(var i = 0 ;startDate<endDate ; i++)
			 {
				 getAllDays.push(startDate.getMonth()+1 +'/'+startDate.getDate());
				  console.log('month ' , startDate.getMonth()+1 , '/' ,startDate.getDate());
				  startDate.setDate(startDate.getDate()+1);
			 }
			 $scope.contactActivityGraph = []
		   $rootScope.eloquaActivityTypes.forEach(function(type,index)
			{
				 var monthdata = new Array();
				 for(var j = 0 ; j < getAllDays.length ; j++)
				 {
					monthdata[j] = { x: getAllDays[j], y: 0 };
				 }
				 $scope.contactActivityGraph.push({key: type, values: monthdata });
			});

			 if($scope.accountWidgetPresent)
			 {
	           $scope.filterActivityData();
			 }
			 if( $scope.contactWidgetPresent)
			 {
			    $scope.FilterContactActivity(type);
			 }

			 $rootScope.showAccountCustomDate = false;
        }
        else if(type == '6 Months')
        {
		   $scope.accountActivityGraph = [];
           $rootScope.eloquaActivityTypes.forEach(function(type,index)
			{
				 var monthdata = new Array();
				 monthdata[0] = { x: "January", y: 0 };
				 monthdata[1] = { x:"February", y: 0};
				 monthdata[2] = { x: "March", y: 0 };
				 monthdata[3] = { x:"April", y: 0 };
				 monthdata[4] = { x:"May", y: 0 };
				 monthdata[5] = { x:"June", y: 0 };
				 monthdata[6] = { x:"July", y: 0 };
				 monthdata[7] = { x:"August", y: 0 };
				 monthdata[8] = { x:"September", y: 0 };
				 monthdata[9] = { x:"October", y: 0 };
				 monthdata[10] = { x:"November", y: 0 };
				 monthdata[11] = { x:"December", y: 0 };
				 $scope.accountActivityGraph.push({key: type, values: monthdata });
			});
            endDate = new Date();
            startDate = new Date(endDate.getTime() - (182 * 24 * 60 * 60 * 1000));
            $scope.todate = Math.floor(endDate.getTime() / 1000);
            $scope.fromdate = Math.floor(startDate.getTime() / 1000);
			$rootScope.fromdate = $scope.fromdate;
	        $rootScope.todate = $scope.todate;
			 var getAllMonths = [];
			 for(var i = 0 ;startDate<endDate ; i++)
			 {
				 getAllMonths.push(month[startDate.getMonth()]);
				  console.log('month ' , month[startDate.getMonth()]);
				  startDate.setMonth(startDate.getMonth()+1);
			 }
		   $scope.contactActivityGraph = []
		   $rootScope.eloquaActivityTypes.forEach(function(type,index)
			{
				 var monthdata = new Array();
				 for(var j = 0 ; j < getAllMonths.length ; j++)
				 {
					monthdata[j] = { x: getAllMonths[j], y: 0 };
				 }
				 $scope.contactActivityGraph.push({key: type, values: monthdata });
			});


			if($scope.accountWidgetPresent)
			 {
	           $scope.filterActivityData();
			 }
			 if( $scope.contactWidgetPresent)
			 {
			    $scope.FilterContactActivity(type);
			 }
			$rootScope.showAccountCustomDate = false;
        }
        else if(type == '1 Year')
        {
			$scope.accountActivityGraph = [];
           $rootScope.eloquaActivityTypes.forEach(function(type,index)
			{
				 var monthdata = new Array();
				 monthdata[0] = { x: "January", y: 0 };
				 monthdata[1] = { x:"February", y: 0};
				 monthdata[2] = { x: "March", y: 0 };
				 monthdata[3] = { x:"April", y: 0 };
				 monthdata[4] = { x:"May", y: 0 };
				 monthdata[5] = { x:"June", y: 0 };
				 monthdata[6] = { x:"July", y: 0 };
				 monthdata[7] = { x:"August", y: 0 };
				 monthdata[8] = { x:"September", y: 0 };
				 monthdata[9] = { x:"October", y: 0 };
				 monthdata[10] = { x:"November", y: 0 };
				 monthdata[11] = { x:"December", y: 0 };
				 $scope.accountActivityGraph.push({key: type, values: monthdata });
			});
            endDate = new Date();
            startDate = new Date(endDate.getTime() - (365 * 24 * 60 * 60 * 1000));
            $scope.todate = Math.floor(endDate.getTime() / 1000);
            $scope.fromdate = Math.floor(startDate.getTime() / 1000);
			$rootScope.fromdate = $scope.fromdate;
			$rootScope.todate = $scope.todate;
			if($scope.accountWidgetPresent)
			 {
	           $scope.filterActivityData();
			 }
			 if($scope.contactWidgetPresent)
			 {
			    $scope.FilterContactActivity(type);
			 }
			$rootScope.showAccountCustomDate = false;
        }
		else if(type == 'Custom Date')
		{
			$rootScope.showAccountCustomDate = true;
		}

    }




    $scope.showNameSearch = function(widget)
    {
      if(widget.json.internalName == 'Activity Breakdown')
      {
        $scope.customDate = false;
        $scope.nameSearch = true;
      }
      else if(widget.json.internalName == 'Latest Activities')
      {
        $scope.customDateForLatestActivitis = false;
        $scope.nameSearchForLatestActivities = true;
      }
      else if(widget.json.internalName == 'Activities Graph')
      {
        $scope.customDateForLatestActivitis = false;
        $scope.nameSearchForLatestActivities = true;
      }
    }

    $scope.filterByName = function(widget ,action , text)
    {
      if(text == '')
      {
         text = undefined;
      }
      if(!angular.isUndefined(text))
      {
        activityFilterByName(widget , action , text);
      }
    }

    function activityFilterByName(widget , action , text)
    {
       if(widget.json.internalName == 'Activity Breakdown')
          {
             widget.name = widget.json.internalName;
             $scope.widgetdata[widget.id] = {};
             $rootScope.eloquaActivityTypes.forEach(function(type){
             var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
             widget.json.params.forEach(function(param){
              if(param.name == 'type'){
                 query[param.name] = type;
               } else {
                  if(param.name == 'fromdate')
                  {
                      query[param.name] = $scope.fromdate;
                  }
                  else if(param.name == 'todate')
                  {
                      query[param.name] = $scope.todate;
                  }
                }
              });
             Account.secondarydata(query, function(result)
             {
                $scope.filterArray = [];
                result.result.forEach(function(activity)
                {
                  activity.details.forEach(function(details){
                  if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                   if(!angular.isUndefined(text) && !action)
                   {
                    if(activity.name.toLowerCase().match(text) || activity.name.match(text))
                    {
                      $scope.filterArray.push(activity);
                    }
                    else
                    {
                      return;
                    }
                  }
                  else
                  {
                    if(!(activity.name.toLowerCase().match(text) || activity.name.match(text)))
                    {
                      $scope.filterArray.push(activity);
                    }
                    else
                    {
                      return;
                    }
                  }

                  }
                 });
                });
                $scope.widgetdata[widget.id][type] = $scope.filterArray;
                $scope.widgetdata[widget.id][type].forEach(function(activity)
                {
                  activity.details.forEach(function(details){
                  if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                  }
                  if(details[Object.keys(details)[0]] == "Collection")
                  {
                      activity.formSubmitionData = details[Object.keys(details)[1]];
                      activity.formSubmitionData = activity.formSubmitionData.split("elqdelimiter");
                  }
                 });
                });
             });
          });
        }
        else if(widget.json.internalName == 'Latest Activities'){
           widget.name = widget.json.internalName;
           $scope.widgetdata[widget.id] = [];
           $rootScope.eloquaActivityTypes.forEach(function(type){
             var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
             widget.json.params.forEach(function(param){
               if(param.name == 'type'){
                 query[param.name] = type;
               } else {
                 if(param.name == 'fromdate')
                  {
                      query[param.name] = $scope.fromdate;
                  }
                  else if(param.name == 'todate')
                  {
                      query[param.name] = $scope.todate;
                  }
               }

             });
             Account.secondarydata(query, function(result){

                $scope.filterArray = [];
                result.result.forEach(function(activity)
                {
                  activity.details.forEach(function(details){
                  if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                   if(!angular.isUndefined(text) && !action)
                   {
                    if(activity.name.toLowerCase().match(text) || activity.name.match(text))
                    {
                      $scope.filterArray.push(activity);
                    }
                    else
                    {
                      return;
                    }
                  }
                  else
                  {
                    if(!(activity.name.toLowerCase().match(text) || activity.name.match(text)))
                    {
                      $scope.filterArray.push(activity);
                    }
                    else
                    {
                      return;
                    }
                  }
                  }
                 });
                });

               $scope.widgetdata[widget.id] = $scope.widgetdata[widget.id].concat($scope.filterArray);
               $scope.widgetdata[widget.id].forEach(function(activity)
               {
                activity.activityType = activity.activityType;
                activity.activityDate = activity.activityDate;
                activity.show = true;
                if(activity.activityType == 'emailOpen')
                {
                     activity.image = "/img/emailOpen.PNG";
                }
                else if(activity.activityType == 'emailSend')
                {
                      activity.image = "/img/emailSends.PNG";
                }
                else if(activity.activityType == 'emailClickThrough')
                {
                        activity.image =  "/img/emailClicked.PNG";
                }
                else if(activity.activityType == 'webVisit')
                {
                         activity.image =   "/img/webVisited.PNG";
                }
                else if(activity.activityType == 'formSubmit')
                {
                        activity.image = "/img/formSubmition.PNG";
                }
                else if(activity.activityType == 'campaignMembership')
                {
                        activity.image =  "/img/externalActivities.PNG";
                }
                activity.details.forEach(function(details){
                  if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.subjectLine = details[Object.keys(details)[1]];
                  }
                  else if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                  }

                });

               });

             });
           });
         }   else if(widget.json.internalName == 'Activities Graph'){
              widget.name = widget.json.internalName;
              $scope.widgetdata[widget.id] = [];
              $rootScope.eloquaActivityTypes.forEach(function(type, index){
                var monthdata = new Array();
                monthdata[0] = { x: "January", y: 0 };
                monthdata[1] = { x:"February", y: 0};
                monthdata[2] = { x: "March", y: 0 };
                monthdata[3] = { x:"April", y: 0 };
                monthdata[4] = { x:"May", y: 0 };
                monthdata[5] = { x:"June", y: 0 };
                monthdata[6] = { x:"July", y: 0 };
                monthdata[7] = { x:"August", y: 0 };
                monthdata[8] = { x:"September", y: 0 };
                monthdata[9] = { x:"October", y: 0 };
                monthdata[10] = { x:"November", y: 0 };
                monthdata[11] = { x:"December", y: 0 };
                $scope.widgetdata[widget.id].push({key: type, values: monthdata });
                var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
                widget.json.params.forEach(function(param){
                  if(param.name == 'type'){
                    query[param.name] = type;
                  } else {
                    if(param.name == 'fromdate')
                     {
                         query[param.name] = $scope.fromdate;
                     }
                     else if(param.name == 'todate')
                     {
                         query[param.name] = $scope.todate;
                     }
                  }

                });
                Account.secondarydata(query, function(result){

                   $scope.filterArray = [];
                   result.result.forEach(function(activity)
                   {
                     activity.details.forEach(function(details){
                     if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
                       || details[Object.keys(details)[0]] == 'URL')
                     {
                       activity.name = details[Object.keys(details)[1]];
                      if(!angular.isUndefined(contains) && angular.isUndefined(notContains))
                      {
                       if(activity.name.toLowerCase().match(contains) || activity.name.match(contains))
                       {
                         var key;
                         if(widget.json.interval == 'month'){
                           key = new Date(activity.activityDate*1000).getMonth();
                         } else if (widget.json.interval == 'year') {
                           key = new Date(activity.activityDate*1000).getYear();
                         } else if (widget.json.interval == 'day') {
                           key = new Date(activity.activityDate*1000).getDay();
                         }
                         $scope.widgetdata[widget.id][index].values[parseInt(key)].y++;

                       }
                       else
                       {
                         return;
                       }
                     }
                     else if(!angular.isUndefined(contains) && !angular.isUndefined(notContains))
                     {
                       if((activity.name.toLowerCase().match(contains) || activity.name.match(contains))
                         && !(activity.name.toLowerCase().match(notContains) || activity.name.match(notContains)))
                       {
                        var key;
                         if(widget.json.interval == 'month'){
                           key = new Date(activity.activityDate*1000).getMonth();
                         } else if (widget.json.interval == 'year') {
                           key = new Date(activity.activityDate*1000).getYear();
                         } else if (widget.json.interval == 'day') {
                           key = new Date(activity.activityDate*1000).getDay();
                         }
                         $scope.widgetdata[widget.id][index].values[parseInt(key)].y++;

                       }
                       else
                       {
                         return;
                       }
                     }
                     else
                     {
                       if(!(activity.name.toLowerCase().match(notContains) || activity.name.match(notContains)))
                       {
                         var key;
                         if(widget.json.interval == 'month'){
                           key = new Date(activity.activityDate*1000).getMonth();
                         } else if (widget.json.interval == 'year') {
                           key = new Date(activity.activityDate*1000).getYear();
                         } else if (widget.json.interval == 'day') {
                           key = new Date(activity.activityDate*1000).getDay();
                         }
                         $scope.widgetdata[widget.id][index].values[parseInt(key)].y++;

                       }
                       else
                       {
                         return;
                       }
                     }
                     }
                    });
                   });



                });
              });
            }
    }


    $scope.filterByCustomDate = function(widget , startDate , endDate)
    {

      if(!angular.isUndefined(startDate) && !angular.isUndefined(endDate))
      {
        filterByDateContectDate(widget, startDate/1000 , endDate/1000);
      }
    }



    function filterByDateContectDate(widget)
    {

      if (widget.json.internalName == 'Activity Breakdown')
      {
           widget.name = widget.json.internalName;
           $scope.widgetdata[widget.id] = {};
           $rootScope.eloquaActivityTypes.forEach(function(type){
             console.log(type);
             var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
             widget.json.params.forEach(function(param){
               if(param.name == 'type'){
                 query[param.name] = type;
               } else {
                  if(param.name == 'fromdate')
                  {
                      query[param.name] = $scope.fromdate;
                  }
                  else if(param.name == 'todate')
                  {
                      query[param.name] = $scope.todate;
                  }
                }

             });
             Account.secondarydata(query, function(result){
               $scope.widgetdata[widget.id][type] = result.result;
                $scope.widgetdata[widget.id][type].forEach(function(activity)
                {
                  activity.details.forEach(function(details){
                  if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];

                  }
                  if(details[Object.keys(details)[0]] == "Collection")
                  {
                      activity.formSubmitionData = details[Object.keys(details)[1]];
                      activity.formSubmitionData = activity.formSubmitionData.split("elqdelimiter");

                   }
                  });
                });
             });
          });
        }
        else if(widget.json.internalName == 'Latest Activities'){
           widget.name = widget.json.internalName;
           $scope.widgetdata[widget.id] = [];
           $rootScope.eloquaActivityTypes.forEach(function(type){
             var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
             widget.json.params.forEach(function(param){
               if(param.name == 'type'){
                 query[param.name] = type;
               } else {
                 if(param.name == 'fromdate')
                  {
                      query[param.name] = $scope.fromdate;
                  }
                  else if(param.name == 'todate')
                  {
                      query[param.name] = $scope.todate;
                  }
               }

             });
             Account.secondarydata(query, function(result){
               $scope.widgetdata[widget.id] = $scope.widgetdata[widget.id].concat(result.result);
               $scope.widgetdata[widget.id].forEach(function(activity)
               {
                activity.activityType = activity.activityType;
                activity.activityDate = activity.activityDate;
                activity.show = true;
                if(activity.activityType == 'emailOpen')
                {
                     activity.image = "/img/emailOpen.PNG";
                }
                else if(activity.activityType == 'emailSend')
                {
                      activity.image = "/img/emailSends.PNG";
                }
                else if(activity.activityType == 'emailClickThrough')
                {
                        activity.image =  "/img/emailClicked.PNG";
                }
                else if(activity.activityType == 'webVisit')
                {
                         activity.image =   "/img/webVisited.PNG";
                }
                else if(activity.activityType == 'formSubmit')
                {
                        activity.image = "/img/formSubmition.PNG";
                }
                else if(activity.activityType == 'campaignMembership')
                {
                        activity.image =  "/img/externalActivities.PNG";
                }
                activity.details.forEach(function(details){
                  if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.subjectLine = details[Object.keys(details)[1]];
                  }
                  else if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                  }

                });

               });

             });
           });
         }else if(widget.json.internalName == 'Activities Graph'){
              widget.name = widget.json.internalName;
              $scope.widgetdata[widget.id] = [];
              $rootScope.eloquaActivityTypes.forEach(function(type, index){
                var monthdata = new Array();
                monthdata[0] = { x: "January", y: 0 };
                monthdata[1] = { x:"February", y: 0};
                monthdata[2] = { x: "March", y: 0 };
                monthdata[3] = { x:"April", y: 0 };
                monthdata[4] = { x:"May", y: 0 };
                monthdata[5] = { x:"June", y: 0 };
                monthdata[6] = { x:"July", y: 0 };
                monthdata[7] = { x:"August", y: 0 };
                monthdata[8] = { x:"September", y: 0 };
                monthdata[9] = { x:"October", y: 0 };
                monthdata[10] = { x:"November", y: 0 };
                monthdata[11] = { x:"December", y: 0 };
                $scope.widgetdata[widget.id].push({key: type, values: monthdata });
                var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
                widget.json.params.forEach(function(param){
                  if(param.name == 'type'){
                    query[param.name] = type;
                  } else {
                    if(param.name == 'fromdate')
                     {
                         query[param.name] = $scope.fromdate;
                     }
                     else if(param.name == 'todate')
                     {
                         query[param.name] = $scope.todate;
                     }
                  }

                });
                Account.secondarydata(query, function(result){
                   result.result.forEach(function(activity)
                   {

                     var key;
                     if(widget.json.interval == 'month'){
                       key = new Date(activity.activityDate*1000).getMonth();
                     } else if (widget.json.interval == 'year') {
                       key = new Date(activity.activityDate*1000).getYear();
                     } else if (widget.json.interval == 'day') {
                       key = new Date(activity.activityDate*1000).getDay();
                     }
                     $scope.widgetdata[widget.id][index].values[parseInt(key)].y++;

                   });




                });
              });
            }
    }

    $scope.latestEmailOpen = true;
    $scope.latestEmailSend = true;
    $scope.latestEmailClick = true;
    $scope.latestFormSubmit = true;
    $scope.latestWebVisit = true;
    $scope.latestCampainMember = true;
    $scope.checkedType = function(widget , type)
    {
      if (widget.json.internalName == 'Activity Breakdown')
      {
        if(type == 'emailOpens')
        {
          $scope.emailOpens = !$scope.emailOpens;
        }
        else if(type == 'emailSends')
        {
          $scope.emailSends = !$scope.emailSends;
        }
        else if(type == 'emailClick')
        {
          $scope.emailClick = !$scope.emailClick;
        }
        else if(type == 'formSubmited')
        {
          $scope.formSubmited = !$scope.formSubmited;
        }
        else if(type == 'campaignMemberships')
        {
          $scope.campaignMemberships = !$scope.campaignMemberships;
        }
        else if(type=='webVisites')
        {
           $scope.webVisites = ! $scope.webVisites;
        }
      }
      else if(widget.json.internalName == 'Latest Activities')
      {
         var activitytypes = '';
          widget.json.params.forEach(function(param){
               if(param.name == 'type'){
                activitytypes = type;
               }
             });

           if(activitytypes == 'emailOpens')
                {
                     $scope.latestEmailOpen = !$scope.latestEmailOpen;
                }
                else if(activitytypes == 'emailSends')
                {
                      $scope.latestEmailSend = !$scope.latestEmailSend;
                }
                else if(activitytypes == 'emailClick')
                {
                        $scope.latestEmailClick = !$scope.latestEmailClick;
                }
                else if(activitytypes == 'webVisites')
                {
                         $scope.latestWebVisit = !$scope.latestWebVisit;
                }
                else if(activitytypes == 'formSubmited')
                {
                        $scope.latestFormSubmit = !$scope.latestFormSubmit;
                }
                else if(activitytypes == 'campaignMemberships')
                {
                        $scope.latestCampainMember = !$scope.latestCampainMember;
                }
          }
    }

     $rootScope.eloquaControlsAccountBased =  [
                  {
                    internalName: 'Activities Graph',
                    objectname: 'accountBasedActivityGraph',
                    image: 'images/activity_graph.svg',
                    type: 'eloqua',
                    urlkey: 'Latest_Acitivities',
                    params: [
                      { name: "fromdate", value: $scope.fromdate},
                      { name : "todate", value: $scope.todate},
                      { name : "type", value: 'emailOpen'}
                    ]
                  },
                  { internalName: 'Activity Breakdown',
                     objectname: 'accountBasedActivityBreakdown',
                     image: 'images/Activity.svg',
                     type: 'eloqua',
                     urlkey: 'Latest_Acitivities',
                     params: [
                       { name: "fromdate", value: $scope.fromdate},
                       { name : "todate", value: $scope.todate},
                       { name : "type", value: 'emailOpen'}
                     ] },
                     { internalName: 'Latest Activities',
                        objectname: 'accountBasedLatestActivity',
                        image : 'images/latest_activity.svg',
                        type: 'eloqua',
                        urlkey: 'Latest_Acitivities',
                        params:
                        [
                         { name: "fromdate", value: $scope.fromdate},
                         { name : "todate", value: $scope.todate},
                         { name : "type", value: 'emailOpen'}
                         ]
                       }
                      ];


     $rootScope.eloquaControls =  [
               {
                  internalName: 'Activities Graph',
                  objectname: '',
                  image: 'images/activity_graph.svg',
                  type: 'eloqua',
                  urlkey: 'Latest_Acitivities',
                  params: [
                    { name: "fromdate", value: $scope.fromdate},
                    { name : "todate", value: $scope.todate},
                    { name : "type", value: 'emailOpen'}
                  ],
				options: {
					legend: {
					  display: true,
					  position: 'bottom',
					  labels: {
						fontColor: "#000080",
					  }
					}
				   }
				  },
                  { internalName: 'Activity Breakdown',
                     objectname: '',
                     image: 'images/Activity.svg',
                     type: 'eloqua',
                     urlkey: 'Latest_Acitivities',
                     params: [
                       { name: "fromdate", value: $scope.fromdate},
                       { name : "todate", value: $scope.todate},
                       { name : "type", value: 'emailOpen'}
                     ] },
                     { internalName: 'Latest Activities',
                        objectname: '',
                        image : 'images/latest_activity.svg',
                        type: 'eloqua',
                        urlkey: 'Latest_Acitivities',
                        params:
                        [
                         { name: "fromdate", value: $scope.fromdate},
                         { name : "todate", value: $scope.todate},
                         { name : "type", value: 'emailOpen'}
                         ]},
                        // { internalName: 'Activity Drill Down',
                        // type: 'eloqua',
                           // objectname: '',
                           // image : 'images/tab.svg',
                           // params: []
                         // }
                        ];

						 $rootScope.eloquaAnalystics =  [
						   { internalName: 'Opportunities',
							  objectname: '',
							  params: [] },
							{ internalName: 'Events',
							   objectname: '',
							   params: [] } ,
						   { internalName: 'Leads',
							  objectname: '',
							  params: [] } ];
	       $scope.accountActivityGraph = [];
		  $rootScope.eloquaActivityTypes.forEach(function(type,index)
			{
				 var monthdata = new Array();
				 monthdata[0] = { x: "January", y: 0 };
				 monthdata[1] = { x:"February", y: 0};
				 monthdata[2] = { x: "March", y: 0 };
				 monthdata[3] = { x:"April", y: 0 };
				 monthdata[4] = { x:"May", y: 0 };
				 monthdata[5] = { x:"June", y: 0 };
				 monthdata[6] = { x:"July", y: 0 };
				 monthdata[7] = { x:"August", y: 0 };
				 monthdata[8] = { x:"September", y: 0 };
				 monthdata[9] = { x:"October", y: 0 };
				 monthdata[10] = { x:"November", y: 0 };
				 monthdata[11] = { x:"December", y: 0 };
                 $scope.accountActivityGraph.push({key: type, values: monthdata });
			});


    $rootScope.parentOpen = {};
    $rootScope.parentsecondaryopen = {};
    $rootScope.parentanalyticsopen = {};
    $scope.sortableOptions = {

     };

$scope.selected = [];

  $scope.options = {
   rowSelection: false,
    multiSelect: true,
    autoSelect: true,
    decapitate: false,
    largeEditDialog: false,
    boundaryLinks: true,
    limitSelect: true,
    pageSelect: true
  };

  $scope.query = {
    order: 'Name',
    limit: 5,
    page: 1
  };

     $scope.getListInfo = function(){
     var skip = ($scope.query.page - 1) * $scope.query.limit;
     Settings.find({where: {email: {regexp: /$scope.query.query/}}, skip: skip, limit: $scope.query.limit, order: $scope.query.order }, function(result){
       $scope.acldata = result;
     });
   }

     $scope.getuser = function(contactId){
       Account.findcontact({ id: $scope.accountId, contactId: contactId, type: $scope.urltype}, function(user){
         $scope.eloquauser = user.eloqua;
         $scope.contactId = user.eloqua.id;
         if(user.salesforce){
           $scope.salesforceuser = user.salesforce;
           $rootScope.salesforceuseravailable = true;
            $rootScope.parentobjects.forEach(function(objectname, index){
              $rootScope.getsalesforcefields(objectname, index);
            });
           $rootScope.$watchCollection('secondarydata', function(secondarydata){
             var secondarylength = Object.keys($rootScope.secondarydata);
             if(secondarylength.length >= $scope.parentobjects.length){
               $scope.getTabs($rootScope.settings.id);
             }
           });
         } else {
           $rootScope.loadingSalesForce  = false;
           $scope.getTabs($rootScope.settings.id);
         }
       })
     }


     $rootScope.eloquaquery = "";
     $rootScope.eloquafieldspage = 1;
     $rootScope.eloquafields = {
     };
     $scope.geteloquafields = function(query, page, objname){
       Account.contactfields({id: $scope.accountId, query: query, page: page, objname: objname},function(result){
         $rootScope.eloquafields[objname] = result.elements;
         $rootScope.eloquafields[objname].forEach(function(field){
             field.name = field.name;
             field.label = field.name;
             field.internalName = field.internalName;
             field.fullName = field.name + ' ('+ field.internalName + ' )';
             if(field.fullName.length > 26)
             {
              field.fullName = field.fullName.substr(0,22) +'...';
             }

         });
       });
       $rootScope.loadingEloqua = false;
     }

     $rootScope.eloquaParentobjects.forEach(function(objname){
      if(objname == 'Analytics' || objname == 'Contact Activities' || objname == 'Account Activities')
      {
         $rootScope.loadingEloqua = false;
      }
      else
      {
         $scope.geteloquafields("", 1, objname);
      }
      });





     $rootScope.getsalesforcefields = function(objectname, index){
       if(objectname){
         Objectjson.findOne({ "filter": {"where": {"type": "salesforce", "name": objectname }}}, function(object){
           $rootScope.salesforceobjectfields[object.name] = object.json.fields;
           $scope.getSalesforceParentData(objectname, index);
           if(object.json.childRelationships){
             $rootScope.salesforcechilds[object.name] = object.json.childRelationships;
             $rootScope.salesforcechilds[object.name].forEach(function(field)
             {
                field.name = field.childSObject
                field.fullName = field.childSObject.replace(/([a-z](?=[A-Z]))/g, '$1 ')  + '( ' +field.childSObject+' )';
                field.title = field.name ;
                if(field.fullName.length > 28)
                {
                  field.fullName = field.fullName.substr(0,25) + '...';
                }
             });
           }
           $rootScope.salesforceobjectfields[object.name].forEach(function(field)
           {
             field.objectname = object.name;
             field.id = field.name;
             field.label = field.label;
             field.name = field.name;
             field.parentName = field.label + " ( " + field.name + " )";
             field.title = field.parentName;
             field.chieldName = field.parentName;
             if(field.parentName.length > 30)
             {
               field.parentName = field.parentName.substr(0 ,27) + "..."
             }

             if(field.chieldName.length > 24)
             {
               field.chieldName = field.chieldName.substr(0 , 22) + "..."
             }

           });
         });
         $rootScope.loadingSalesForce  = false;
       }
     }





    $scope.resetdefault = function(){
       Settings.reset({id: $rootScope.settings.id }, function(){
         $scope.getTabs($rootScope.settings.id);
       });
     }



      $scope.revert = function(){
        Settings.revert({id: $rootScope.settings.id }, function(){
          $scope.getTabs($rootScope.settings.id);
        });
      }

       $scope.deleteWidgets = false;
       $scope.deleteTabs = false;
       $scope.widgetIndexForDelete = '';
       $scope.getIndexForDelete = function(index)
       {
         $scope.deleteWidgets = true;
         $scope.deleteTabs = false;
         $scope.widgetIndexForDelete = index;
       }

       $scope.deleteWidget = function()
       {
         $scope.removecolumn($scope.widgetIndexForDelete);
       }

       $scope.tab = '';
       $scope.getIndexForDeleteTab = function(tab , index)
       {
         $scope.deleteWidgets = false;
         $scope.deleteTabs = true;
         $scope.tabIndexForDelete = index;
         $scope.tab = tab;
       }

       $scope.deleteTab = function()
       {
         $scope.removeTab($scope.tab ,$scope.tabIndexForDelete);
       }

       $scope.removeTab = function(tab , index)
        {

          tab.widgets.forEach( function (widget ,index)
          {
             $scope.removecolumn(index);
          });

            Tab.removeById({id: tab.id} ,function (result) {
              $rootScope.alltabs.splice(index , 1);
            });

        }

   $scope.fieldselectsettings = {
       idProperty: "name",
       displayProp: "name"
     }

     $scope.tabchanged = function(index){

       $rootScope.tabindex = index;
     }

     $scope.onKeydown = function(event){
         event.stopPropagation();
      };


     $scope.multiselectevents = {
       onSelectionChanged : function(){
         $rootScope.alltabs[$rootScope.tabindex].widgets.forEach(function(widget){
           $rootScope.saveWidget(widget);
           $scope.getSecondaryData(widget);
         });
       }
     }



     $scope.getTabs = function(settingsId){
       Tab.find({filter: { "where": {"settingsId": settingsId}, "include": "widgets"}}, function(alltabs){
         $rootScope.alltabs = alltabs;
         $rootScope.alltabs.forEach(function(tab){
           tab.widgets.forEach(function(widget){
             if(widget.type == 'group box 1' || widget.type == 'group box 2' || widget.type == 'group box 3'){
               widget.json.properties.forEach(function(item){
                 item.forEach(function(property){
                     $scope.getfielddata(property.property);
                 });
               });
             } else {
               $scope.getSecondaryData(widget);
             }
           });
         });
       });
        $rootScope.loadingTabs = false;
     }

     $scope.getfielddata = function(property){
       if( property.source == 'salesforce' && $rootScope.secondarydata[property.objname] && $rootScope.secondarydata[property.objname].Id){
         var query = {id: $scope.accountId,  fields: property.fieldname, source: 'salesforce', object: property.objname, key: 'Id', value: $rootScope.secondarydata[property.objname].Id };
         query.salesforcecontactId = $scope.salesforceuser.Id;
         Account.secondaryproperty(query , function(result){
           if(result && result.records)
           {
             property.value = jsonPath(result.records[0], '$.' + property.fieldname);
           }
         });
       }
     }

     $scope.widgetdata = {};


     $scope.getSalesforceParentData = function(object, index){
          var query = {id: $scope.accountId, source: 'salesforce', object: object, key: 'Email', value: $scope.salesforceuser.Email };
           var queryfields = _.map($rootScope.salesforceobjectfields[object], 'name');
           query.fields = queryfields.join(',');
           query.salesforcecontactId = $scope.salesforceuser.Id;
           Account.secondaryproperty(query , function(result){
             if(result && result.records)
             {
               $rootScope.secondarydata[object] = result.records[0];
             }
           });
        }


     $scope.FilterContactActivity = function(type)
	 {
		     $scope.EmailOpenContact = [];
		     $scope.EmailSendContact = [];
		     $scope.EmailClickContact = [];
		     $scope.FormSubmitContact = [];
		     $scope.WebVisitContact = [];
		     $scope.CampainMemberContact = [];
		     $scope.latestActivitesContact = [];
		     $scope.eloquaActivityTypes = [];
			 if(type != '30 Days'&& type != '6 Months')
			 {
			 $scope.contactActivityGraph = [];
             $rootScope.eloquaActivityTypes.forEach(function(type , index)
		     {
			     var monthdata = new Array();
				 monthdata[0] = { x: "Jan", y: 0 };
				 monthdata[1] = { x:"Feb", y: 0};
				 monthdata[2] = { x: "Mar", y: 0 };
				 monthdata[3] = { x:"Apr", y: 0 };
				 monthdata[4] = { x:"May", y: 0 };
				 monthdata[5] = { x:"Jun", y: 0 };
				 monthdata[6] = { x:"Jul", y: 0 };
				 monthdata[7] = { x:"Aug", y: 0 };
				 monthdata[8] = { x:"Sept", y: 0 };
				 monthdata[9] = { x:"Oct", y: 0 };
				 monthdata[10] = { x:"Nov", y: 0 };
				 monthdata[11] = { x:"Dec", y: 0 };
				 $scope.contactActivityGraph.push({key: type, values: monthdata });
		      });
			 }
		   $scope.eloquaContactDataOnLoad.forEach(function(activity)
			{

			if(activity.activityDate <= $scope.todate && activity.activityDate >= $scope.fromdate)
			{
		     if(type == '30 Days')
			 {
				var key = new Date(activity.activityDate*1000).getMonth()+1+ '/'+ new Date(activity.activityDate*1000).getDate();
				//console.log('key before ' , $scope.contactActivityGraph);
				$scope.contactActivityGraph.forEach(function(value)
				{
					value[Object.keys(value)[1]].forEach(function(newValue)
					{
							console.log(value , ' ' , activity.activityType)
							if(newValue[Object.keys(newValue)[0]] == key && value[Object.keys(value)[0]] == activity.activityType)
							{
								newValue[Object.keys(newValue)[1]]++;
							}
					});


				});
			 }
			 else if(type == '6 Months')
			 {
				var key = month[new Date(activity.activityDate*1000).getMonth()];
				console.log('key before ' , $scope.contactActivityGraph);
				$scope.contactActivityGraph.forEach(function(value)
				{
					value[Object.keys(value)[1]].forEach(function(newValue)
					{
							console.log(value , ' ' , activity.activityType)
							if(newValue[Object.keys(newValue)[0]] == key && value[Object.keys(value)[0]] == activity.activityType)
							{
								newValue[Object.keys(newValue)[1]]++;
							}
					});


				});
			 }
			 else
			 {
				 var key = new Date(activity.activityDate*1000).getMonth();
				 $scope.contactActivityGraph[activity.index].values[parseInt(key)].y++;
			 }

			 activity.activityType = activity.activityType;
			 activity.activityDate = activity.activityDate;
			if(activity.activityType == 'emailOpen')
			{
				 activity.image = "/img/emailOpen.PNG";
			}
			else if(activity.activityType == 'emailSend')
			{
				  activity.image = "/img/emailSends.PNG";
			}
			else if(activity.activityType == 'emailClickThrough')
			{
					activity.image =  "/img/emailClicked.PNG";
			}
			else if(activity.activityType == 'webVisit')
			{
					 activity.image =   "/img/webVisited.PNG";
			}
			else if(activity.activityType == 'formSubmit')
			{
					activity.image = "/img/formSubmition.PNG";
			}
			else if(activity.activityType == 'campaignMembership')
			{
					activity.image =  "/img/externalActivities.PNG";
			}
			activity.details.forEach(function(details)
			{
			  if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
				|| details[Object.keys(details)[0]] == 'URL')
			  {
				activity.subjectLine = details[Object.keys(details)[1]];
			  }
			  else if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
				|| details[Object.keys(details)[0]] == 'URL')
			  {
				activity.name = details[Object.keys(details)[1]];
			  }

			  activity.emailAddress = activity.emailAddress;
			 if(details[Object.keys(details)[0]] == 'EmailClickedThruLink')
			  {
				 activity.link = details[Object.keys(details)[1]];
			  }
			  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailOpen')
			  {
				activity.name = details[Object.keys(details)[1]];


				  $scope.EmailOpenContact.push(activity);
			  }
			  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailSend')
			  {
				activity.name = details[Object.keys(details)[1]];
				$scope.EmailSendContact.push(activity);
			  }
			  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailClickThrough')
			  {

				activity.name = details[Object.keys(details)[1]];
				$scope.EmailClickContact.push(activity);
			  }
			  if(details[Object.keys(details)[0]] == 'URL' )
			  {
				activity.name = details[Object.keys(details)[1]];
				$scope.WebVisitContact.push(activity);
			  }
			  if(details[Object.keys(details)[0]] == 'FormName' )
			  {
				activity.name = details[Object.keys(details)[1]];
				$scope.FormSubmitContact.push(activity);
			  }
			  if(details[Object.keys(details)[0]] == "Collection")
			  {
				  activity.formSubmitionData = details[Object.keys(details)[1]];
				  activity.formSubmitionData = activity.formSubmitionData.split("elqdelimiter");
			  }

			});
			$scope.latestActivitesContact.push(activity);
			 console.log('loading....');
			}
		   });
	}


     $scope.filterActivityData = function()
	 {
		    $scope.latestActivites = [];
			$scope.emailOpend = [];
			$scope.emailSended = [];
			$scope.emailClicked = [];
			$scope.webVisited = [];
			$scope.formSubmits = [];
			$scope.campaignMember = [];
			var index = '';
			var contact = '';
			$scope.eloquaAccountDataOnLoad.forEach(function(activity)
			{
			    if(activity.activityDate <= $scope.todate && activity.activityDate >= $scope.fromdate)
				{
                 var key = new Date(activity.activityDate*1000).getMonth();
		         $scope.accountActivityGraph[activity.index].values[parseInt(key)].y++;
				 activity.activityType = activity.activityType;
                 activity.activityDate = activity.activityDate;

                if(activity.activityType == 'emailOpen')
                {
                     activity.image = "/img/emailOpen.PNG";
                }
                else if(activity.activityType == 'emailSend')
                {
                      activity.image = "/img/emailSends.PNG";
                }
                else if(activity.activityType == 'emailClickThrough')
                {
                        activity.image =  "/img/emailClicked.PNG";
                }
                else if(activity.activityType == 'webVisit')
                {
                         activity.image =   "/img/webVisited.PNG";
                }
                else if(activity.activityType == 'formSubmit')
                {
                        activity.image = "/img/formSubmition.PNG";
                }
                else if(activity.activityType == 'campaignMembership')
                {
                        activity.image =  "/img/externalActivities.PNG";
                }
                activity.details.forEach(function(details)
				{
                  if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.subjectLine = details[Object.keys(details)[1]];
                  }
                  else if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                  }

				  //activity.emailAddress = contact.emailAddress;
				 if(details[Object.keys(details)[0]] == 'EmailClickedThruLink')
				  {
					 activity.link = details[Object.keys(details)[1]];
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailOpen')
				  {
					activity.name = details[Object.keys(details)[1]];


					  $scope.emailOpend.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailSend')
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.emailSended.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailClickThrough')
				  {

					activity.name = details[Object.keys(details)[1]];
					$scope.emailClicked.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'URL' )
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.webVisited.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'FormName' )
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.formSubmits.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == "Collection")
				  {

				    activity.formSubmitionData = details[Object.keys(details)[1]];
					if(activity.formSubmitionData != null)
					  {
						  activity.formSubmitionData = activity.formSubmitionData.split("elqdelimiter");
					  }

				  }

                });
                $scope.latestActivites.push(activity);
				}
               });
			//});
	 }


     $scope.flagForaccountBasedActivity = true;
	 $scope.flagForcontactBasedActivity = true;
	 $scope.accountWidgetPresent = false;
	 $scope.contactWidgetPresent = false;
     $scope.getSecondaryData = function(widget)
	 {
       if(widget.type == 'list box')
       {
         if(widget.json.source == 'salesforce' && $scope.salesforceuser){
           widget.name = 'list - ' + widget.json.relationshipName;
           var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.childSObject, key: widget.json.objectname, value: $rootScope.secondarydata[widget.json.parent][widget.json.valuefield] };
           var queryfields = _.map(widget.json.fields, 'id');
           query.fields = queryfields.join(',');
           query.salesforcecontactId = $scope.salesforceuser.Id;
           Account.secondarydata(query, function(result){
             $scope.widgetdata[widget.id] = result.records;
           });
         }
         else if(widget.json.source == 'eloqua')
         {
           widget.name = widget.json.internalName;
           var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
           widget.json.params.forEach(function(param){
             query[param.name] = param.value;
           });
           Account.secondarydata(query, function(result){
             $scope.widgetdata[widget.id] = result;

           });
         }
       }
       else if (widget.type == 'count box')
       {
         widget.name = 'Count - ' + widget.json.relationshipName;
         var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.childSObject, key: widget.json.objectname, count: true, value: $rootScope.secondarydata[widget.json.parent][widget.json.valuefield]  };
         if(widget.json.source == 'salesforce' && $scope.salesforceuser){
           query.salesforcecontactId = $scope.salesforceuser.Id;
           Account.secondarydata(query, function(result){
             $scope.widgetdata[widget.id] = result.totalSize;
           });
         }
       }
       else if (widget.type == 'pie chart' || widget.type == 'line chart' || widget.type == 'bar chart') {
        if(widget.type == 'pie chart')
        {
          widget.name = widget.type + ' - ' + widget.json.fieldinfo.objname + " by " + widget.json.fieldinfo.name;
        } else {
          var fields = _.map(widget.json.fieldinfo, 'name');
          widget.name = widget.type + ' - ' + widget.json.fieldinfo[0].objname + " by " + fields.join(", ");
        }
         if(widget.json.source == 'salesforce' && $scope.salesforceuser)
         {
           if(widget.type == 'bar chart' || widget.type == 'line chart'){
             $scope.widgetdata[widget.id] = [];
             widget.json.options.chart.x = function(d){return d.expr0;};
             widget.json.options.chart.y = function(d){return d.expr1;};
             widget.json.options.chart.xAxis.axisLabel = 'Date';
             widget.json.options.chart.yAxis.axisLabel = 'Count';

             widget.json.fieldinfo.forEach(function(fieldinfo, index){
               var query = {id: $scope.accountId, source: widget.json.source, object: fieldinfo.objname, key: fieldinfo.objectname, value: $rootScope.secondarydata[fieldinfo.parent][fieldinfo.valuefield], groupby: fieldinfo.name, fieldtype: fieldinfo.type };
               query.interval = widget.json.interval;
               Account.chartdata(query, function(result){
                 if (widget.type == 'bar chart' || widget.type == 'line chart') {
                   $scope.widgetdata[widget.id][index] = {key: fieldinfo.name, values: result.records };
                 }
               });
             });
           } else {
             var query = {id: $scope.accountId, source: widget.json.source, object: widget.json.fieldinfo.objname, key: widget.json.fieldinfo.objectname, value: $rootScope.secondarydata[widget.json.fieldinfo.parent][widget.json.fieldinfo.valuefield] , groupby: widget.json.fieldinfo.name, fieldtype: widget.json.fieldinfo.type };
             Account.chartdata(query, function(result){
               if(widget.type == 'pie chart'){
                 $scope.widgetdata[widget.id] = result.records;
                 widget.json.options.chart.x = function(d){return d[widget.json.fieldinfo.name];};
                 widget.json.options.chart.y = function(d){return d.expr0;};
               }
             });
           }
         }
       }
       else if (widget.type == 'eloqua')
	   {
       if((widget.objectname == 'accountBasedLatestActivity' || widget.objectname == 'accountBasedActivityBreakdown' || widget.objectname == 'accountBasedActivityGraph' ) && $scope.flagForaccountBasedActivity)
		{
		   $scope.accountWidgetPresent = true;
		   $rootScope.loadingEloqua = true;
		   $scope.eloquaAccountDataOnLoad = [];
		   $rootScope.showDateFilter = true;
		   $scope.globalAccountWidget = widget;
		   $scope.latestActivites = [];
		   $scope.emailOpend = [];
		   $scope.emailSended = [];
		   $scope.emailClicked = [];
		   $scope.webVisited = [];
		   $scope.formSubmits = [];
		   $scope.campaignMember = [];

          var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
	      Account.accountBasedContacts(query, function(result)
		  {
			$rootScope.loadingEloqua = true;
		    var totalContact =  result.elements.length;
			var count = 1;
		   result.elements.forEach( function(contact)
		   {
			$rootScope.eloquaActivityTypes.forEach(function(type,index)
			{
			  $rootScope.loadingEloqua = true;
			  var query = {id: $scope.accountId, contactId: contact.id, source: widget.json.source, object: widget.json.urlkey };
				 widget.json.params.forEach(function(param){
				 if(param.name == 'type'){
					query[param.name] = type;
				   } else if(param.name == 'fromdate'){
					   query[param.name] = $scope.fromdate;
				   }
				   else if(param.name == 'todate')
				   {
					 query[param.name]= $scope.todate;
				   }
				 });
				 $rootScope.loadingEloqua = true;
			 Account.accountBasedAnalytics(query, function(response)
			 {

				//console.log(count++ , '  ' , totalContact*6);
				if(count++ == totalContact*6)
				{
					$rootScope.loadingEloqua = false;
				}
				else{
					console.log('loading...');
				}
               response.result.forEach(function(activity)
               {
				 activity.index = index;
				 $scope.eloquaAccountDataOnLoad.push(activity);
                 var key = new Date(activity.activityDate*1000).getMonth();
		         $scope.accountActivityGraph[index].values[parseInt(key)].y++;
				 activity.activityType = activity.activityType;
                 activity.activityDate = activity.activityDate;
                if(activity.activityType == 'emailOpen')
                {
                     activity.image = "/img/emailOpen.PNG";
                }
                else if(activity.activityType == 'emailSend')
                {
                      activity.image = "/img/emailSends.PNG";
                }
                else if(activity.activityType == 'emailClickThrough')
                {
                        activity.image =  "/img/emailClicked.PNG";
                }
                else if(activity.activityType == 'webVisit')
                {
                         activity.image =   "/img/webVisited.PNG";
                }
                else if(activity.activityType == 'formSubmit')
                {
                        activity.image = "/img/formSubmition.PNG";
                }
                else if(activity.activityType == 'campaignMembership')
                {
                        activity.image =  "/img/externalActivities.PNG";
                }
                activity.details.forEach(function(details)
				{
                  if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.subjectLine = details[Object.keys(details)[1]];
                  }
                  else if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                  }

				  activity.emailAddress = contact.emailAddress;
				 if(details[Object.keys(details)[0]] == 'EmailClickedThruLink')
				  {
					 activity.link = details[Object.keys(details)[1]];
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailOpen')
				  {
					activity.name = details[Object.keys(details)[1]];


					  $scope.emailOpend.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailSend')
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.emailSended.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailClickThrough')
				  {

					activity.name = details[Object.keys(details)[1]];
					$scope.emailClicked.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'URL' )
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.webVisited.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'FormName' )
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.formSubmits.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == "Collection")
				  {
					  activity.formSubmitionData = details[Object.keys(details)[1]];
					  if(activity.formSubmitionData != null)
					  {
					     activity.formSubmitionData = activity.formSubmitionData.split("elqdelimiter");
					  }

				  }

                });
                $scope.latestActivites.push(activity);
               });
              });

			 });

			});
		   });

		   $scope.flagForaccountBasedActivity = false;
		  }
		else if ( (widget.json.internalName == 'Activity Breakdown' || widget.json.internalName == 'Activities Graph'
		  || widget.json.internalName == 'Latest Activities' && $scope.flagForcontactBasedActivity) && (widget.objectname != 'accountBasedLatestActivity' && widget.objectname != 'accountBasedActivityBreakdown' && widget.objectname != 'accountBasedActivityGraph'))
		  {
		     $scope.contactWidgetPresent = true;
		   widget.name = widget.name;
		   $rootScope.showDateFilter = true;
		   widget.name = widget.name;
		   $scope.globalContactWidget = widget;
		   $scope.eloquaContactDataOnLoad = [];
           $scope.EmailOpenContact = [];
		   $scope.EmailSendContact = [];
		   $scope.EmailClickContact = [];
		   $scope.FormSubmitContact = [];
		   $scope.WebVisitContact = [];
		   $scope.CampainMemberContact = [];
		   $scope.latestActivitesContact = [];
		   $scope.contactActivityGraph = [];


           $rootScope.eloquaActivityTypes.forEach(function(type , index)
		   {
			   // var startDate1 = startDate;
			   // var endDate1 = endDate;

			   // console.log(startDate , endDate);
			   // for(var i = 0 ; startDate1 < endDate1 ; i++)
			    // {
				 // console.log(monthdata);
				  // monthdata[i] = { x: month[startDate1.getMonth()], y: 0 };
				  // startDate1.setMonth(startDate1.getMonth()+1);
			    // }

			     var monthdata = new Array();

				 monthdata[0] = { x: "Jan", y: 0};
				 monthdata[1] = { x:"Feb", y: 0 };
				 monthdata[2] = { x: "Mar", y: 0 };
				 monthdata[3] = { x:"Apr", y: 0 };
				 monthdata[4] = { x:"May", y: 0 };
				 monthdata[5] = { x:"Jun", y: 0 };
				 monthdata[6] = { x:"Jul", y: 0 };
				 monthdata[7] = { x:"Aug", y: 0 };
				 monthdata[8] = { x:"Sep", y: 0 };
				 monthdata[9] = { x:"Oct", y: 0 };
				 monthdata[10] = { x:"Nov", y: 0 };
				 monthdata[11] = { x:"Dec", y: 0 };
				 $scope.contactActivityGraph.push({key: type, values: monthdata });

				 var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
				 widget.json.params.forEach(function(param){
				   if(param.name == 'type'){
					 query[param.name] = type;
				   } else if(param.name == 'fromdate'){
					   query[param.name] = $scope.fromdate;
				   }
				   else if(param.name == 'todate')
				   {
					 query[param.name]= $scope.todate;
				   }

				 });
             Account.secondarydata(query, function(result)
			 {
			   result.result.forEach(function(activity)
                {
				 activity.index = index;
				 $scope.eloquaContactDataOnLoad.push(activity);
				 var key = new Date(activity.activityDate*1000).getMonth();
				 $scope.contactActivityGraph[index].values[parseInt(key)].y++;

				 activity.activityType = activity.activityType;
                 activity.activityDate = activity.activityDate;
                if(activity.activityType == 'emailOpen')
                {
                     activity.image = "/img/emailOpen.PNG";
                }
                else if(activity.activityType == 'emailSend')
                {
                      activity.image = "/img/emailSends.PNG";
                }
                else if(activity.activityType == 'emailClickThrough')
                {
                        activity.image =  "/img/emailClicked.PNG";
                }
                else if(activity.activityType == 'webVisit')
                {
                         activity.image =   "/img/webVisited.PNG";
                }
                else if(activity.activityType == 'formSubmit')
                {
                        activity.image = "/img/formSubmition.PNG";
                }
                else if(activity.activityType == 'campaignMembership')
                {
                        activity.image =  "/img/externalActivities.PNG";
                }
                activity.details.forEach(function(details)
				{
                  if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.subjectLine = details[Object.keys(details)[1]];
                  }
                  else if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                  }

				  activity.emailAddress = activity.emailAddress;
				 if(details[Object.keys(details)[0]] == 'EmailClickedThruLink')
				  {
					 activity.link = details[Object.keys(details)[1]];
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailOpen')
				  {
					activity.name = details[Object.keys(details)[1]];


					  $scope.EmailOpenContact.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailSend')
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.EmailSendContact.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailClickThrough')
				  {

					activity.name = details[Object.keys(details)[1]];
					$scope.EmailClickContact.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'URL' )
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.WebVisitContact.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'FormName' )
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.FormSubmitContact.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == "Collection")
				  {
					  activity.formSubmitionData = details[Object.keys(details)[1]];
					  activity.formSubmitionData = activity.formSubmitionData.split("elqdelimiter");
				  }

                });
                $scope.latestActivitesContact.push(activity);
               });
			   console.log('contactActivityGraph ' , $scope.contactActivityGraph);
              });

             });
            $scope.flagForcontactBasedActivity = false;
         }
       }
     }

	 $scope.getuser($scope.contactId || $scope.email);
      $scope.addcolumn = function(tabindex){
        $rootScope.column = {};
        var modalInstance = $uibModal.open({
                        templateUrl: 'views/partial/addcolumn.html',
                        size: 'md',
                        controller : 'AddColumnCtrl'
                    });
       }



       $scope.addtab = function(){
         $rootScope.tab = {};
         var modalInstance = $uibModal.open({
                         templateUrl: 'views/partial/addtab.html',
                         size: 'md',
                         controller : 'AddTabCtrl'
                     });
        }


       $scope.removecolumn = function(index){
         Widget.removeById({id: $rootScope.alltabs[$rootScope.tabindex].widgets[index].id}, function(removed){
           $rootScope.alltabs[$rootScope.tabindex].widgets.splice(index, 1);
         });
       };

       $scope.$on('angular-resizable.resizeEnd', function(e, args){
         if(args.width){
           $rootScope.alltabs[$rootScope.tabindex].widgets[args.id].width = args.width;
         }
         if(args.height){
           $rootScope.alltabs[$rootScope.tabindex].widgets[args.id].height = args.height;
         }
         $rootScope.saveWidget($rootScope.alltabs[$rootScope.tabindex].widgets[args.id]);
       });

       $scope.dropped = function(index, item, external, type, tabindex){
         item.width = 400;
         item.height = 200;
         item.name = 'untitled';
         item.json = {};

         if(item.type == 'group box 1'){
          item.name = 'Group box one column';
           item.json.properties = [[]];
           $rootScope.saveWidget(item);
         }else if(item.type == 'group box 2'){
          item.name = 'Group box two column';
           item.json.properties = [[],[]];
           $rootScope.saveWidget(item);
         }else if (item.type == 'group box 3') {
           item.name =  'Group box three column';
           item.json.properties = [[],[],[]];
           $rootScope.saveWidget(item);
         } else if(item.type == 'list box') {
           item.name =  'List box';
           $rootScope.saveWidget(item);
         }
         else if (item.type == 'eloqua') {
          item.width = 600;
         item.height = 500;
           item.name = item.internalName;
           item.json = {
             internalName: item.internalName,
             urlkey: item.urlkey,
             params: item.params,
             source: item.type
           };
           if(item.internalName == 'Activities Graph'){
			   item.width = 800;
               item.height = 400;
             item.json.interval = 'month';
             item.json.options = {
               chart: {
                   type: 'multiBarChart',
                   height: 280,
                   margin : {
                       top: .1,
                       right: 20,
                       bottom: 100,
                       left: 45
                   },
				  showControls:false,
				   legend:
					{
					  margin:
					  {
						top: 110
					  },

					},
                   clipEdge: true,
                   duration: 500,
                   stacked: true,
				   reduceXTicks:false,
                   xAxis: {
                       axisLabel: '',
                       showMaxMin: false
                   },
                   yAxis: {
                       axisLabel: '',
                       axisLabelDistance: -40
                   },

                },


			 }
           }
		   console.log('item ' , item);
           $rootScope.saveWidget(item);
         }
         else if (item.type == 'count box') {
           item.name =  'Count box';
           $rootScope.saveWidget(item);
         } if (item.type == 'line chart'){
               item.name =  'Line chart';
               item.json = {};
               item.json.interval = 'year';
               item.json.options = {
                       chart: {
                           type: 'lineChart',
                           height: 450,
                           margin : {
                               top: 20,
                               right: 20,
                               bottom: 40,
                               left: 55
                           },

                           xAxis: {
                               axisLabel: ''
                           },
                           yAxis: {
                               axisLabel: '',
                               axisLabelDistance: -10
                           }
                       },
                       title: {
                           enable: true,
                           text: ''
                       }
                   };
                   $rootScope.saveWidget(item);
             } else if (item.type == 'bar chart') {
                 item.name =  'Bar chart';
                 item.json = {};
                 item.json.interval = 'year';
                 item.json.options = {
                   chart: {
                       type: 'multiBarChart',
                       height: 450,
                       margin : {
                           top: 20,
                           right: 20,
                           bottom: 45,
                           left: 45
                       },
                       clipEdge: true,
                       duration: 500,
                       stacked: true,
                       xAxis: {
                           axisLabel: '',
                           showMaxMin: false
                       },
                       yAxis: {
                           axisLabel: '',
                           axisLabelDistance: -20
                       }
                   }
               };
               $rootScope.saveWidget(item);
             } else if (item.type == 'pie chart') {
               item.name =  'Pie chart';
               item.json = {};
               item.json.options =   {
                   chart: {
                       type: 'pieChart',
                       height: 500,
                       showLabels: true,
                       duration: 5,
                       labelThreshold: 0.01,
                       labelSunbeamLayout: true,
                       xAxis: {
                           axisLabel: ''
                       },
                       yAxis: {
                           axisLabel: ''
                       },
                       legend: {
                           margin: {
                               top: 5,
                               right: 35,
                               bottom: 5,
                               left: 0
                           }
                       }
                   }
               };
               $rootScope.saveWidget(item);
             } else if (item.type == 'tab') {
               var tab = {
                 name: "Tab",
                 description: "untitled"
               };
               $rootScope.savetab(tab);
             }
       }

	  $scope.droppedontabset = function(index, item, external, type, tabindex){
	  if (item.type == 'tab') {
			  var tab = {
				name: "Tab",
				description: "untitled"
			  };
			  $rootScope.savetab(tab);
			}
	  }

       $scope.droppedOnWidget = function(index, property, external, type, widget, subcolumnindex){
         if(widget.type == 'group box 2' || widget.type == 'group box 3' || widget.type == 'group box 1'){
           if(property.internalName){
             var internameArray = property.internalName.split('_');
             if(internameArray && internameArray[1]){
              property.internalName = internameArray[1].substring(0,1).toLowerCase()+internameArray[1].substring(1);
             }
             property.source = "eloqua";
             var data = {property: property, width: 30, height: 30};
             widget.json.properties[subcolumnindex].push(data);
           } else {
             property.source = "salesforce";
             var data = {property: property, width: 30, height: 30 };
             widget.json.properties[subcolumnindex].push(data);
             widget.json.properties.forEach(function(item){
               item.forEach(function(property){
                 $scope.getfielddata(property.property);
               });
             });
           }
         } else if (widget.type == 'count box') {
           if(property.internalName){
             property.source = "eloqua";
           } else {
             property.source = "salesforce";
           }
           widget.json = property;
           $scope.getSecondaryData(widget);
         } else if (widget.type == 'list box') {
           if(property.internalName){
             property.source = "eloqua";
           } else {
             property.source = "salesforce";
             property.fields = [$rootScope.salesforceobjectfields[property.childSObject][0],$rootScope.salesforceobjectfields[property.childSObject][1],$rootScope.salesforceobjectfields[property.childSObject][2]];
           }
           widget.json = property;
           $scope.getSecondaryData(widget);
         } else if (widget.type == 'pie chart' || widget.type == 'line chart' || widget.type == 'bar chart') {
            widget.json.source = property.source;
            if(widget.type == 'line chart' || widget.type == 'bar chart'){
              if(widget.json.fieldinfo){
                widget.json.fieldinfo.push(property);
              } else {
                widget.json.fieldinfo = [property];
              }
            } else {
              widget.json.fieldinfo = property;
            }
            $scope.getSecondaryData(widget);
         }
         $rootScope.saveWidget(widget);
       }
  }]);

'use strict';

/**
 * @ngdoc function
 * @name profilerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the profilerApp
 */
angular.module('profilerApp')
  .controller('SearchCtrl', ["$scope", "$rootScope", "$uibModal", "$uibModalStack", "$state", "$stateParams", "$location", "Account", function ($scope, $rootScope, $uibModal, $uibModalStack, $state, $stateParams,$location, Account) {
    var urlquery = $location.search();
    $scope.userId = urlquery.userId;
    $scope.siteId = urlquery.siteId;
    $scope.getAccount = function(siteId){
      Account.findOne({where:{ siteId: siteId}}, function(account){
         $scope.account = account;
         $scope.accountId = account.id;
         $scope.searchuser($scope.query, $scope.page);

      });
    }
    $scope.page = urlquery.page;
    $scope.total = 0;
    $scope.query = urlquery.query;
    $scope.maxSize = 10;
     $scope.searching = true;

    $scope.searchuser = function(query, page){
      Account.search({id: $scope.accountId, query: query, page: page}, function(result){
        $scope.searching = true;
        $scope.page = page;
        $scope.users = result.elements;
        $scope.total = result.total;
        console.log("$scope.users ",$scope.users);
        $scope.searching = false;
      });
    }
    $scope.profile = function(id){
      $state.go('home.tabs', {id: id, type: 'id', accountId: $scope.accountId, siteId: urlquery.siteId, userId: urlquery.userId})
    }
    $scope.getAccount($scope.siteId);

  }]);

angular.module('profilerApp').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/admin.html',
    "<selection> <md-toolbar class=\"crmToolBar\" style=\"background-color: #f2f4f5;min-height: 52px;height: 35px;border-bottom: 1px solid #ccc;position: fixed\"> <div class=\"md-toolbar-tools\"> <a href=\"http://www.portqii.com/\" target=\"_blank\" style=\"background:none\"> <img class=\"companyLogo\" src=\"/images/portQii1.png\"> </a> <h2 flex=\"\" md-truncate=\"\" style=\"color:#96999b;margin-left: 8px\">Sales Intelligence</h2> </div> <md-progress-linear md-mode=\"indeterminate\" ng-if=\"loadingEloqua\" style=\"margin-bottom: -3px !important\"></md-progress-linear> </md-toolbar> <md-content style=\"padding-top: 50px\"> <md-tabs md-dynamic-height md-border-bottom> <md-tab label=\"{{type}}\" ng-repeat=\"type in connectiontypes\"> <md-tab-label> <h4 ng-hide=\"tab.edit\" style=\"text-transform: capitalize;font-size: 16px\"> {{type}} </h4> </md-tab-label> <md-tab-body> <md-content class=\"md-padding\"> <div class=\"alert alert-success\" ng-if=\"alreadyAdded\"> ! User already added.</div> <div layout=\"row\"> <div layout=\"column\" ng-if=\"type == 'salesforce'\" flex=\"40\"> <form ng-submit=\"saveAccount(account)\"> <md-input-container ng-repeat=\"field in fields[type]\" class=\"md-block\"> <label style=\"font-size: 18px\">{{field.name | capitalize}}</label> <input ng-model=\"account.credentials[type][field.name]\" type=\"{{field.type}}\"> </md-input-container> <md-input-container class=\"md-block\"> <md-button type=\"submit\" class=\"md-raised md-primary\" style=\"margin-left: 0px;text-transform: capitalize\">Save</md-button> </md-input-container> </form> </div> <div ng-init=\"eloquafilter = false;\" layout=\"column\" flex=\"50\" ng-show=\"type == 'eloqua'\"> <div layout=\"row\"> <div layout=\"column\" flex=\"40\"> <form ng-submit=\"saveAccount(account)\"> <md-input-container class=\"md-block\"> <label style=\"font-size: 18px\">SFDC accountId field</label> <md-select ng-model=\"account.sfdcAccountIdField\"> <md-option ng-value=\"opt.internalName\" ng-repeat=\"opt in eloquacontactfileds\">{{ opt.name }}</md-option> </md-select> </md-input-container> <md-input-container class=\"md-block\"> <md-button type=\"submit\" class=\"md-raised md-primary\" style=\"margin-left: 0px;text-transform: capitalize\">Save</md-button> </md-input-container> </form> </div> </div> <md-toolbar class=\"md-table-toolbar\" ng-hide=\"eloquausersselected.length || eloquafilter\" aria-hidden=\"false\" style=\"min-height: 50px;height: 54px;background-color: rgb(242, 244, 245);color: black\"> <div class=\"md-toolbar-tools\"> <h2 class=\"md-title\">Eloqua Users</h2> <div flex=\"\" class=\"flex\"></div> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"eloquafilter = true\" aria-label=\"filter_list\"> <md-icon class=\"material-icons\" md-svg-src=\"images/filter_list.svg \" style=\"fill:black\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar md-default ng-hide\" ng-show=\"eloquafilter &amp;&amp; !eloquausersselected.length\" aria-hidden=\"true\" style=\"min-height: 50px;height: 54px;background-color: #f2f4f5\"> <div class=\"md-toolbar-tools\"> <!--  <md-icon class=\"material-icons\" style=\"position:  absolute;padding-top: 4px;font-size: 20px;padding-left: 4px;\">search</md-icon> --> <form flex=\"\" class=\"ng-pristine ng-valid flex\"> <input type=\"text\" ng-model=\"eloquaquery.query\" ng-change=\"searchusers(eloquaquery.query)\" placeholder=\"search\" class=\"form-control\" aria-invalid=\"false\" style=\"height: 26px; font-size: initial;width: 200px\"> </form> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"eloquafilter = false\" aria-label=\"close\"> <md-icon md-svg-src=\"images/clear.svg \" class=\"material-icons\" style=\"fill:red\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar alternate ng-hide\" ng-show=\"eloquausersselected.length\" aria-hidden=\"true\"> <div class=\"md-toolbar-tools layout-align-space-between-stretch\" layout-align=\"space-between\"> <div>0 item selected</div> </div> </md-toolbar> <!-- exact table from live demo --> <md-table-container style=\"background-color: white\"> <table md-table md-row-select multiple ng-model=\"eloquausersselected\" md-progress=\"promise\"> <thead md-head md-on-reorder=\"getEloquaUsers\"> <tr md-row> <th md-column style=\"border-bottom: 1px solid #ddd\">Email</th> <th md-column>First Name</th> <th md-column>Last Name</th> <th md-column>Add</th> </tr> </thead> <tbody md-body> <tr md-row md-select=\"item\" md-select-id=\"id\" md-auto-select ng-repeat=\"item in eloquausers\"> <td md-cell>{{item.emailAddress}}</td> <td md-cell>{{item.firstName}}</td> <td md-cell>{{item.lastName}}</td> <td md-cell> <button ng-click=\"adduser(item)\" class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" aria-label=\"add\"> <md-icon class=\"material-icons\" md-svg-src=\"images/add.svg \" style=\"fill: cornflowerblue;font-weight: bold\"></md-icon> <div class=\"md-ripple-container\"></div></button> </td> </tr> </tbody> </table> </md-table-container> <md-table-pagination md-limit=\"eloquaquery.limit\" md-limit-options=\"[5, 10, 15]\" md-page=\"eloquaquery.page\" md-total=\"{{eloquauserscount}}\" md-on-paginate=\"getEloquaUsers\" md-page-select style=\"background-color: white\"></md-table-pagination> </div> <div layout=\"column\" style=\"margin-left: 20px\" flex=\"50\" ng-show=\"type == 'eloqua'\"> <md-toolbar class=\"md-table-toolbar\" ng-hide=\"selected.length || filter.show\" aria-hidden=\"false\" style=\"min-height: 50px;height: 54px;background-color: rgb(242, 244, 245);color: black\"> <div class=\"md-toolbar-tools\"> <h2 class=\"md-title\">User Permissions</h2> <div flex=\"\" class=\"flex\"></div> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = true\" aria-label=\"filter_list\"> <md-icon class=\"material-icons\" md-svg-src=\"images/filter_list.svg \" style=\"fill:black\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar md-default ng-hide\" ng-show=\"filter.show &amp;&amp; !selected.length\" aria-hidden=\"true\" style=\"min-height: 50px;height: 54px;background-color: #f2f4f5\"> <div class=\"md-toolbar-tools\"> <!-- <md-icon class=\"material-icons\" style=\"position:  absolute;padding-top: 4px;font-size: 20px;padding-left: 4px;\">search</md-icon> --> <form flex=\"\" name=\"filter.form\" class=\"ng-pristine ng-valid flex\"> <input type=\"text\" ng-model=\"query.query\" ng-change=\"searchacl(query.query)\" placeholder=\"search\" class=\"form-control\" aria-invalid=\"false\" style=\"height: 26px; font-size: initial;width: 200px\"> </form> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = false\" aria-label=\"close\"> <md-icon class=\"material-icons\" md-svg-src=\"images/clear.svg \" style=\"fill:red\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar alternate ng-hide\" ng-show=\"selected.length\" aria-hidden=\"true\"> <div class=\"md-toolbar-tools layout-align-space-between-stretch\" layout-align=\"space-between\"> <div>0 item selected</div> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"delete()\" aria-label=\"delete\"> <md-icon class=\"material-icons\">delete</md-icon> </button> </div> </md-toolbar> <!-- exact table from live demo --> <md-table-container style=\"overflow: hidden;background-color: white\"> <table md-table md-row-select multiple ng-model=\"selected\" md-progress=\"promise\"> <thead md-head md-order=\"query.order\" md-on-reorder=\"getAcl\"> <tr md-row> <th md-column md-order-by=\"email\">Email</th> <th md-column md-order-by=\"userId\">User Id</th> <th md-column md-order-by=\"siteId\">Site Id</th> <th md-column md-order-by=\"created\">Created </th> <th md-column md-order-by=\"created\">Action</th> </tr> </thead> <tbody md-body> <tr md-row md-select=\"item\" md-select-id=\"id\" md-auto-select ng-repeat=\"item in acldata\"> <td md-cell title=\"{{item.title}}\">{{item.email}}</td> <td md-cell>{{item.userId}}</td> <td md-cell>{{item.siteId}}</td> <td md-cell style=\"padding-right: 42px\">{{item.created | date:'yyyy-MM-dd'}}</td> <td md-cell> <button ng-click=\"removeuser(item)\" class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" aria-label=\"close\"> <md-icon class=\"material-icons\" md-svg-src=\"images/clear.svg \" style=\"fill: red;font-weight: bold\"></md-icon> <div class=\"md-ripple-container\"></div></button> </td> </tr> </tbody> </table> </md-table-container> <md-table-pagination md-limit=\"query.limit\" md-limit-options=\"[5, 10, 15]\" md-page=\"query.page\" md-total=\"{{aclcount}}\" md-on-paginate=\"getAcl\" md-page-select style=\"background-color: white\"></md-table-pagination> </div> </div> </md-content> </md-tab-body> </md-tab> </md-tabs> </md-content> </selection>"
  );


  $templateCache.put('views/main.html',
    "<section layout=\"row\" flex=\"\" id=\"main\"> <md-toolbar class=\"crmToolBar\" style=\"background-color: #f2f4f5;min-height: 52px;height: 35px;border-bottom: 1px solid #ccc;position: fixed\"> <div class=\"md-toolbar-tools\"> <md-button class=\"md-icon-button\" ng-click=\"toggleMain();\" onclick=\"openNav('mainView')\" aria-label=\"Settings\"> <i class=\"fa fa-bars\" style=\"color: #0572ce;font-size: 21px;margin-top: 1px\"></i> </md-button> <a href=\"http://www.portqii.com/\" target=\"_blank\" style=\"background:none\"> <img class=\"companyLogo\" src=\"/images/portQii1.png\"> </a> <h2 flex=\"\" md-truncate=\"\" style=\"color:#96999b;margin-left: 8px\">Sales Intelligence</h2>  <span style=\"font-size:15px;opacity:.6;color:black\" ng-if=\"showDateFilter && !showAccountCustomDate\"> {{fromdate*1000 | date}} to {{todate*1000 | date}} </span> <div ng-show=\"showAccountCustomDate \" style=\"height: 32px\"> <md-datepicker ng-model=\"myStartDate\" md-placeholder=\"Start date\" md-open-on-focus=\"\" ng-init=\"myStartDate = null\" md-max-date=\"maxDate\"></md-datepicker> <span class=\"pull-right\" style=\"margin-top: -1px\"> <md-datepicker ng-model=\"myEndDate\" md-placeholder=\"End date\" md-open-on-focus=\"\" ng-init=\"myEndDate = null\" md-max-date=\"maxDate\"></md-datepicker> <md-button style=\"height: 23px;margin-top: 0px;min-width:  46px;margin-bottom:  0px;background-color: #f2f4f5;min-height: 25px\" ng-click=\"filterByCustomDate(myStartDate , myEndDate)\" ng-disabled=\"myStartDate == null && myEndDate == null\"> <md-icon class=\"\" md-svg-icon=\"images/filter_icon.svg\" style=\"min-width: 20px;width:  20px;margin-top: -10px;fill:black\"></md-icon> </md-button> </span> </div> <md-menu-bar ng-if=\"showDateFilter\" style=\"padding:0px\"> <md-menu> <button ng-click=\"$mdMenu.open()\"> <i class=\"fa fa-calendar\" style=\"color:#0572ce;font-size: 23px\" aria-hidden=\"true\"></i> </button> <md-menu-content style=\"min-width: 160px\"> <md-menu-item> <md-button ng-click=\"$mdMenu.close();filterByDate('30 Days')\"> 30 Days </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=\"$mdMenu.close();filterByDate('6 Months')\"> 6 Months </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=\"$mdMenu.close();filterByDate('1 Year')\"> 1 Year </md-button> </md-menu-item> <md-menu-item> <md-button ng-click=\"filterByDate('Custom Date')\"> Custom Date </md-button> </md-menu-item> </md-menu-content> </md-menu> </md-menu-bar> </div> <md-progress-linear md-mode=\"indeterminate\" ng-if=\"loadingSalesForce || loadingEloqua || loadingTabs\" style=\"margin-bottom: -3px !important;height:4px\"></md-progress-linear> </md-toolbar> <md-sidenav class=\"md-sidenav-left\" md-component-id=\"mainView\" md-disable-backdrop=\"false\" md-whiteframe=\"4\" style=\"position:fixed;background-color: #313334;width: 200px\" align=\"begin\"> <md-content layout-margin=\"\"> <div layout=\"column\" layout-align=\"top left\" style=\"margin: 0px;background-color: #313334\"> <span flex=\"100\"></span> <md-button aria-label=\"\" class=\"md-icon-button\" ng-click=\"toggleMain()\" onclick=\"closeNav('Main')\"> <md-icon md-svg-src=\"images/clear.svg\" style=\"fill: #c0c4c8\"></md-icon> </md-button> </div> <aside class=\"off-sidebar off-sidebar-left clearfix\" style=\"background-color: #313334;margin: 0px\"> <ul class=\"nav nav-sidebar nav-stacked main-menu\" class=\"mainUL\"> <li> <a ng-click=\"toggleLeft();\" onclick=\"openNav('left')\" style=\"cursor: pointer\"> <md-icon md-svg-src=\"images/dataModel.svg\" style=\"fill: #c0c4c8\"></md-icon> <span class=\"text\" style=\"margin-left: 15px\"> Data Model </span> </a> </li> <li> <a ng-click=\"toggleRight(); \" onclick=\"openNav('right')\" style=\"cursor: pointer\"> <md-icon md-svg-src=\"images/settings.svg\" style=\"fill: #c0c4c8\"></md-icon> <span class=\"text\" style=\"margin-left: 15px\"> Controls </span> </a> </li> <li> <a style=\"cursor: pointer\"> <md-switch class=\"md-primary\" md-no-ink=\"\" ng-change=\"showHideIcon();\" aria-label=\"Switch No Ink\" ng-model=\"edit\" style=\"margin: 0px ; height: 16px; cursor: pointer\">Edit </md-switch> </a> </li> <li> <a ng-click=\"toggleMain();resetdefault()\" onclick=\"closeNav('Main')\" style=\"cursor: pointer\"> <md-icon md-svg-src=\"images/reset.svg\" style=\"fill: #c0c4c8\"></md-icon> <span class=\"text\" style=\"margin-left: 15px\"> Reset </span> </a> </li> </ul></aside> </md-content> </md-sidenav> <md-sidenav class=\"md-sidenav-left\" md-component-id=\"left\" md-disable-backdrop=\"false\" md-whiteframe=\"4\" style=\"position:fixed;background-color: #313334;width: 330px;min-width: 300px\" align=\"begin\"> <md-content layout-margin=\"\" style=\"background-color: #313334\"> <div layout=\"column\" layout-align=\"top right\" style=\"margin: 0px\"> <md-button aria-label=\"\" class=\"md-icon-button\" ng-click=\"toggleLeft()\" onclick=\"closeNav('left')\"> <md-icon md-svg-src=\"images/clear.svg\" style=\"fill: #c0c4c8\"></md-icon> </md-button> </div> <md-tabs md-dynamic-height=\"\" md-border-bottom=\"\"> <md-tab ng-if=\"salesforceuseravailable\"> <md-tab-label> <h4 style=\"text-transform: capitalize;font-size: 16px;color: #c0c4c8\"> Salesforce </h4> </md-tab-label> <md-tab-body> <md-content class=\"md-padding\" style=\"overflow: hidden;padding-right: 0px;padding-left: 0px;background-color: #313334;color: #c0c4c8\"> <script type=\"text/ng-template\" id=\"field.html\"><md-list style=\"margin-bottom: -40px;padding-top: 0px;\" ng-if=\"node.open\" flex ng-init=\"parentnode = node;\">\n" +
    "               <div class=\"contactNode\"   style=\"padding-left: 55px;padding-bottom: 15px\"  ng-repeat= \"node in salesforceobjectfields[node.referenceTo[0]] | filter:salesforcequery\"  ng-init=\"node.fieldname = parentnode.parentfieldname + '.' + node.name; node.objectname = parentnode.objectname + '.' + node.referenceTo[0]; node.objname = parent; node.parentfieldname =  parentnode.parentfieldname + '.' + node.relationshipName  || node.name; node.wherefield =  node.relationshipName + '.' + parentnode.wherefield; node.valuefield = parentnode.valuefield;\" >\n" +
    "                 <md-icon  ng-click=\"node.open = true; getsalesforcefields(node.referenceTo[0]);\" md-svg-src=\"images/add.svg \" class=\"parentPlusIcon\" ng-if = 'node.referenceTo[0] && node.referenceTo[0].length > 0 && !node.open' style='margin-top: -2px;margin-left: -28px;'>\n" +
    "                  </md-icon>\n" +
    "                    <md-icon  ng-click=\"node.open = false\" md-svg-src=\"images/remove.svg \" class=\"parentMinusIcon\" ng-if = 'node.referenceTo[0] && node.referenceTo[0].length > 0 && node.open' style='margin-left: -28px;'>\n" +
    "                 </md-icon>\n" +
    "                    <span dnd-draggable=\"node\"  dnd-type=\"node.type\" title=\"{{node.title}}\" dnd-callback=\"true\">\n" +
    "                          {{node.parentName }}\n" +
    "                    </span>\n" +
    "                    <md-list ng-if=\"node.open\" ng-include=\"'field.html'\" ng-init=\"getsalesforcefields(node.reference[0]);\" >\n" +
    "\n" +
    "\n" +
    "                    </md-list>\n" +
    "               </div>\n" +
    "\n" +
    "               <md-list style = \"margin-left: -4px;padding-top: 0px\">\n" +
    "               <md-list-item ng-init=\"object.childanalyticsopen = false; object.objname = object.childSObject; object.parent = parent; object.objectname = parentnode.wherefield; object.valuefield = parentnode.valuefield;\" ng-if=\"node.open\" ng-repeat=\"object in salesforcechilds[node.referenceTo[0]] | filter:salesforcequery\">\n" +
    "                   <div  dnd-draggable=\"object\" dnd-callback=\"true\" style=\"padding-left: 17px;\" title=\"{{object.title}}\" >\n" +
    "                     <md-icon md-svg-src=\"images/add.svg \" class=\"chieldPlusIcon\" ng-if = '!object.childanalyticsopen' ng-click=\"object.childanalyticsopen = !object.childanalyticsopen\" style='margin-top: -2px;'>\n" +
    "                     </md-icon>\n" +
    "                     <md-icon ng-if='object.childanalyticsopen' ng-click=\"object.childanalyticsopen = !object.childanalyticsopen\" md-svg-src=\"images/remove.svg\" class=\"chieldMinusIcon\">\n" +
    "                     </md-icon>\n" +
    "                       {{object.fullName}}\n" +
    "                      <md-list-item ng-show=\"object.childanalyticsopen\" dnd-draggable=\"field\" dnd-callback=\"true\"  ng-repeat=\"field in salesforceobjectfields[object.childSObject]\" ng-if=\"field.type == 'picklist' || field.type == 'datetime'\"  style=\"padding-left: 40px\" >\n" +
    "                       <span ng-if=\"field.type == 'picklist'\"  ng-init=\"field.parent = parent; field.source = 'salesforce'; field.field = object.field; field.objname = object.childSObject; field.widgettype = 'chart'; field.objectname =  parentnode.wherefield; field.valuefield = parentnode.valuefield;\" title=\"by{{field.title}}\">\n" +
    "                       by {{field.chieldName}}\n" +
    "                       </span>\n" +
    "                       <span  ng-if=\"field.type == 'datetime'\"  ng-init=\"field.parent = parent; field.source = 'salesforce';field.field = object.field; field.objname = object.childSObject; field.widgettype = 'chart'; field.interval = 'year'; field.objectname = parentnode.wherefield; field.valuefield = parentnode.valuefield;\" title=\"by{{field.title}}\">\n" +
    "                       by {{field.chieldName}}\n" +
    "                       </span>\n" +
    "                     </md-list-item>\n" +
    "                 </div>\n" +
    "              </md-list-item>\n" +
    "             </md-list>\n" +
    "              </md-list></script> <input type=\"text\" class=\"form-control\" ng-model=\"salesforcequery\" placeholder=\"Search...\" style=\"height: 24px\"> <md-list ng-repeat=\"parent in parentobjects\" ng-init=\"parentOpen[parent] = false\"> <div style=\"padding-top: 15px;padding-bottom: 15px\"> <md-icon md-svg-src=\"images/add.svg \" class=\"parentPlusIcon\" ng-if=\"!parentOpen[parent]\" style=\"margin-top: -2px\" ng-click=\"parentOpen[parent] = !parentOpen[parent]\"></md-icon> <md-icon ng-if=\"parentOpen[parent]\" ng-click=\"parentOpen[parent] = !parentOpen[parent]\" md-svg-src=\"images/remove.svg\" class=\"parentMinusIcon\" style=\"fill: white\"></md-icon> {{parent | capitalize}}</div> <md-list style=\"margin-bottom: -40px;padding-top: 0px\" flex> <div class=\"contactNode\" ng-if=\"parentOpen[parent]\" style=\"padding-left: 55px;padding-bottom: 15px\" ng-repeat=\"node in salesforceobjectfields[parent] | filter:salesforcequery\" ng-init=\"node.open = false; node.fieldname = node.name; node.objectname = node.referenceTo[0]; node.objname = parent;  node.parentfieldname = node.relationshipName || node.name; node.wherefield = node.name; node.valuefield = node.name;\"> <md-icon ng-click=\"node.open = true; getsalesforcefields(node.referenceTo[0]);\" md-svg-src=\"images/add.svg \" class=\"parentPlusIcon\" ng-if=\"node.referenceTo[0] && node.referenceTo[0].length > 0 && !node.open\" style=\"margin-top: -2px;margin-left: -28px\"> </md-icon> <md-icon ng-click=\"node.open = false\" md-svg-src=\"images/remove.svg \" class=\"parentMinusIcon\" ng-if=\"node.referenceTo[0] && node.referenceTo[0].length > 0 && node.open\" style=\"margin-left: -28px\"> </md-icon> <span dnd-draggable=\"node\" dnd-type=\"node.type\" title=\"{{node.title}}\" dnd-callback=\"true\"> {{node.parentName}} </span> <md-list ng-if=\"node.open\" ng-include=\"'field.html'\"> </md-list> </div> <md-list style=\"margin-left: -4px;padding-top: 0px\"> <md-list-item ng-init=\"object.childanalyticsopen = false; object.objname = object.childSObject; object.parent = parent; object.objectname = object.field; object.valuefield = 'Id';\" ng-show=\"parentOpen[parent]\" ng-repeat=\"object in salesforcechilds[parent] | filter:salesforcequery\"> <div dnd-draggable=\"object\" dnd-callback=\"true\" style=\"padding-left: 17px\" title=\"{{object.title}}\"> <md-icon md-svg-src=\"images/add.svg \" class=\"chieldPlusIcon\" ng-if=\"!object.childanalyticsopen\" ng-click=\"object.childanalyticsopen = !object.childanalyticsopen\" style=\"margin-top: -2px\"> </md-icon> <md-icon ng-if=\"object.childanalyticsopen\" ng-click=\"object.childanalyticsopen = !object.childanalyticsopen\" md-svg-src=\"images/remove.svg\" class=\"chieldMinusIcon\"> </md-icon> <span dnd-draggable=\"object\" title=\"{{object.title}}\" dnd-callback=\"true\" dnd-type=\"'Object'\"> {{object.fullName}} <md-list-item ng-show=\"object.childanalyticsopen\" dnd-draggable=\"field\" dnd-callback=\"true\" ng-repeat=\"field in salesforceobjectfields[object.childSObject]\" ng-if=\"field.type == 'picklist' || field.type == 'datetime'\" style=\"padding-left: 40px\"> <span ng-if=\"field.type == 'picklist'\" ng-init=\"field.parent = parent; field.source = 'salesforce'; field.field = object.field; field.objname = object.childSObject; field.widgettype = 'chart'; field.objectname = object.field; field.valuefield = 'Id'\" title=\"by{{field.title}}\"> by {{field.chieldName}} </span> <span ng-if=\"field.type == 'datetime'\" ng-init=\"field.parent = parent; field.source = 'salesforce';field.field = object.field; field.objname = object.childSObject; field.widgettype = 'chart';field.interval = 'year'; field.objectname = object.field; field.valuefield = 'Id'\" title=\"by{{field.title}}\"> by {{field.chieldName}} </span> </md-list-item> </span></div> </md-list-item> </md-list> </md-list> </md-list> </md-content> </md-tab-body> </md-tab> <md-tab> <md-tab-label> <h4 style=\"text-transform: capitalize;font-size: 16px;color: #c0c4c8\"> Eloqua </h4> </md-tab-label> <md-tab-body> <md-content class=\"md-padding\" style=\"overflow: hidden;padding-right: 0px;padding-left: 0px;background-color: #313334;color: #c0c4c8\"> <input type=\"text\" class=\"form-control\" ng-model=\"eloquaquery\" placeholder=\"Search...\" style=\"height: 24px\"> <md-list ng-repeat=\"parent in eloquaParentobjects\" ng-init=\"parentOpen[parent] = false\"> <div style=\"padding-top: 15px;padding-bottom: 15px\"> <md-icon md-svg-src=\"images/add.svg \" class=\"parentPlusIcon\" ng-if=\"!parentOpen[parent]\" style=\"margin-top: -2px\" ng-click=\"parentOpen[parent] = !parentOpen[parent]\"></md-icon> <md-icon ng-if=\"parentOpen[parent]\" ng-click=\"parentOpen[parent] = !parentOpen[parent]\" md-svg-src=\"images/remove.svg\" class=\"parentMinusIcon\"></md-icon> {{parent | capitalize}}</div> <md-list style=\"margin-bottom: -40px;padding-top: 0px\" flex> <div ng-if=\"parentOpen[parent] && parent == 'Analytics'\" ng-repeat=\"field in eloquaAnalystics\" style=\"padding-left: 50px\"> <span dnd-draggable=\"field\" ng-init=\"field.label = '{{field}}'\" title=\"\" dnd-callback=\"true\" dnd-type=\"'Node'\"> {{field.internalName}} </span> <md-list> </md-list> </div> <div ng-if=\"parentOpen[parent] && parent == 'Contact Activities'\" style=\"padding-left: 50px\"> <ul class=\"nav nav-sidebar nav-stacked main-menu\"> <li ng-repeat=\"item in eloquaControls\"> <a dnd-draggable=\"item\" dnd-type=\"'tabs'\" dnd-callback=\"true\"> <md-icon md-svg-src=\"{{item.image}}\" style=\"fill: #c0c4c8\"></md-icon> <span class=\"text\" style=\"margin-left: 15px\"> {{item.internalName}} </span> </a> </li> </ul> <md-list> </md-list> </div> <div ng-if=\"parentOpen[parent] && parent == 'Account Activities'\" style=\"padding-left: 50px\"> <ul class=\"nav nav-sidebar nav-stacked main-menu\"> <li ng-repeat=\"item in eloquaControlsAccountBased\"> <a dnd-draggable=\"item\" dnd-type=\"'tabs'\" dnd-callback=\"true\"> <md-icon md-svg-src=\"{{item.image}}\" style=\"fill: #c0c4c8\"></md-icon> <span class=\"text\" style=\"margin-left: 15px\"> {{item.internalName}} </span> </a> </li> </ul> <md-list> </md-list> </div> <div class=\"contactNode\" ng-if=\"parentOpen[parent]\" style=\"padding-left: 50px\" ng-repeat=\"field in eloquafields[parent] | filter:eloquaquery\"> <span dnd-draggable=\"field\" title=\"{{field.name + ' ( '+ field.internalName +' )'}}\" dnd-callback=\"true\" dnd-type=\"'Node'\"> {{field.fullName }} </span> <md-list> </md-list> </div> </md-list> </md-list> </md-content> </md-tab-body> </md-tab></md-tabs></md-content></md-sidenav> <md-sidenav class=\"md-sidenav-left\" md-component-id=\"right\" md-disable-backdrop=\"false\" md-whiteframe=\"4\" style=\"position:fixed;margin-top: 0px;background-color: #313334;width: 270px\" align=\"begin\"> <md-content layout-margin=\"\" style=\"background-color: #313334\"> <div layout=\"column\" layout-align=\"top right\" style=\"margin: 0px\"> <md-button aria-label=\"\" class=\"md-icon-button\" ng-click=\"toggleRight()\" onclick=\"closeNav('right')\"> <md-icon md-svg-src=\"images/clear.svg\" style=\"fill: #c0c4c8\"></md-icon> </md-button> </div> <ul class=\"nav nav-sidebar nav-stacked main-menu\"> <li> <a dnd-draggable=\"{type: 'tab'}\" dnd-type=\"'tabs'\" dnd-callback=\"true\"> <md-icon md-svg-src=\"images/tab.svg\" style=\"fill: #c0c4c8\"></md-icon> <span class=\"text\" style=\"margin-left: 15px\"> Tab </span> </a> </li> <li> <a dnd-effect-allowed=\"move\" dnd-type=\"'widgets'\" dnd-draggable=\"{type: 'group box 1'}\" dnd-callback=\"true\"> <md-icon md-svg-src=\"images/groupbox1_chart.svg\" title=\"Group Box (1 column)\" style=\"fill: #c0c4c8\"></md-icon> <span class=\"text\" style=\"margin-left: 15px\"> Group Box (1 column) </span> </a> </li> <li> <a dnd-draggable=\"{type: 'group box 2'}\" dnd-callback=\"true\" dnd-type=\"'widgets'\"> <md-icon md-svg-src=\"images/groupbox2_chart.svg\" title=\"Group Box (2 column)\" style=\"fill: #c0c4c8\"></md-icon> <span class=\"text\" style=\"margin-left: 15px\"> Group Box (2 column) </span> </a> </li> <li> <a dnd-draggable=\"{type: 'group box 3'}\" dnd-callback=\"true\" dnd-type=\"'widgets'\"> <md-icon md-svg-src=\"images/groupbox_chart.svg\" title=\"Group Box (3 column)\" style=\"fill: #c0c4c8\"></md-icon> <span class=\"text\" style=\"margin-left: 15px\"> Group Box (3 column) </span> </a> </li> <li> <a dnd-draggable=\"{type: 'list box'}\" dnd-callback=\"true\" dnd-type=\"'widgets'\"> <md-icon md-svg-src=\"images/list_chart.svg\" style=\"fill: #c0c4c8\"></md-icon> <span class=\"text\" style=\"margin-left: 15px\"> List Box </span> </a> </li> <li> <a dnd-draggable=\"{type: 'pie chart'}\" dnd-callback=\"true\" dnd-type=\"'widgets'\"> <md-icon md-svg-src=\"images/pie_chart.svg\" style=\"fill: #c0c4c8\"></md-icon> <span class=\"text\" style=\"margin-left: 15px\"> Pie Chart </span> </a> </li> <li> <a dnd-draggable=\"{type: 'line chart'}\" dnd-callback=\"true\" dnd-type=\"'widgets'\"> <md-icon md-svg-src=\"images/line_chart.svg\" style=\"fill: #c0c4c8\"> </md-icon> <span class=\"text\" style=\"margin-left: 15px\"> Line Chart </span> </a> </li> <li> <a dnd-draggable=\"{type: 'bar chart'}\" dnd-callback=\"true\" dnd-type=\"'widgets'\"> <md-icon md-svg-src=\"images/bar-chart.svg\" style=\"fill: #c0c4c8\"> </md-icon> <span class=\"text\" style=\"margin-left: 15px\"> Bar Chart </span> </a> </li> <li> <a dnd-draggable=\"{type: 'count box'}\" dnd-callback=\"true\" dnd-type=\"'widgets'\"> <span style=\"color: #c0c4c8\"> 123 </span> <span class=\"text\" style=\"margin-left: 15px\"> Count Box </span> </a> </li> <!-- <li>\n" +
    "            <a dnd-draggable=\"{type: 'activity breakdown'}\" dnd-callback=\"true\" dnd-type=\"'widgets'\">\n" +
    "                <md-icon md-svg-src=\"images/Activity.svg\"  style=\"fill: #c0c4c8\">\n" +
    "                </md-icon>\n" +
    "            <span class=\"text\" style=\"margin-left: 15px;\" dnd-type=\"'widgets'\">\n" +
    "            Activity Breakdown\n" +
    "          </span>\n" +
    "        </a>\n" +
    "        </li>\n" +
    "        <li>\n" +
    "            <a dnd-draggable=\"{type: 'latest activities'}\" dnd-callback=\"true\" dnd-type=\"'widgets'\">\n" +
    "                <md-icon md-svg-src=\"images/latest_activity.svg\"  style=\"fill: #c0c4c8\">\n" +
    "                </md-icon>\n" +
    "            <span class=\"text\" style=\"margin-left: 15px;\" dnd-type=\"'widgets'\">\n" +
    "            Latest Activities\n" +
    "          </span>\n" +
    "        </a>\n" +
    "        </li> --> </ul> </md-content> </md-sidenav> <md-content flex=\"\" layout-padding=\"\" style=\"margin-top: 25px\"> <md-progress-linear ng-if=\"loading\" md-mode=\"indeterminate\"></md-progress-linear> <div layout=\"row\" flex=\"100\" ui-view style=\"height: 100%\"> </div> </md-content> </section> <script>function openNav(id) {\n" +
    "   if(id == 'left')\n" +
    "    {\n" +
    "          if($(\"#main\").css('marginLeft') == \"0px\" || $(\"#main\").css('marginLeft') == \"200px\")\n" +
    "          {\n" +
    "\n" +
    "            document.getElementById(\"main\").style.marginLeft = \"330px\";\n" +
    "          }\n" +
    "          else\n" +
    "          {\n" +
    "            document.getElementById(\"main\").style.marginLeft  = \"0px\";\n" +
    "          }\n" +
    "\n" +
    "    }\n" +
    "    else if(id  == 'mainView')\n" +
    "    {\n" +
    "       if($(\"#main\").css('marginLeft') == \"0px\")\n" +
    "          {\n" +
    "            document.getElementById(\"main\").style.marginLeft = \"200px\";\n" +
    "          }\n" +
    "          else\n" +
    "          {\n" +
    "            document.getElementById(\"main\").style.marginLeft  = \"0px\";\n" +
    "          }\n" +
    "    }\n" +
    "    else\n" +
    "    {\n" +
    "       document.getElementById(\"main\").style.marginLeft  = \"270px\";\n" +
    "    }\n" +
    "}\n" +
    "\n" +
    "/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */\n" +
    "function closeNav(id)\n" +
    "{\n" +
    "  if(id == 'Main')\n" +
    "  {\n" +
    "    document.getElementById(\"main\").style.marginLeft = \"0px\";\n" +
    "  }\n" +
    "  else\n" +
    "  {\n" +
    "    document.getElementById(\"main\").style.marginLeft = \"200px\";\n" +
    "  }\n" +
    "\n" +
    "}</script>"
  );


  $templateCache.put('views/search.html',
    "<section layout=\"row\" flex=\"\" id=\"main\"> <md-toolbar class=\"crmToolBar\" style=\"background-color: #f2f4f5;min-height: 52px;height: 35px;border-bottom: 1px solid #ccc;position: fixed\"> <div class=\"md-toolbar-tools\"> <a href=\"http://www.portqii.com/\" target=\"_blank\" style=\"background:none\"> <img class=\"companyLogo\" src=\"/images/portQii1.png\"> </a> <h2 flex=\"\" md-truncate=\"\" style=\"color:#96999b;margin-left: 8px\">Sales Intelligence</h2> </div> </md-toolbar> <md-content style=\"width: 100%;padding-top: 52px\"> <div style=\"border-bottom: 1px solid rgb(204, 204, 204); padding: 10px 15px; background-color: rgb(255, 255, 255)\"><h4 class=\"page-header-name\">Contacts </h4> </div>  <div> <div flex-gt-sm=\"100\" flex=\"\" style=\"padding: 0px 12px 8px 12px\"> <div> <form id=\"section-search-form\" class=\"navbar-form\" ng-submit=\"searchuser(query, page);searching  = true\" role=\"search\" novalidate style=\"max-width: 500px;padding-left: 0px\"> <div class=\"form-group has-icon\"> <i class=\"fa fa-search form-control-icon input-lg\" style=\"position :absolute;color: #878C90;font-size: 14px;margin-top: 4px\"></i> <input type=\"text\" id=\"section_search\" ng-model=\"query\" class=\"form-control section-search input-lg\" name=\"q\" placeholder=\"Search Contacts\" value=\"\" style=\"padding: 0px 26px 0px 26px;padding-left: 42px;height: 40px;width: 450px;font-size: 16px;background-color: #fcfdfe;border: 1px solid #C4CED7\"> <i class=\"fa fa-times form-control-icon search-close input-lg visible\" ng-click=\"query = null\" style=\"margin-left: -30px;font-size: 14px;color: #878C90;cursor: pointer;height: 0px;padding: 0px\"></i> </div> </form> </div> <md-content style=\"border: 1px solid #ddd;background-color: white;border-radius: 5px;    margin-top: 0px\"> <h4 style=\"font-size: 16px;padding-left: 12px\">Recently Viewed</h4> <hr style=\"margin-bottom: 0px;margin-top: 0px\"> <md-progress-circular md-mode=\"indeterminate\" style=\"margin : 10px\" ng-if=\"searching\" md-diameter=\"25\"></md-progress-circular> <h4 ng-if=\"users.length == 0 && !searching\" style=\"margin: 10px\">No record found</h4> <md-list flex=\"\" style=\"padding-top: 0px\"> <md-list-item class=\"md-3-line\" ng-repeat=\"user in users\" ng-click=\"profile(user.id)\" style=\"border-bottom: 1px solid #ddd\"> <div class=\"md-list-item-text\" layout=\"column\"> <h4 style=\"font-size: 16px;font-weight: 700\">{{ user.firstName +\" \" + user.lastName }}</h4> <h4>{{ user.emailAddress }}</h4> <p>{{ user.accountName }}</p> </div>  </md-list-item> </md-list> </md-content> </div> <!--  <div class=\"col-lg-12 user-table\">\n" +
    "    <table class=\"table table-striped\">\n" +
    "      <thead>\n" +
    "        <tr style= \"cursor: default;\">\n" +
    "          <th>Email Address</th>\n" +
    "          <th>First Name</th>\n" +
    "          <th>Last Name</th>\n" +
    "          <th>Status</th>\n" +
    "        </tr>\n" +
    "      </thead>\n" +
    "      <tbody>\n" +
    "        <tr ng-click=\"profile(user.id)\" ng-repeat=\"user in users\" style = \"cursor: pointer;\">\n" +
    "          <td>{{user.emailAddress}}</td>\n" +
    "          <td>{{user.firstName}}</td>\n" +
    "          <td>{{user.lastName}}</td>\n" +
    "          <td>{{user.currentStatus}}</td>\n" +
    "        </tr>\n" +
    "      </tbody>\n" +
    "    </table>\n" +
    "\n" +
    "    <div class=\"col-lg-6 col-md-6\" style=\"padding: 0px;padding-top: 7px;\" ng-if=\"users.length\">\n" +
    "      Showing from {{((page - 1 )* 10) + 1}} to {{page * 10 <= total? page * 10: total}} of {{total}} results\n" +
    "    </div>\n" +
    "     <div class=\"col-lg-6 col-md-6\" ng-if=\"users.length\" style =\"text-align: right\">\n" +
    "       <ul uib-pagination total-items=\"total\" ng-model=\"page\" max-size=\"maxSize\" class=\"pagination-sm\" ng-change=\"searchuser(query, page)\" boundary-links=\"true\" rotate=\"false\"></ul>\n" +
    "    </div>\n" +
    "  </div> --> </div></md-content> </section>"
  );


  $templateCache.put('views/tabs.html',
    "<section layout=\"column\" flex=\"\" style=\"min-width:auto\"> <div class=\"row profileDiv\"> <div class=\"modal fade\" id=\"deleteWarning\" tabindex=\"-1\" role=\"dialog\" style=\"margin:auto;display:none\"> <div class=\"modal-dialog\" id=\"showPopup\" align=\"center\" style=\"width: 450px; height: 155px;top:20%;border-radius:0px!important\"> <div class=\"modal-content\" style=\"text-align: center; width:450px; min-height:155px;border-radius:0px!important\"> <div class=\"modal-header\" style=\"padding-top: 12px;padding-bottom: 10px;border-radius:0px!important\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\" style=\"font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-size:18px\" align=\"left\"> <span class=\"glyphicon glyphicon-info-sign\"></span>&nbsp;&nbsp;Information</h4> </div> <div class=\"modal-body putTextHere\" style=\"padding-left: 25px;margin-top: 0px;margin-bottom: 0px;padding-bottom: 8px\"> <p class=\"universalFont\" id=\"putDetailsHere\">Are you sure you want to delete?</p> </div> <div class=\"modal-footer\" style=\"padding-top:12px;text-align: center;border-radius:0px!important\"> <button type=\"button\" ng-if=\"deleteWidgets\" class=\"btn btn-md btn-default universalStyleForButtons\" id=\"Yes\" ng-click=\"deleteWidget()\" data-dismiss=\"modal\" style=\"height: 25px;padding: 2px;width: 58px;border-radius: 0px\">Yes</button> <button type=\"button\" ng-if=\"deleteTabs\" class=\"btn btn-md btn-default universalStyleForButtons\" id=\"Yes\" ng-click=\"deleteTab()\" data-dismiss=\"modal\" style=\"height: 25px;padding: 2px;width: 58px;border-radius: 0px\">Yes</button> <button type=\"button\" class=\"btn btn-md btn-danger universalStyleForButtons\" id=\"No\" style=\"height: 25px;padding: 2px;width: 58px;border-radius: 0px\" data-dismiss=\"modal\">No</button> </div> </div> </div> </div> </div> <md-content layout-margin=\"\" dnd-external-sources=\"true\" dnd-list=\"\" dnd-drop=\"droppedontabset(index, item, external, type, $index)\"> <md-tabs md-dynamic-height=\"\" md-border-bottom=\"\"> <md-tab ng-click=\"tabchanged($index);\" ng-repeat=\"tab in alltabs\" label=\"{{tab.name}}\"> <md-tab-label> <h4 ng-hide=\"tab.edit\" style=\"text-transform: capitalize;font-size: 16px\"> {{tab.name}} <a href=\"\" ng-hide=\"\" ng-click=\"tab.edit = true\" ng-if=\"editName\" style=\"text-decoration: none\"> <md-icon md-svg-icon=\"images/edit.svg\" class=\"editIcon\"></md-icon> </a> <a ng-if=\"deletePanel\" data-toggle=\"modal\" data-target=\"#deleteWarning\" onclick=\"showConfirm()\" ng-click=\"getIndexForDeleteTab(tab ,$index)\" style=\"margin-top: -17px;height: 26px;min-height: 26px;width: 40px;min-width: 40px; margin-bottom: -2px;margin-left: 0px\"> <md-icon style=\"fill : red\" md-svg-icon=\"images/clear.svg\"></md-icon> </a> </h4> <div ng-show=\"tab.edit\" style=\"margin-top: -10px\"> <input type=\"text\" ng-model=\"tab.name\" style=\"color: black;height: 26px;width: 134px;font-size: 12px\" ng-keydown=\"onKeydown($event)\"> <md-button class=\"md-raised md-primary\" ng-click=\"tab.edit = false;savetab(tab);\" style=\"min-width: 30px;min-height: 26px;height: 26px\"> <md-icon md-svg-icon=\"images/save.svg\" style=\"margin-top: -12px\"></md-icon> </md-button> </div> </md-tab-label> <md-tab-body> <div style=\"width:100%; min-height: 420px;height: auto;padding: 20px;color:#444444;padding-top: 0px;font-family: Helvetica Neue , Helvetica, Arial, sans-serif\" dnd-external-sources=\"true\" dnd-list=\"\" dnd-drop=\"dropped(index, item, external, type, $index)\" class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" ng-model=\"tab.widgets\" id=\"placeholder\" ui-sortable=\"sortableOptions\"> <div id=\"{{$index}}\" class=\"resizable\" resizable r-directions=\"['right', 'bottom']\" r-flex=\"false\" r-width=\"column.width\" r-height=\"column.height\" ng-repeat=\"column in tab.widgets\" style=\"float: left; margin-left: 10px; margin-top: 10px\" resize> <div style=\"height: 100%; overflow-x: scroll;width: inherit\" class=\"panel panel-default\"> <md-toolbar class=\"\" style=\"min-height: 40px;width:inherit;position: absolute\"> <div class=\"md-toolbar-tools\" style=\"height: 48px;background-color:white;color:#444444;border-bottom: 1px solid #eeeeee;border-right: 1px solid #ccc\"> <h4 ng-hide=\"column.edit\" style=\"font-size: 16px\">{{column.name | capitalize }}</h4> <span ng-show=\"column.edit\"> <form ng-submit=\"saveWidget(column); column.edit = false;\"> <input type=\"text\" ng-model=\"column.name\" style=\"color: black;height: 26px;width: 134px;font-size: 14px\"> </form> </span> <a href=\"\" ng-hide=\"column.edit\" ng-click=\"column.edit = true\" ng-if=\"editName\"> <md-icon md-svg-icon=\"images/edit.svg\" class=\"editIcon\" style=\"fill: black;margin-top: -5px;margin-left: 5px\"></md-icon> </a> <span flex=\"\"></span> <md-menu-bar ng-if=\"column.type == 'eloqua' && column.json.internalName == 'Activity Breakdown'  || column.type == 'eloqua' && column.json.internalName == 'Latest Activities' || column.type == 'eloqua' && column.json.internalName == 'Activities Graph' \"> <span style=\"font-size:15px;opacity:.6\"> {{fromdate*1000 | date}} to {{todate*1000 | date}} </span> <md-menu ng-show=\"column.json.internalName != &quot;Activities Graph&quot;\"> <button ng-click=\"$mdMenu.open()\"> <i class=\"fa fa-filter\" aria-hidden=\"true\" style=\"color:#428bca\"></i> </button> <md-menu-content width=\"3\"> <md-menu-item ng-repeat=\"filter in filters\"> <md-menu class=\"nested-menu\"> <md-button ng-click=\"\">{{ filter.filterBy }}</md-button> <md-menu-content width=\"3\"> <md-menu-item ng-repeat=\"type in filter.type\" md-position-mode=\"target-left target\"> <!-- <md-button ng-click=\"filterByDate(type.name , column)\" ng-if=\"filter.filterBy == 'By Date'\"> --> <!-- {{ type.name }} --> <!-- </md-button> --> <md-checkbox md-menu-item ng-if=\"filter.filterBy == 'By Type'\" style=\"margin-left: 28px\" ng-checked=\"true\" ng-mouseup=\"checkedType(column , type.type)\"> <span> {{ type.name }} </span> </md-checkbox> </md-menu-item></md-menu-content> </md-menu> </md-menu-item> <!-- <md-menu-item> --> <!-- <md-button ng-click=\"showNameSearch(column)\"> --> <!-- By Name --> <!-- </md-button> --> <!-- </md-menu-item> --> </md-menu-content> </md-menu> </md-menu-bar> <md-button class=\"md-icon-button\" aria-label=\"delete\" ng-if=\"deletePanel\" data-toggle=\"modal\" data-target=\"#deleteWarning\" onclick=\"showConfirm()\" ng-click=\"getIndexForDelete($index)\"> <md-icon style=\"fill : red\" md-svg-icon=\"images/clear.svg\"></md-icon> </md-button> </div> <md-progress-linear md-mode=\"indeterminate\" ng-if=\"dataLoaded\" style=\"margin-bottom: -3px !important\"></md-progress-linear> </md-toolbar> <md-content layout-padding=\"\" style=\"background-color: white;min-width: min-content\"> objectname: 'accountBasedLatestActivity' <div class=\"panel-body\" ng-show=\"column.type == 'eloqua' && column.json.internalName == 'Latest Activities' && column.objectname == 'accountBasedLatestActivity'\" ng-model=\"column.json.properties\" style=\"padding-top: 45px;min-width: 520px\"> <div ng-show=\"customDateForLatestActivitis\" style=\"height: 32px\"> Start Date : <md-datepicker ng-model=\"myStartDate1\" md-placeholder=\"Enter date\" md-open-on-focus=\"\"></md-datepicker> <span flex=\"\"></span> <span class=\"pull-right\" style=\"margin-top: -1px\"> End Date : <md-datepicker ng-model=\"myEndDate1\" md-placeholder=\"Enter date\" md-open-on-focus=\"\"></md-datepicker> <md-button style=\"height: 23px;margin-top: 0px;min-width:  46px;margin-bottom:  0px;background-color: #f2f4f5;min-height: 25px\" ng-click=\"filterByCustomDate(column , myStartDate1 , myEndDate1)\"> <md-icon class=\"\" md-svg-icon=\"images/filter_icon.svg\" style=\"min-width: 20px;width:  20px;margin-top: -10px\"></md-icon> </md-button> </span> </div> <div style=\"height: 32px\" ng-show=\"nameSearchForLatestActivities\"> <span style=\"display:flex\"> <input type=\"checkbox\" ng-model=\"checked\" style=\"width: 12px;height: 12px;margin-right: 2px\"> not <span style=\"margin-left: 20px\">Contains </span> <input type=\"text\" class=\"form-control\" name=\"assetName\" ng-model=\"assetName\" placeholder=\"Name..\" style=\"width: 50%;height: 23px;display: inline;margin-left: 6px\"> <md-button style=\"height: 23px;min-width:  46px;margin-top: 0px; margin-bottom:  0px;background-color: #f2f4f5;min-height: 23px\" ng-click=\"filterByName(column , checked , assetName)\"> <md-icon class=\"\" md-svg-icon=\"images/filter_icon.svg\" style=\"min-width: 20px;width:  20px;margin-top: -10px\"></md-icon> </md-button> </span> </div> <div class=\"col-lg-12\" style=\"min-height: 300px; border: 1px solid #dddddd;height: auto;padding:0px\"> <md-list flex=\"\" style=\"padding: 0px\" ng-repeat=\"activity in latestActivites |orderBy :activityDate\"> <md-list-item class=\"md-2-line\" ng-init=\"\" style=\"border-bottom: 1px solid #eeeeee;cursor: pointer\" ng-show=\"activity.activityType == 'emailOpen' && latestEmailOpen\"> <img src=\"{{activity.image}}\" class=\"\" style=\"margin-right: 14px;margin-top:20px\"> <div class=\"md-list-item-text\" layout=\"column\"> <strong style=\"\">{{ activity.subjectLine }}</strong> <p><md-icon md-svg-icon=\"images/watch.svg\" style=\"width:14px;min-width: 14px\"></md-icon> {{activity.activityDate * 1000 | date : \"medium\"}}</p> </div> <md-icon class=\"pull-right\" md-svg-icon=\"images/activities_right.svg\" style=\"\"></md-icon> </md-list-item> <md-list-item class=\"md-2-line\" ng-init=\"\" style=\"border-bottom: 1px solid #eeeeee;cursor: pointer\" ng-show=\"activity.activityType == 'emailSend' && latestEmailSend\"> <img src=\"{{activity.image}}\" class=\"\" style=\"margin-right: 14px;margin-top:20px\"> <div class=\"md-list-item-text\" layout=\"column\"> <strong style=\"\">{{ activity.subjectLine }}</strong> <p><md-icon md-svg-icon=\"images/watch.svg\" style=\"width:14px;min-width: 14px\"></md-icon> {{activity.activityDate * 1000 | date : \"medium\"}}</p> </div> <md-icon class=\"pull-right\" md-svg-icon=\"images/activities_right.svg\" style=\"\"></md-icon> </md-list-item> <md-list-item class=\"md-2-line\" ng-init=\"\" style=\"border-bottom: 1px solid #eeeeee;cursor: pointer\" ng-show=\"activity.activityType == 'emailClickThrough' && latestEmailClick\"> <img src=\"{{activity.image}}\" class=\"\" style=\"margin-right: 14px;margin-top:20px\"> <div class=\"md-list-item-text\" layout=\"column\"> <strong style=\"\">{{ activity.subjectLine }}</strong> <p><md-icon md-svg-icon=\"images/watch.svg\" style=\"width:14px;min-width: 14px\"></md-icon> {{activity.activityDate * 1000 | date : \"medium\"}}</p> </div> <md-icon class=\"pull-right\" md-svg-icon=\"images/activities_right.svg\" style=\"\"></md-icon> </md-list-item> <md-list-item class=\"md-2-line\" ng-init=\"\" style=\"border-bottom: 1px solid #eeeeee;cursor: pointer\" ng-show=\"activity.activityType == 'webVisit' && latestWebVisit\"> <img src=\"{{activity.image}}\" class=\"\" style=\"margin-right: 14px;margin-top:20px\"> <div class=\"md-list-item-text\" layout=\"column\"> <strong style=\"\">{{ activity.subjectLine }}</strong> <p><md-icon md-svg-icon=\"images/watch.svg\" style=\"width:14px;min-width: 14px\"></md-icon> {{activity.activityDate * 1000 | date : \"medium\"}}</p> </div> <md-icon class=\"pull-right\" md-svg-icon=\"images/activities_right.svg\" style=\"\"></md-icon> </md-list-item> <md-list-item class=\"md-2-line\" ng-init=\"\" style=\"border-bottom: 1px solid #eeeeee;cursor: pointer\" ng-show=\"activity.activityType == 'formSubmit' && latestFormSubmit\"> <img src=\"{{activity.image}}\" class=\"\" style=\"margin-right: 14px;margin-top:20px\"> <div class=\"md-list-item-text\" layout=\"column\"> <strong style=\"height: 20px\">{{ activity.subjectLine }}</strong> <p><md-icon md-svg-icon=\"images/watch.svg\" style=\"width:14px;min-width: 14px\"></md-icon> {{activity.activityDate * 1000 | date : \"medium\"}}</p> </div> <md-icon class=\"pull-right\" md-svg-icon=\"images/activities_right.svg\" style=\"\"></md-icon> </md-list-item> <md-list-item class=\"md-2-line\" ng-init=\"\" style=\"border-bottom: 1px solid #eeeeee;cursor: pointer\" ng-show=\"activity.activityType == 'latestCampainMember' && campaignMembership\"> <img src=\"{{activity.image}}\" class=\"\" style=\"margin-right: 14px;margin-top:20px\"> <div class=\"md-list-item-text\" layout=\"column\"> <strong style=\"\">{{ activity.subjectLine }}</strong> <p><md-icon md-svg-icon=\"images/watch.svg\" style=\"width:14px;min-width: 14px\"></md-icon> {{activity.activityDate * 1000 | date : \"medium\"}}</p> </div> <md-icon class=\"pull-right\" md-svg-icon=\"images/activities_right.svg\" style=\"\"></md-icon> </md-list-item> </md-list> </div> </div> <div class=\"panel-body\" ng-show=\"column.type == 'eloqua' && column.json.internalName == 'Latest Activities' && column.objectname != 'accountBasedLatestActivity'\" ng-model=\"column.json.properties\" style=\"padding-top: 45px;min-width: 520px\"> <div ng-show=\"customDateForLatestActivitis\" style=\"height: 32px\"> Start Date : <md-datepicker ng-model=\"myStartDate1\" md-placeholder=\"Enter date\" md-open-on-focus=\"\"></md-datepicker> <span flex=\"\"></span> <span class=\"pull-right\" style=\"margin-top: -1px\"> End Date : <md-datepicker ng-model=\"myEndDate1\" md-placeholder=\"Enter date\" md-open-on-focus=\"\"></md-datepicker> <md-button style=\"height: 23px;margin-top: 0px;min-width:  46px;margin-bottom:  0px;background-color: #f2f4f5;min-height: 25px\" ng-click=\"filterByCustomDate(column , myStartDate1 , myEndDate1)\"> <md-icon class=\"\" md-svg-icon=\"images/filter_icon.svg\" style=\"min-width: 20px;width:  20px;margin-top: -10px\"></md-icon> </md-button> </span> </div> <div style=\"height: 32px\" ng-show=\"nameSearchForLatestActivities\"> <span flex=\"50\"> Name Contains : <input type=\"text\" class=\"form-control\" name=\"contains\" ng-model=\"nameContains\" style=\"width: 24%;height: 27px;display: inline\"> </span> <sapn flex=\"50\"> Name not Contains : <input type=\"text\" class=\"form-control\" name=\"notContains\" ng-model=\"nameNotContains\" style=\"width: 24%;height: 27px;display: inline\"> <md-button style=\"height: 23px;margin-top: -6px;min-width:  46px;margin-bottom:  0px;background-color: #f2f4f5;min-height: 25px\" ng-click=\"filterByName(column , nameContains , nameNotContains)\"> <md-icon class=\"\" md-svg-icon=\"images/filter_icon.svg\" style=\"min-width: 20px;width:  20px;margin-top: -10px\"></md-icon> </md-button>  </sapn></div> <div class=\"col-lg-12\" style=\"min-height: 300px; border: 1px solid #dddddd;height: auto;padding:0px\"> <md-list flex=\"\" style=\"padding: 0px\" ng-repeat=\"activity in latestActivitesContact\"> <md-list-item class=\"md-2-line\" ng-init=\"\" style=\"border-bottom: 1px solid #eeeeee;cursor: pointer\" ng-show=\"activity.activityType == 'emailOpen' && latestEmailOpen\"> <img src=\"{{activity.image}}\" class=\"\" style=\"margin-right: 14px;margin-top:20px\"> <div class=\"md-list-item-text\" layout=\"column\"> <strong style=\"height: 20px\">{{ activity.subjectLine }}</strong> <p><md-icon md-svg-icon=\"images/watch.svg\" style=\"width:14px;min-width: 14px\"></md-icon> {{activity.activityDate * 1000 | date : \"medium\"}}</p> </div> <md-icon class=\"pull-right\" md-svg-icon=\"images/activities_right.svg\" style=\"\"></md-icon> </md-list-item> <md-list-item class=\"md-2-line\" ng-init=\"\" style=\"border-bottom: 1px solid #eeeeee;cursor: pointer\" ng-show=\"activity.activityType == 'emailSend' && latestEmailSend\"> <img src=\"{{activity.image}}\" class=\"\" style=\"margin-right: 14px;margin-top:20px\"> <div class=\"md-list-item-text\" layout=\"column\"> <strong style=\"height: 20px\">{{ activity.subjectLine }}</strong> <p><md-icon md-svg-icon=\"images/watch.svg\" style=\"width:14px;min-width: 14px\"></md-icon> {{activity.activityDate * 1000 | date : \"medium\"}}</p> </div> <md-icon class=\"pull-right\" md-svg-icon=\"images/activities_right.svg\" style=\"\"></md-icon> </md-list-item> <md-list-item class=\"md-2-line\" ng-init=\"\" style=\"border-bottom: 1px solid #eeeeee;cursor: pointer\" ng-show=\"activity.activityType == 'emailClickThrough' && latestEmailClick\"> <img src=\"{{activity.image}}\" class=\"\" style=\"margin-right: 14px;margin-top:20px\"> <div class=\"md-list-item-text\" layout=\"column\"> <strong style=\"height: 20px\">{{ activity.subjectLine }}</strong> <p><md-icon md-svg-icon=\"images/watch.svg\" style=\"width:14px;min-width: 14px\"></md-icon> {{activity.activityDate * 1000 | date : \"medium\"}}</p> </div> <md-icon class=\"pull-right\" md-svg-icon=\"images/activities_right.svg\" style=\"\"></md-icon> </md-list-item> <md-list-item class=\"md-2-line\" ng-init=\"\" style=\"border-bottom: 1px solid #eeeeee;cursor: pointer\" ng-show=\"activity.activityType == 'webVisit' && latestWebVisit\"> <img src=\"{{activity.image}}\" class=\"\" style=\"margin-right: 14px;margin-top:20px\"> <div class=\"md-list-item-text\" layout=\"column\"> <strong style=\"height: 20px\">{{ activity.subjectLine }}</strong> <p><md-icon md-svg-icon=\"images/watch.svg\" style=\"width:14px;min-width: 14px\"></md-icon> {{activity.activityDate * 1000 | date : \"medium\"}}</p> </div> <md-icon class=\"pull-right\" md-svg-icon=\"images/activities_right.svg\" style=\"\"></md-icon> </md-list-item> <md-list-item class=\"md-2-line\" ng-init=\"\" style=\"border-bottom: 1px solid #eeeeee;cursor: pointer\" ng-show=\"activity.activityType == 'formSubmit' && latestFormSubmit\"> <img src=\"{{activity.image}}\" class=\"\" style=\"margin-right: 14px;margin-top:20px\"> <div class=\"md-list-item-text\" layout=\"column\"> <strong style=\"height: 20px\">{{ activity.subjectLine }}</strong> <p><md-icon md-svg-icon=\"images/watch.svg\" style=\"width:14px;min-width: 14px\"></md-icon> {{activity.activityDate * 1000 | date : \"medium\"}}</p> </div> <md-icon class=\"pull-right\" md-svg-icon=\"images/activities_right.svg\" style=\"\"></md-icon> </md-list-item> <md-list-item class=\"md-2-line\" ng-init=\"\" style=\"border-bottom: 1px solid #eeeeee;cursor: pointer\" ng-show=\"activity.activityType == 'latestCampainMember' && campaignMembership\"> <img src=\"{{activity.image}}\" class=\"\" style=\"margin-right: 14px;margin-top:20px\"> <div class=\"md-list-item-text\" layout=\"column\"> <strong style=\"height: 20px\">{{ activity.subjectLine }}</strong> <p><md-icon md-svg-icon=\"images/watch.svg\" style=\"width:14px;min-width: 14px\"></md-icon> {{activity.activityDate * 1000 | date : \"medium\"}}</p> </div> <md-icon class=\"pull-right\" md-svg-icon=\"images/activities_right.svg\" style=\"\"></md-icon> </md-list-item> </md-list> </div> </div> <div class=\"panel-body\" ng-show=\"(column.type == 'eloqua' || column.type == 'eloquaAccountBsaed') && column.objectname == 'accountBasedActivityBreakdown'\" ng-model=\"column.json.properties\" style=\"padding-top: 45px;min-width: 520px;height:420px\"> <div ng-show=\"customDate\" style=\"height: 32px\"> Start Date : <md-datepicker ng-model=\"myStartDate\" md-placeholder=\"Enter date\" md-open-on-focus=\"\"></md-datepicker> <span flex=\"\"></span> <span class=\"pull-right\" style=\"margin-top: -1px\"> End Date : <md-datepicker ng-model=\"myEndDate\" md-placeholder=\"Enter date\" md-open-on-focus=\"\"></md-datepicker> <md-button style=\"height: 23px;margin-top: 0px;min-width:  46px;margin-bottom:  0px;background-color: #f2f4f5;min-height: 25px\" ng-click=\"filterByCustomDate(column , myStartDate , myEndDate)\"> <md-icon class=\"\" md-svg-icon=\"images/filter_icon.svg\" style=\"min-width: 20px;width:  20px;margin-top: -10px\"></md-icon> </md-button> </span> </div> <div style=\"height: 32px\" ng-show=\"nameSearch\"> <span style=\"display:flex\"> <input type=\"checkbox\" ng-model=\"checked1\" style=\"width: 12px;height: 12px;margin-right: 2px\"> not <span style=\"margin-left: 20px\">Contains </span> <input type=\"text\" class=\"form-control\" name=\"assetNames\" ng-model=\"assetNames\" style=\"width: 50%;height: 23px;display: inline;margin-left: 6px\" placeholder=\"Name..\"> <md-button style=\"height: 23px;min-width:  46px;margin-bottom:  0px;background-color: #f2f4f5;min-height: 23px;margin-top: 0px\" ng-click=\"filterByName(column , checked1 , assetNames)\"> <md-icon class=\"\" md-svg-icon=\"images/filter_icon.svg\" style=\"min-width: 20px;width:  20px;margin-top: -10px\"></md-icon> </md-button> </span> </div> <div dnd-external-sources=\"false\" dnd-list=\"\" class=\"col-lg-12\" style=\"min-height: 300px; border: 1px solid #dddddd;height: auto;padding-bottom:20px;padding-top: 20px\"> <div style=\"cursor: pointer\" ng-init=\"emailSend = false\" ng-click=\"emailSend = !emailSend\" ng-show=\"emailSends\"> <img class=\"\" src=\"/img/emailSends.PNG\"> <strong>Email Sends</strong> <strong class=\"pull-right\" style=\"margin-right: 20px;font-size: 16px\">{{widgetdata[column.id][\"emailSend\"].length}}</strong> <hr> </div> <div ng-show=\"emailSend && emailSended.length && emailSends\" style=\"border: 1px solid #ddd;max-height : 400px;border-bottom: 0px solid\" class=\"md-whiteframe-3dp\"> <!-- <div  ng-repeat=\"asset in emailSended\"> --> <!-- <div style=\"padding-bottom: 20px;padding-left: 20px\"> --> <!-- <b>Email : </b> --> <!-- {{asset.emailAddress}} --> <!-- </br> --> <!-- <b>Name : </b>{{asset.name}} --> <!-- </br> --> <!-- <b>Activity Date : </b>{{asset.activityDate * 1000 | date : \"medium\"}} --> <!-- </div> --> <!-- </div> --> <md-toolbar class=\"md-table-toolbar\" ng-hide=\"selected.length || filter.show\" aria-hidden=\"false\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);color: black;z-index: 0\"> <div class=\"md-toolbar-tools\"> <h2 class=\"md-title\" style=\"font-size: 15px\">Email Send</h2> <div flex=\"\" class=\"flex\"></div> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = true\" aria-label=\"filter_list\"> <md-icon class=\"material-icons\" md-svg-src=\"images/filter_list.svg \" style=\"fill:black\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar md-default ng-hide\" ng-show=\"filter.show &amp;&amp; !selected.length\" aria-hidden=\"true\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);z-index: 0\"> <div class=\"md-toolbar-tools\"> <!-- <md-icon class=\"material-icons\" style=\"position:  absolute;padding-top: 4px;font-size: 20px;padding-left: 4px;\">search</md-icon> --> <form flex=\"\" name=\"filter.form\" class=\"ng-pristine ng-valid flex\"> <input type=\"text\" ng-model=\"emailSend2\" ng-change=\"\" placeholder=\"search\" class=\"form-control\" aria-invalid=\"false\" style=\"height: 26px; font-size: initial;width: 200px\"> </form> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = false\" aria-label=\"close\"> <md-icon class=\"material-icons\" md-svg-src=\"images/clear.svg \" style=\"fill:red\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-table-container style=\"max-height : 300px;border-bottom:0px solid\"> <table md-table ng-model=\"selected\" md-progress=\"promise\"> <thead md-head md-order=\"query.order\" md-on-reorder=\"\"> <tr md-row> <th md-column><b>Name</b></th> <th md-column><b>Email</b></th> <th md-column><b>Activity Date</b></th> </tr> </thead> <tbody md-body> <tr md-row md-select=\"asset\" ng-repeat=\"asset in emailSended | filter : emailSend2 \"> <td md-cell>{{asset.name}}</td> <td md-cell>{{asset.emailAddress}}</td> <td md-cell>{{asset.activityDate * 1000 | date : \"medium\"}}</td> </tr> </tbody> </table> </md-table-container> <hr style=\"margin-top: 0px\"> </div> <div ui-sortable=\"sortableOptions\" style=\"cursor: pointer\" ng-init=\"emailopen = false\" ng-click=\"emailopen = !emailopen\" ng-show=\"emailOpens\"> <img class=\"\" src=\"/img/emailOpen.PNG\"> <strong>Email Open</strong> <strong class=\"pull-right\" style=\"margin-right: 20px;font-size: 16px\">{{emailOpend.length}}</strong> <hr> </div> <div ng-show=\"emailOpens && emailopen && emailOpend.length\" style=\"border: 1px solid #ddd;max-height : 400px;border-bottom: 0px solid\" class=\"md-whiteframe-3dp\"> <!-- <div ng-repeat=\"asset in emailOpend\">  <div style=\"padding-bottom: 20px;padding-left: 20px\"> --> <!-- <b>Email : </b> --> <!-- {{asset.emailAddress}} --> <!-- </br> --> <!-- <b>Name : </b> {{asset.name}} --> <!-- </br> --> <!-- <b> Activity Date : </b>{{asset.activityDate * 1000 | date : \"medium\"}} --> <!-- </div> --> <!-- </div> --> <md-toolbar class=\"md-table-toolbar\" ng-hide=\"selected.length || filter.show\" aria-hidden=\"false\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);color: black;z-index: 0\"> <div class=\"md-toolbar-tools\"> <h2 class=\"md-title\" style=\"font-size: 15px\">Email Open</h2> <div flex=\"\" class=\"flex\"></div> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = true\" aria-label=\"filter_list\"> <md-icon class=\"material-icons\" md-svg-src=\"images/filter_list.svg \" style=\"fill:black\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar md-default ng-hide\" ng-show=\"filter.show &amp;&amp; !selected.length\" aria-hidden=\"true\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);z-index: 0\"> <div class=\"md-toolbar-tools\"> <!-- <md-icon class=\"material-icons\" style=\"position:  absolute;padding-top: 4px;font-size: 20px;padding-left: 4px;\">search</md-icon> --> <form flex=\"\" name=\"filter.form\" class=\"ng-pristine ng-valid flex\"> <input type=\"text\" ng-model=\"emailOpen2\" ng-change=\"\" placeholder=\"search\" class=\"form-control\" aria-invalid=\"false\" style=\"height: 26px; font-size: initial;width: 200px\"> </form> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = false\" aria-label=\"close\"> <md-icon class=\"material-icons\" md-svg-src=\"images/clear.svg \" style=\"fill:red\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-table-container style=\"max-height : 300px;border-bottom:0px solid\"> <table md-table ng-model=\"selected\" md-progress=\"promise\"> <thead md-head md-order=\"query.order\" md-on-reorder=\"\"> <tr md-row> <th md-column><b>Name</b></th> <th md-column><b>Email</b></th> <th md-column><b>Activity Date</b></th> </tr> </thead> <tbody md-body> <tr md-row md-select=\"asset\" ng-repeat=\"asset in emailOpend | filter : emailOpen2 \"> <td md-cell>{{asset.name}}</td> <td md-cell>{{asset.emailAddress}}</td> <td md-cell>{{asset.activityDate * 1000 | date : \"medium\"}}</td> </tr> </tbody> </table> </md-table-container> <hr style=\"margin-top: 0px\"> </div> <div ui-sortable=\"sortableOptions\" style=\"cursor: pointer\" ng-init=\"emailClickThrough = false\" ng-click=\"emailClickThrough = !emailClickThrough\" ng-show=\"emailClick\"> <img class=\"\" src=\"/img/emailClicked.PNG\"> <strong>Email Click</strong> <strong class=\"pull-right\" style=\"margin-right: 20px;font-size: 16px\">{{emailClicked.length}}</strong> <hr> </div> <div ng-show=\"emailClick && emailClickThrough && emailClicked.length\" style=\"border: 1px solid #ddd;max-height : 400px;border-bottom: 0px solid\" class=\"md-whiteframe-3dp\"> <!-- <div ng-repeat=\"asset in emailClicked\">  <div style=\"padding-bottom: 20px;padding-left: 20px\"> --> <!-- <b>Email : </b> --> <!-- {{asset.emailAddress}} --> <!-- </br> --> <!-- <b>Name : </b> {{asset.name}} --> <!-- </br> --> <!-- <b> Activity Date : </b>{{asset.activityDate * 1000 | date : \"medium\"}} --> <!-- </br> --> <!-- <b> Link : </b><a href= \"{{asset.link}}\" target=\"_blank\" style=\"cursor: pointer\">{{asset.link}}</a> --> <!-- </div> --> <!-- </div> --> <md-toolbar class=\"md-table-toolbar\" ng-hide=\"selected.length || filter.show\" aria-hidden=\"false\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);color: black;z-index: 0\"> <div class=\"md-toolbar-tools\"> <h2 class=\"md-title\" style=\"font-size: 15px\">Email Click</h2> <div flex=\"\" class=\"flex\"></div> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = true\" aria-label=\"filter_list\"> <md-icon class=\"material-icons\" md-svg-src=\"images/filter_list.svg \" style=\"fill:black\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar md-default ng-hide\" ng-show=\"filter.show &amp;&amp; !selected.length\" aria-hidden=\"true\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);z-index: 0\"> <div class=\"md-toolbar-tools\"> <!-- <md-icon class=\"material-icons\" style=\"position:  absolute;padding-top: 4px;font-size: 20px;padding-left: 4px;\">search</md-icon> --> <form flex=\"\" name=\"filter.form\" class=\"ng-pristine ng-valid flex\"> <input type=\"text\" ng-model=\"emailClick2\" ng-change=\"\" placeholder=\"search\" class=\"form-control\" aria-invalid=\"false\" style=\"height: 26px; font-size: initial;width: 200px\"> </form> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = false\" aria-label=\"close\"> <md-icon class=\"material-icons\" md-svg-src=\"images/clear.svg \" style=\"fill:red\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-table-container style=\"max-height : 300px;border-bottom:0px solid\"> <table md-table ng-model=\"selected\" md-progress=\"promise\"> <thead md-head md-order=\"query.order\" md-on-reorder=\"\"> <tr md-row> <th md-column><b>Name</b></th> <th md-column><b>Email</b></th> <th md-column><b>Activity Date</b></th> <th md-column><b>Link</b></th> </tr> </thead> <tbody md-body> <tr md-row md-select=\"asset\" ng-repeat=\"asset in emailClicked | filter : emailClick2 \"> <td md-cell>{{asset.name}}</td> <td md-cell>{{asset.emailAddress}}</td> <td md-cell>{{asset.activityDate * 1000 | date : \"medium\"}}</td> <td md-cell><a href=\"{{asset.link}}\" target=\"_blank\" style=\"cursor: pointer\">{{asset.link}}</a></td> </tr> </tbody> </table> </md-table-container> <hr style=\"margin-top: 0px\"> </div> <div ui-sortable=\"sortableOptions\" style=\"cursor: pointer\" ng-init=\"webVisit = false\" ng-click=\"webVisit = !webVisit\" ng-show=\"webVisites\"> <img class=\"\" src=\"/img/webVisited.PNG\"> <strong>Web Page Visited</strong> <strong class=\"pull-right\" style=\"margin-right: 20px;font-size: 16px\">{{webVisited.length}}</strong> <hr> </div> <div ng-show=\"webVisites && webVisit && webVisited.length\" style=\"border: 1px solid #ddd;max-height : 400px;border-bottom: 0px solid\" class=\"md-whiteframe-3dp\"> <!-- <div ng-repeat=\"asset in webVisited\"> --> <!-- <div style=\"padding-bottom: 20px;padding-left: 20px\"> --> <!-- <b>Name : </b><a href= \"{{asset.name}}\" target=\"_blank\" style=\"cursor: pointer\"> {{asset.name}}</a> --> <!-- </br> --> <!-- <b> Activity Date : </b>{{asset.activityDate * 1000 | date : \"medium\"}} --> <!-- </div> --> <!-- </div> --> <md-toolbar class=\"md-table-toolbar\" ng-hide=\"selected.length || filter.show\" aria-hidden=\"false\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);color: black;z-index: 0\"> <div class=\"md-toolbar-tools\"> <h2 class=\"md-title\" style=\"font-size: 15px\">Web Visited</h2> <div flex=\"\" class=\"flex\"></div> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = true\" aria-label=\"filter_list\"> <md-icon class=\"material-icons\" md-svg-src=\"images/filter_list.svg \" style=\"fill:black\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar md-default ng-hide\" ng-show=\"filter.show &amp;&amp; !selected.length\" aria-hidden=\"true\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);z-index: 0\"> <div class=\"md-toolbar-tools\"> <!-- <md-icon class=\"material-icons\" style=\"position:  absolute;padding-top: 4px;font-size: 20px;padding-left: 4px;\">search</md-icon> --> <form flex=\"\" name=\"filter.form\" class=\"ng-pristine ng-valid flex\"> <input type=\"text\" ng-model=\"webVisit2\" ng-change=\"\" placeholder=\"search\" class=\"form-control\" aria-invalid=\"false\" style=\"height: 26px; font-size: initial;width: 200px\"> </form> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = false\" aria-label=\"close\"> <md-icon class=\"material-icons\" md-svg-src=\"images/clear.svg \" style=\"fill:red\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-table-container style=\"max-height : 300px;border-bottom:0px solid\"> <table md-table ng-model=\"selected\" md-progress=\"promise\"> <thead md-head md-order=\"query.order\" md-on-reorder=\"\"> <tr md-row> <th md-column><b>Link</b></th> <th md-column><b>Activity Date</b></th> </tr> </thead> <tbody md-body> <tr md-row md-select=\"asset\" ng-repeat=\"asset in webVisited | filter : webVisit2 \"> <td md-cell><a href=\"{{asset.name}}\" target=\"_blank\" style=\"cursor: pointer\"> {{asset.name}}</a></td> <td md-cell>{{asset.activityDate * 1000 | date : \"medium\"}}</td> </tr> </tbody> </table> </md-table-container> <hr style=\"margin-top: 0px\"> </div> <div ui-sortable=\"sortableOptions\" style=\"cursor: pointer\" ng-init=\"formSubmit = false\" ng-click=\"formSubmit = !formSubmit\" ng-show=\"formSubmited\"> <img class=\"\" src=\"/img/formSubmition.PNG\"> <strong>Form Submissions</strong> <strong class=\"pull-right\" style=\"margin-right: 20px;font-size: 16px\">{{formSubmits.length}}</strong> <hr> </div> <div ng-show=\"formSubmited && formSubmit && formSubmits.length\" style=\"border: 1px solid #ddd;max-height : 400px;border-bottom: 0px solid\" class=\"md-whiteframe-3dp\"> <!-- <div ng-repeat=\"asset in formSubmits\">  <div style=\"padding-bottom: 20px;padding-left: 20px\"> --> <!-- <b>Name : </b> {{asset.name}} --> <!-- </br> --> <!-- <b> Activity Date : </b>{{asset.activityDate * 1000 | date : \"medium\"}} --> <!-- </br> --> <!-- <md-icon md-svg-src=\"images/add.svg \" ng-init=\"submitedData = true\" class=\"parentPlusIcon\" ng-show = \"submitedData\"   ng-click=\"submitedData =!submitedData\"></md-icon> --> <!-- <md-icon ng-show=\"!submitedData\"  ng-click=\"submitedData=!submitedData\" md-svg-src=\"images/remove.svg\" class=\"parentMinusIcon\"></md-icon> --> <!-- <b>Submited Data</b> --> <!-- <div ng-show=\"!submitedData\" ng-repeat=\"data in  asset.formSubmitionData\"> --> <!-- <span style=\"padding-left: 20px\">{{data}}</span> --> <!-- </div> --> <!-- </div> --> <!-- </div> --> <md-toolbar class=\"md-table-toolbar\" ng-hide=\"selected.length || filter.show\" aria-hidden=\"false\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);color: black;z-index: 0\"> <div class=\"md-toolbar-tools\"> <h2 class=\"md-title\" style=\"font-size: 15px\">Form Submit</h2> <div flex=\"\" class=\"flex\"></div> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = true\" aria-label=\"filter_list\"> <md-icon class=\"material-icons\" md-svg-src=\"images/filter_list.svg \" style=\"fill:black\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar md-default ng-hide\" ng-show=\"filter.show &amp;&amp; !selected.length\" aria-hidden=\"true\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);z-index: 0\"> <div class=\"md-toolbar-tools\"> <form flex=\"\" name=\"filter.form\" class=\"ng-pristine ng-valid flex\"> <input type=\"text\" ng-model=\"formSubmit2\" ng-change=\"\" placeholder=\"search\" class=\"form-control\" aria-invalid=\"false\" style=\"height: 26px; font-size: initial;width: 200px\"> </form> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = false\" aria-label=\"close\"> <md-icon class=\"material-icons\" md-svg-src=\"images/clear.svg \" style=\"fill:red\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-table-container style=\"max-height : 300px;border-bottom:0px solid\"> <table md-table ng-model=\"selected\" md-progress=\"promise\"> <thead md-head md-order=\"query.order\" md-on-reorder=\"\"> <tr md-row> <th md-column><b>Submited Data</b></th> <th md-column><b>Name</b></th> <th md-column><b>Activity Date</b></th> </tr> </thead> <tbody md-body> <tr md-row md-select=\"asset\" ng-repeat=\"asset in formSubmits | filter : formSubmit2\"> <td md-cell><md-icon md-svg-src=\"images/add.svg \" ng-init=\"submitedData = true\" class=\"parentPlusIcon\" ng-show=\"submitedData\" ng-click=\"submitedData =!submitedData\"></md-icon> <md-icon ng-show=\"!submitedData\" ng-click=\"submitedData=!submitedData\" md-svg-src=\"images/remove.svg\" class=\"parentMinusIcon\"></md-icon> <div ng-show=\"!submitedData\" ng-repeat=\"data in  asset.formSubmitionData\"> <span style=\"padding-left: 20px\">{{data}}</span> </div> </td> <td md-cell>{{asset.name}}</td> <td md-cell>{{asset.activityDate * 1000 | date : \"medium\"}}</td> </tr> </tbody> </table> </md-table-container> <hr style=\"margin-top: 0px\"> </div> <div ui-sortable=\"sortableOptions\" style=\"cursor: pointer\" ng-init=\"campaignMembership = false\" ng-click=\"campaignMembership = !campaignMembership\" ng-show=\"campaignMemberships\"> <img class=\"\" src=\"/img/externalActivities.PNG\"> <strong>Campaign Membership</strong> <strong class=\"pull-right\" style=\"margin-right: 20px;font-size: 16px\">{{campaignMember.length}}</strong> </div> <div ng-show=\"campaignMemberships && campaignMembership && campaignMember.length\"> <div ng-repeat=\"asset in campaignMember\"> <div style=\"padding-bottom: 20px;padding-left: 20px\"> <b>Name : </b> {{asset.name}}  <b> Activity Date : </b>{{asset.activityDate * 1000 | date : \"medium\"}} </div> </div> <hr> </div> </div> </div> <div class=\"panel-body\" ng-show=\"column.type == 'eloqua' && column.json.internalName == 'Activity Breakdown' &&  column.objectname != 'accountBasedActivityBreakdown'\" ng-model=\"column.json.properties\" style=\"padding-top: 45px;min-width: 520px;height:400px\"> <div style=\"height: 32px\" ng-show=\"nameSearch\"> <span style=\"display:flex\"> <input type=\"checkbox\" ng-model=\"checked1\" style=\"width: 12px;height: 12px;margin-right: 2px\"> not <span style=\"margin-left: 20px\">Contains </span> <input type=\"text\" class=\"form-control\" name=\"assetNames\" ng-model=\"assetNames\" style=\"width: 50%;height: 23px;display: inline;margin-left: 6px\" placeholder=\"Name..\"> <md-button style=\"height: 23px;min-width:  46px;margin-bottom:  0px;background-color: #f2f4f5;min-height: 23px;margin-top: 0px\" ng-click=\"filterByName(column , checked1 , assetNames)\"> <md-icon class=\"\" md-svg-icon=\"images/filter_icon.svg\" style=\"min-width: 20px;width:  20px;margin-top: -10px\"></md-icon> </md-button> </span> </div> <div dnd-external-sources=\"false\" dnd-list=\"\" class=\"col-lg-12\" style=\"min-height: 300px; border: 1px solid #dddddd;height: auto;padding-bottom:20px;padding-top: 20px\"> <div style=\"cursor: pointer\" ng-init=\"emailSend = false\" ng-click=\"emailSend = !emailSend\" ng-show=\"emailSends\"> <img class=\"\" src=\"/img/emailSends.PNG\"> <strong>Email Send</strong> <strong class=\"pull-right\" style=\"margin-right: 20px;font-size: 16px\">{{EmailSendContact.length}}</strong> <hr> </div> <div ng-show=\"emailSend && EmailSendContact.length && emailSends\" style=\"border: 1px solid #ddd;max-height : 400px;border-bottom: 0px solid\" class=\"md-whiteframe-3dp\"> <md-toolbar class=\"md-table-toolbar\" ng-hide=\"selected.length || filter.show\" aria-hidden=\"false\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);color: black;z-index: 0\"> <div class=\"md-toolbar-tools\"> <h2 class=\"md-title\" style=\"font-size: 15px\">Email Send</h2> <div flex=\"\" class=\"flex\"></div> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = true\" aria-label=\"filter_list\"> <md-icon class=\"material-icons\" md-svg-src=\"images/filter_list.svg \" style=\"fill:black\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar md-default ng-hide\" ng-show=\"filter.show &amp;&amp; !selected.length\" aria-hidden=\"true\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);z-index: 0\"> <div class=\"md-toolbar-tools\"> <!-- <md-icon class=\"material-icons\" style=\"position:  absolute;padding-top: 4px;font-size: 20px;padding-left: 4px;\">search</md-icon> --> <form flex=\"\" name=\"filter.form\" class=\"ng-pristine ng-valid flex\"> <input type=\"text\" ng-model=\"emailSend1\" ng-change=\"\" placeholder=\"search\" class=\"form-control\" aria-invalid=\"false\" style=\"height: 26px; font-size: initial;width: 200px\"> </form> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = false\" aria-label=\"close\"> <md-icon class=\"material-icons\" md-svg-src=\"images/clear.svg \" style=\"fill:red\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-table-container style=\"max-height : 300px;border-bottom:0px solid\"> <table md-table ng-model=\"selected\" md-progress=\"promise\"> <thead md-head md-order=\"query.order\" md-on-reorder=\"emailSend1\"> <tr md-row> <th md-column><b>Name</b></th> <th md-column><b>Activity Date</b></th> </tr> </thead> <tbody md-body> <tr md-row md-select=\"asset\" ng-repeat=\"asset in EmailSendContact | filter : emailSend1 \"> <td md-cell>{{asset.name}}</td> <td md-cell>{{asset.activityDate * 1000 | date : \"medium\"}}</td> </tr> </tbody> </table> </md-table-container> <hr style=\"margin-top: 0px\"> </div> <div ui-sortable=\"sortableOptions\" style=\"cursor: pointer\" ng-init=\"emailopen = false\" ng-click=\"emailopen = !emailopen\" ng-show=\"emailOpens\"> <img class=\"\" src=\"/img/emailOpen.PNG\"> <strong>Email Open</strong> <strong class=\"pull-right\" style=\"margin-right: 20px;font-size: 16px\">{{EmailOpenContact.length}}</strong> <hr> </div> <div ng-show=\"emailOpens && emailopen && EmailOpenContact.length\" style=\"border: 1px solid #ddd;max-height : 400px;border-bottom: 0px solid\" class=\"md-whiteframe-3dp\"> <md-toolbar class=\"md-table-toolbar\" ng-hide=\"selected.length || filter.show\" aria-hidden=\"false\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);color: black;z-index: 0\"> <div class=\"md-toolbar-tools\"> <h2 class=\"md-title\" style=\"font-size: 15px\">Email Open</h2> <div flex=\"\" class=\"flex\"></div> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = true\" aria-label=\"filter_list\"> <md-icon class=\"material-icons\" md-svg-src=\"images/filter_list.svg \" style=\"fill:black\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar md-default ng-hide\" ng-show=\"filter.show &amp;&amp; !selected.length\" aria-hidden=\"true\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);z-index: 0\"> <div class=\"md-toolbar-tools\"> <!-- <md-icon class=\"material-icons\" style=\"position:  absolute;padding-top: 4px;font-size: 20px;padding-left: 4px;\">search</md-icon> --> <form flex=\"\" name=\"filter.form\" class=\"ng-pristine ng-valid flex\"> <input type=\"text\" ng-model=\"emailOpen1\" ng-change=\"\" placeholder=\"search\" class=\"form-control\" aria-invalid=\"false\" style=\"height: 26px; font-size: initial;width: 200px\"> </form> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = false\" aria-label=\"close\"> <md-icon class=\"material-icons\" md-svg-src=\"images/clear.svg \" style=\"fill:red\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-table-container style=\"max-height : 300px;border-bottom:0px solid\"> <table md-table ng-model=\"selected\" md-progress=\"promise\"> <thead md-head md-order=\"query.order\" md-on-reorder=\"\"> <tr md-row> <th md-column><b>Name</b></th> <th md-column><b>Activity Date</b></th> </tr> </thead> <tbody md-body> <tr md-row md-select=\"asset\" ng-repeat=\"asset in EmailOpenContact | filter : emailOpen1 \"> <td md-cell>{{asset.name}}</td> <td md-cell>{{asset.activityDate * 1000 | date : \"medium\"}}</td> </tr> </tbody> </table> </md-table-container> <hr style=\"margin-top: 0px\"> </div> <div ui-sortable=\"sortableOptions\" style=\"cursor: pointer\" ng-init=\"emailClickThrough = false\" ng-click=\"emailClickThrough = !emailClickThrough\" ng-show=\"emailClick\"> <img class=\"\" src=\"/img/emailClicked.PNG\"> <strong>Email Click</strong> <strong class=\"pull-right\" style=\"margin-right: 20px;font-size: 16px\">{{EmailClickContact.length}}</strong> <hr> </div> <div ng-show=\"emailClick && emailClickThrough && EmailClickContact.length\" style=\"border: 1px solid #ddd;max-height : 400px;border-bottom: 0px solid\" class=\"md-whiteframe-3dp\"> <md-toolbar class=\"md-table-toolbar\" ng-hide=\"selected.length || filter.show\" aria-hidden=\"false\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);color: black;z-index: 0\"> <div class=\"md-toolbar-tools\"> <h2 class=\"md-title\" style=\"font-size: 15px\">Email Click</h2> <div flex=\"\" class=\"flex\"></div> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = true\" aria-label=\"filter_list\"> <md-icon class=\"material-icons\" md-svg-src=\"images/filter_list.svg \" style=\"fill:black\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar md-default ng-hide\" ng-show=\"filter.show &amp;&amp; !selected.length\" aria-hidden=\"true\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);z-index: 0\"> <div class=\"md-toolbar-tools\"> <form flex=\"\" name=\"filter.form\" class=\"ng-pristine ng-valid flex\"> <input type=\"text\" ng-model=\"emailClick1\" ng-change=\"\" placeholder=\"search\" class=\"form-control\" aria-invalid=\"false\" style=\"height: 26px; font-size: initial;width: 200px\"> </form> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = false\" aria-label=\"close\"> <md-icon class=\"material-icons\" md-svg-src=\"images/clear.svg \" style=\"fill:red\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-table-container style=\"max-height : 300px;border-bottom:0px solid\"> <table md-table ng-model=\"selected\" md-progress=\"promise\"> <thead md-head md-order=\"query.order\" md-on-reorder=\"\"> <tr md-row> <th md-column><b>Name</b></th> <th md-column><b>Activity Date</b></th> <th md-column><b>Link</b></th> </tr> </thead> <tbody md-body> <tr md-row md-select=\"asset\" ng-repeat=\"asset in EmailClickContact | filter : emailClick1 \"> <td md-cell>{{asset.name}}</td> <td md-cell>{{asset.activityDate * 1000 | date : \"medium\"}}</td> <td md-cell><a href=\"{{asset.link}}\" target=\"_blank\" style=\"cursor: pointer\">{{asset.link}}</a></td> </tr> </tbody> </table> </md-table-container> <hr style=\"margin-top: 0px\"> </div> <div ui-sortable=\"sortableOptions\" style=\"cursor: pointer\" ng-init=\"webVisit = false\" ng-click=\"webVisit = !webVisit\" ng-show=\"webVisites\"> <img class=\"\" src=\"/img/webVisited.PNG\"> <strong>Web Page Visited</strong> <strong class=\"pull-right\" style=\"margin-right: 20px;font-size: 16px\">{{WebVisitContact.length}}</strong> <hr> </div> <div ng-show=\"webVisites && webVisit && WebVisitContact.length\" style=\"border: 1px solid #ddd;max-height : 400px;border-bottom: 0px solid\" class=\"md-whiteframe-3dp\"> <md-toolbar class=\"md-table-toolbar\" ng-hide=\"selected.length || filter.show\" aria-hidden=\"false\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);color: black;z-index: 0\"> <div class=\"md-toolbar-tools\"> <h2 class=\"md-title\" style=\"font-size: 15px\">Web Visited</h2> <div flex=\"\" class=\"flex\"></div> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = true\" aria-label=\"filter_list\"> <md-icon class=\"material-icons\" md-svg-src=\"images/filter_list.svg \" style=\"fill:black\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar md-default ng-hide\" ng-show=\"filter.show &amp;&amp; !selected.length\" aria-hidden=\"true\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);z-index: 0\"> <div class=\"md-toolbar-tools\"> <!-- <md-icon class=\"material-icons\" style=\"position:  absolute;padding-top: 4px;font-size: 20px;padding-left: 4px;\">search</md-icon> --> <form flex=\"\" name=\"filter.form\" class=\"ng-pristine ng-valid flex\"> <input type=\"text\" ng-model=\"webVisit1\" ng-change=\"\" placeholder=\"search\" class=\"form-control\" aria-invalid=\"false\" style=\"height: 26px; font-size: initial;width: 200px\"> </form> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = false\" aria-label=\"close\"> <md-icon class=\"material-icons\" md-svg-src=\"images/clear.svg \" style=\"fill:red\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-table-container style=\"max-height : 300px;border-bottom:0px solid\"> <table md-table ng-model=\"selected\" md-progress=\"promise\"> <thead md-head md-order=\"query.order\" md-on-reorder=\"\"> <tr md-row> <th md-column><b>Link</b></th> <th md-column><b>Activity Date</b></th> </tr> </thead> <tbody md-body> <tr md-row md-select=\"asset\" ng-repeat=\"asset in WebVisitContact | filter : webVisit1 \"> <td md-cell><a href=\"{{asset.name}}\" target=\"_blank\" style=\"cursor: pointer\">{{asset.name}}</a></td> <td md-cell>{{asset.activityDate * 1000 | date : \"medium\"}}</td> </tr> </tbody> </table> </md-table-container> <hr style=\"margin-top: 0px\"> </div> <div ui-sortable=\"sortableOptions\" style=\"cursor: pointer\" ng-init=\"formSubmit = false\" ng-click=\"formSubmit = !formSubmit\" ng-show=\"formSubmited\"> <img class=\"\" src=\"/img/formSubmition.PNG\"> <strong>Form Submissions</strong> <strong class=\"pull-right\" style=\"margin-right: 20px;font-size: 16px\">{{FormSubmitContact.length}}</strong> <hr> </div> <div ng-show=\"formSubmited && formSubmit && FormSubmitContact.length\" style=\"border: 1px solid #ddd;max-height : 400px;border-bottom: 0px solid\" class=\"md-whiteframe-3dp\"> <!-- <div ng-repeat=\"asset in FormSubmitContact\">  <div style=\"padding-bottom: 20px;padding-left: 20px\"> --> <!-- <b>Name : </b> {{asset.name}} --> <!-- </br> --> <!-- <b> Activity Date : </b>{{asset.activityDate * 1000 | date : \"medium\"}} --> <!-- </br> --> <!-- <md-icon md-svg-src=\"images/add.svg \" ng-init=\"submitedData = true\" class=\"parentPlusIcon\" ng-show = \"submitedData\"   ng-click=\"submitedData =!submitedData\"></md-icon> --> <!-- <md-icon ng-show=\"!submitedData\"  ng-click=\"submitedData=!submitedData\" md-svg-src=\"images/remove.svg\" class=\"parentMinusIcon\"></md-icon> --> <!-- <b>Submited Data</b> --> <!-- <div ng-show=\"!submitedData\" ng-repeat=\"data in  asset.formSubmitionData\"> --> <!-- <span style=\"padding-left: 20px\">{{data}}</span> --> <!-- </div> --> <!-- </div> --> <!-- </div> --> <md-toolbar class=\"md-table-toolbar\" ng-hide=\"selected.length || filter.show\" aria-hidden=\"false\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);color: black;z-index: 0\"> <div class=\"md-toolbar-tools\"> <h2 class=\"md-title\" style=\"font-size: 15px\">Form Submit</h2> <div flex=\"\" class=\"flex\"></div> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = true\" aria-label=\"filter_list\"> <md-icon class=\"material-icons\" md-svg-src=\"images/filter_list.svg \" style=\"fill:black\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar md-default ng-hide\" ng-show=\"filter.show &amp;&amp; !selected.length\" aria-hidden=\"true\" style=\"min-height: 50px;height: 54px;background-color: rgb(250, 250, 250);z-index: 0\"> <div class=\"md-toolbar-tools\"> <form flex=\"\" name=\"filter.form\" class=\"ng-pristine ng-valid flex\"> <input type=\"text\" ng-model=\"formSubmit1\" ng-change=\"\" placeholder=\"search\" class=\"form-control\" aria-invalid=\"false\" style=\"height: 26px; font-size: initial;width: 200px\"> </form> <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"filter.show = false\" aria-label=\"close\"> <md-icon class=\"material-icons\" md-svg-src=\"images/clear.svg \" style=\"fill:red\"></md-icon> <div class=\"md-ripple-container\"></div></button> </div> </md-toolbar> <md-table-container style=\"max-height : 300px;border-bottom:0px solid\"> <table md-table ng-model=\"selected\" md-progress=\"promise\"> <thead md-head md-order=\"query.order\" md-on-reorder=\"\"> <tr md-row> <th md-column><b>Submited Data</b></th> <th md-column><b>Name</b></th> <th md-column><b>Activity Date</b></th> </tr> </thead> <tbody md-body> <tr md-row md-select=\"asset\" ng-repeat=\"asset in FormSubmitContact | filter : formSubmit1\"> <td md-cell><md-icon md-svg-src=\"images/add.svg \" ng-init=\"submitedData = true\" class=\"parentPlusIcon\" ng-show=\"submitedData\" ng-click=\"submitedData =!submitedData\"></md-icon> <md-icon ng-show=\"!submitedData\" ng-click=\"submitedData=!submitedData\" md-svg-src=\"images/remove.svg\" class=\"parentMinusIcon\"></md-icon> <div ng-show=\"!submitedData\" ng-repeat=\"data in  asset.formSubmitionData\"> <span style=\"padding-left: 20px\">{{data}}</span> </div> </td> <td md-cell>{{asset.name}}</td> <td md-cell>{{asset.activityDate * 1000 | date : \"medium\"}}</td> </tr> </tbody> </table> </md-table-container> <hr style=\"margin-top: 0px\"> </div> <div ui-sortable=\"sortableOptions\" style=\"cursor: pointer\" ng-init=\"campaignMembership = false\" ng-click=\"campaignMembership = !campaignMembership\" ng-show=\"campaignMemberships\"> <img class=\"\" src=\"/img/externalActivities.PNG\"> <strong>Campaign Membership</strong> <strong class=\"pull-right\" style=\"margin-right: 20px;font-size: 16px\">{{CampainMemberContact.length}}</strong> </div> <div ng-show=\"campaignMemberships && campaignMembership && CampainMemberContact.length\"> <div ng-repeat=\"asset in CampainMemberContact\"> <div style=\"padding-bottom: 20px;padding-left: 20px\"> <b>Name : </b> {{asset.name}}  <b> Activity Date : </b>{{asset.activityDate * 1000 | date : \"medium\"}} </div> </div> <hr> </div> </div> </div> <div class=\"panel-body\" ng-show=\"column.type == 'eloqua' && column.internalName == 'Activities Graph' && column.objectname == 'accountBasedActivityGraph'\" style=\"padding-top: 45px;min-width: 600px\"> <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" style=\"margin-top: 30px; height: 300px\"> <nvd3 options=\"column.json.options\" data=\"accountActivityGraph\"></nvd3> </div> </div> <div class=\"panel-body\" ng-show=\"column.type == 'eloqua' && column.internalName == 'Activities Graph' && column.objectname != 'accountBasedActivityGraph'\" style=\"padding-top: 45px;min-width: 600px\"> <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" style=\"margin-top: 30px; height: 250px\"> <nvd3 options=\"column.json.options\" data=\"contactActivityGraph\" style=\"width:100%\"></nvd3> </div> </div> <div class=\"panel-body\" ng-show=\"column.type == 'group box 1'\" ui-sortable=\"sortableOptions\" ng-model=\"column.json.properties\" style=\"padding-top: 45px;min-width: 300px\"> <div dnd-list=\"\" dnd-external-sources=\"true\" dnd-drop=\"droppedOnWidget(index, item, external, type, column, $index)\" class=\"col-lg-12\" style=\"min-height: 300px; border: 1px solid #dddddd;height: auto;padding-bottom: 20px\" ng-repeat=\"subcolumn in column.json.properties\"> <div ui-sortable=\"sortableOptions\" ng-model=\"subcolumn\"> <div ng-repeat=\"item in subcolumn\" id=\"{{$index}}\"> <strong uib-tooltip=\"{{item.property.source}}\">{{item.property.label}} </strong> <a href=\"\" ng-hide=\"column.edit\" ng-if=\"delete && item.property.label\" ng-click=\"subcolumn.splice($index, 1); saveWidget(column);\" style=\"text-decoration: none\"> <md-icon class=\"deleteFieldsIcon\" style=\"fill : red\" md-svg-icon=\"images/clear.svg\"> </md-icon> </a> <span ng-if=\"item.property.source == 'eloqua'\" style=\"display: -webkit-box\"> <span ng-if=\"!eloquauser[item.property.internalName]\"> Not Available </span> <span> {{eloquauser[item.property.internalName]}} </span> </span> <span ng-if=\"item.property.source == 'salesforce'\" style=\"display: -webkit-box\"> <span ng-if=\"!item.property.value[0]\"> Not Available </span> <span> {{item.property.value[0]}} </span> </span> </div> </div> </div> </div> <div class=\"panel-body\" ng-show=\"column.type == 'group box 2'\" ui-sortable=\"sortableOptions\" ng-model=\"column.json.properties\" style=\"padding-top: 45px;min-width: 500px\"> <div dnd-external-sources=\"true\" dnd-list=\"\" dnd-drop=\"droppedOnWidget(index, item, external, type, column, $index)\" class=\"col-lg-6\" style=\"min-height: 300px; border: 1px solid #dddddd;height: auto;padding-bottom: 20px\" ng-repeat=\"subcolumn in column.json.properties\"> <div ui-sortable=\"sortableOptions\" ng-model=\"subcolumn\"> <div ng-repeat=\"item in subcolumn\" id=\"{{$index}}\"> <strong uib-tooltip=\"{{item.property.source}}\">{{item.property.label}} </strong> <a href=\"\" ng-hide=\"column.edit\" ng-if=\"delete && item.property.label\" ng-click=\"subcolumn.splice($index, 1); saveWidget(column);\" style=\"text-decoration: none\"> <md-icon class=\"deleteFieldsIcon\" style=\"fill : red\" md-svg-icon=\"images/clear.svg\"> </md-icon> </a> <span ng-if=\"item.property.source == 'eloqua'\" style=\"display: -webkit-box\"> <span ng-if=\"!eloquauser[item.property.internalName]\"> Not Available </span> <span> {{eloquauser[item.property.internalName]}} </span> </span> <span ng-if=\"item.property.source == 'salesforce'\" style=\"display: -webkit-box\"> <span ng-if=\"!item.property.value[0]\"> Not Available </span> <span> {{item.property.value[0]}} </span> </span> </div> </div> </div> </div> <div class=\"panel-body\" ng-show=\"column.type == 'group box 3'\" ui-sortable=\"sortableOptions\" ng-model=\"column.json.properties\" style=\"padding-top: 45px;min-width: 500px\"> <div dnd-external-sources=\"true\" dnd-list=\"\" dnd-drop=\"droppedOnWidget(index, item, external, type, column, $index)\" class=\"col-lg-4\" style=\"min-height: 300px; border: 1px solid #dddddd;height: auto;padding-bottom: 20px\" ng-repeat=\"subcolumn in column.json.properties\"> <div ui-sortable=\"sortableOptions\" ng-model=\"subcolumn\"> <div ng-repeat=\"item in subcolumn\" id=\"{{$index}}\"> <strong uib-tooltip=\"{{item.property.source}}\">{{item.property.label}}</strong> <a href=\"\" ng-hide=\"column.edit\" ng-if=\"delete && item.property.label\" ng-click=\"subcolumn.splice($index, 1); saveWidget(column);\" style=\"text-decoration: none\"> <md-icon class=\"deleteFieldsIcon\" style=\"fill : red\" md-svg-icon=\"images/clear.svg\"></md-icon> </a> <span ng-if=\"item.property.source == 'eloqua'\" style=\"display: -webkit-box\"> <span ng-if=\"!eloquauser[item.property.internalName]\"> Not Available </span> <span> {{eloquauser[item.property.internalName]}} </span> </span> <span ng-if=\"item.property.source == 'salesforce'\" style=\"display: -webkit-box\"> <span ng-if=\"!item.property.value[0]\"> Not Available </span> <span> {{item.property.value[0]}} </span> </span> </div> </div> </div> </div> <div class=\"panel-body\" ng-show=\"column.type == 'list box'\" w style=\"padding-top: 45px;min-width: 500px\"> <div class=\"col-lg-12\" style=\"padding-right: 17px;padding-top: 5px;padding-bottom: 10px\"> <div class=\"pull-right\" events=\"multiselectevents\" ng-dropdown-multiselect=\"\" options=\"salesforceobjectfields[column.json.childSObject]\" selected-model=\"column.json.fields\"></div> </div> <div dnd-external-sources=\"true\" dnd-list=\"\" dnd-drop=\"droppedOnWidget(index, item, external, type, column, $index)\" class=\"col-lg-12\" style=\"min-height: 80px;height: auto; width:auto;min-width: 100%; border: 1px solid #dddddd;padding: 10px\"> <!-- <md-toolbar class=\"md-table-toolbar\" ng-hide=\"itemSelected.length || listfilter\" aria-hidden=\"false\" style=\"min-height: 40px;height: 40px;background-color:white\">\n" +
    "                    <div class=\"md-toolbaWr-tools\">\n" +
    "\n" +
    "                      <div flex=\"\" class=\"flex\"></div>\n" +
    "                      <button class=\"md-icon-button md-button md-ink-ripple\" type=\"button\" ng-click=\"listfilter = true\" aria-label=\"filter_list\">\n" +
    "                        <md-icon class=\"material-icons\">filter_list</md-icon>\n" +
    "                      <div class=\"md-ripple-container\"></div></button>\n" +
    "                    </div>\n" +
    "                  </md-toolbar> --> <md-toolbar class=\"md-table-toolbar md-default\" aria-hidden=\"true\" style=\"min-height: 40px;height: 40px\"> <div class=\"md-toolbar-tools pull-right\"> <span flex=\"100\"></span> <form flex=\"\" class=\"ng-pristine ng-valid flex\"> <input type=\"text\" ng-model=\"query\" ng-change=\"\" placeholder=\"search\" class=\"form-control\" aria-invalid=\"false\" style=\"height: 26px; font-size: initial;width: 200px;margin-right: -20px\"> </form> </div> </md-toolbar> <md-toolbar class=\"md-table-toolbar alternate ng-hide\" ng-show=\"itemSelected.length\" aria-hidden=\"true\"> <div class=\"md-toolbar-tools layout-align-space-between-stretch\" layout-align=\"space-between\"> <div>0 item selected</div> </div> </md-toolbar> <md-table-container style=\"background-color: white;padding-top:25px\"> <table ng-if=\"secondarydata[column.json.parent]\" md-table md-row-select multiple ng-model=\"itemSelected\" md-progress=\"promise\"> <thead md-head md-on-reorder=\"\"> <th md-column ng-repeat=\"field in column.json.fields\">{{field.id}}</th> </thead> <tbody md-body> <tr md-row md-select=\"item\" md-select-id=\"id\" md-auto-select ng-repeat=\"item in widgetdata[column.id]\"> <td md-cell ng-repeat=\"field in column.json.fields\">{{item[field.id]}}</td> </tr> </tbody> </table> </md-table-container> <md-table-pagination ng-if=\"widgetdata[column.id].length\" md-limit=\"query.limit\" md-limit-options=\"[5, 10, 15]\" md-page=\"query.page\" md-total=\"{{itemcount}}\" md-on-paginate=\"\" md-page-select> </md-table-pagination> </div> </div> <div class=\"panel-body\" ng-show=\"column.type == 'pie chart'\" style=\"padding-top: 45px;min-width: 500px\"> <div dnd-external-sources=\"true\" dnd-list=\"\" dnd-drop=\"droppedOnWidget(index, item, external, type, column, $index)\" class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" style=\"margin-top: 30px; height: 500px\"> <nvd3 options=\"column.json.options\" data=\"widgetdata[column.id]\"></nvd3> </div> </div> <div class=\"panel-body\" ng-show=\"column.type == 'line chart'\" style=\"padding-top: 45px;min-width: 500px\"> <div dnd-external-sources=\"true\" dnd-list=\"\" dnd-drop=\"droppedOnWidget(index, item, external, type, column, $index)\" class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" style=\"margin-top: 30px; height: 500px\"> <nvd3 options=\"column.json.options\" data=\"widgetdata[column.id]\"></nvd3> </div> </div> <div class=\"panel-body\" ng-show=\"column.type == 'bar chart'\" style=\"padding-top: 45px;min-width: 500px\"> <div dnd-external-sources=\"true\" dnd-list=\"\" dnd-drop=\"droppedOnWidget(index, item, external, type, column, $index)\" class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" style=\"margin-top: 30px; height: 500px\"> <nvd3 options=\"column.json.options\" data=\"widgetdata[column.id]\"></nvd3> </div> </div> <div class=\"panel-body\" ng-show=\"column.type == 'count box'\" style=\"padding-top: 45px;min-width: 500px\"> <div dnd-external-sources=\"true\" dnd-list=\"\" dnd-drop=\"droppedOnWidget(index, item, external, type, column, $index)\" class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" style=\"margin-top: 30px; height: 500px\"> <strong>{{widgetdata[column.id]}} <strong> {{column.json.childSObject}} </strong></strong></div> </div> </md-content> </div> </div> </div> </md-tab-body> </md-tab> </md-tabs> </md-content> </section> <script>function openSubMenu(self)\n" +
    "   {\n" +
    "    $(self).next('ul').toggle();\n" +
    "  //  alert(\"clicked\");\n" +
    "    e.stopPropagation();\n" +
    "    e.preventDefault();\n" +
    "\n" +
    "  }\n" +
    "\n" +
    "  function showConfirm()\n" +
    "  {\n" +
    "    $(\"#deleteWarning\").show();\n" +
    "  }\n" +
    "$(\".resizable\").resizable({\n" +
    "    handles: 'se',\n" +
    "    aspectRatio: 2 / 1\n" +
    "});</script>"
  );

}]);
