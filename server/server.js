'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();
var path = require('path');
var http = require('http');
var https = require('https');
var sslConfig = require('./ssl-config');
//...
var options = {
  key: sslConfig.privateKey,
  cert: sslConfig.certificate
};
//..
if (process.env.NODE_ENV == 'production') {
  app.use(loopback.static(path.resolve(__dirname, '../dist')));
} else if (process.env.NODE_ENV == 'dev2') {
  app.use(loopback.static(path.resolve(__dirname, '../app')));
  app.use("/bower_components", loopback.static(__dirname +
        "/../bower_components"));
} else {
  app.use(loopback.static(path.resolve(__dirname, '../app')));
  app.use("/bower_components", loopback.static(__dirname +
        "/../bower_components"));
}
app.start = function() {
  var server = null;
  if (process.env.NODE_ENV == 'production') {
    server = https.createServer(options, app);
  } else {
    server = http.createServer(app);
  }
  server.listen(app.get('port'), function() {
    var baseUrl = (process.env.NODE_ENV == 'production' ? 'https://' : 'http://') + app.get('host') + ':' + app.get('port');
    app.emit('started', baseUrl);
    console.log('LoopBack server listening @ %s%s', baseUrl, '/');
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
  return server;
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
