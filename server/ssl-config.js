var path = require('path'),
fs = require("fs");
exports.privateKey = fs.readFileSync(path.join(__dirname, './ssl/PortQiiPrivate.key')).toString();
exports.certificate = fs.readFileSync(path.join(__dirname, './ssl/STAR_portqii_com.crt')).toString();
