'use strict';
var eloquaCall = require('./../eloquaCallBasic');
module.exports = function(app) {
  var eloquaobjects = [
    {
      "name": "Opportunity",
      "label": "Opportunity",
      "type" : "eloqua",
      "json": {
        "fields": [
            {
                "name": "Opportunity ID",
                "internalName": "RemoteOpportunityID",
                "dataType": "string",
                "hasReadOnlyConstraint": false,
                "hasNotNullConstraint": true,
                "hasUniquenessConstraint": false,
                "statement": "{{Opportunity.Id}}",
                "uri": "/opportunities/fields/38"
            },
            {
                "name": "Opportunity Name",
                "internalName": "OpportunityName",
                "dataType": "string",
                "hasReadOnlyConstraint": false,
                "hasNotNullConstraint": true,
                "hasUniquenessConstraint": false,
                "statement": "{{Opportunity.Field(Name)}}",
                "uri": "/opportunities/fields/39"
            },
            {
                "name": "Created Date",
                "internalName": "CreatedDate",
                "dataType": "date",
                "hasReadOnlyConstraint": false,
                "hasNotNullConstraint": true,
                "hasUniquenessConstraint": false,
                "statement": "{{Opportunity.CreatedAt}}",
                "uri": "/opportunities/fields/40"
            },
            {
                "name": "Stage",
                "internalName": "OpportunityStageID",
                "dataType": "string",
                "hasReadOnlyConstraint": false,
                "hasNotNullConstraint": true,
                "hasUniquenessConstraint": false,
                "statement": "{{Opportunity.Field(Stage)}}",
                "uri": "/opportunities/fields/41"
            },
            {
                "name": "Amount",
                "internalName": "OpportunityAmount",
                "dataType": "number",
                "hasReadOnlyConstraint": false,
                "hasNotNullConstraint": true,
                "hasUniquenessConstraint": false,
                "statement": "{{Opportunity.Field(Amount)}}",
                "uri": "/opportunities/fields/42"
            },
            {
                "name": "Currency",
                "internalName": "OpportunityCurrencyID",
                "dataType": "string",
                "hasReadOnlyConstraint": false,
                "hasNotNullConstraint": false,
                "hasUniquenessConstraint": false,
                "statement": "{{Opportunity.Field(Currency)}}",
                "uri": "/opportunities/fields/43"
            },
            {
                "name": "Close Date",
                "internalName": "CloseDate",
                "dataType": "date",
                "hasReadOnlyConstraint": false,
                "hasNotNullConstraint": false,
                "hasUniquenessConstraint": false,
                "statement": "{{Opportunity.Field(CloseDate)}}",
                "uri": "/opportunities/fields/44"
            },
            {
                "name": "Forecast To Close Date",
                "internalName": "ForecastToCloseDate",
                "dataType": "date",
                "hasReadOnlyConstraint": false,
                "hasNotNullConstraint": false,
                "hasUniquenessConstraint": false,
                "statement": "{{Opportunity.Field(ForecastToCloseDate)}}",
                "uri": "/opportunities/fields/45"
            },
            {
                "name": "Account Name",
                "internalName": "AccountName",
                "dataType": "string",
                "hasReadOnlyConstraint": false,
                "hasNotNullConstraint": false,
                "hasUniquenessConstraint": false,
                "statement": "{{Opportunity.Field(AccountName)}}",
                "uri": "/opportunities/fields/46"
            },
            {
                "name": "Territory",
                "internalName": "Territory",
                "dataType": "string",
                "hasReadOnlyConstraint": false,
                "hasNotNullConstraint": false,
                "hasUniquenessConstraint": false,
                "statement": "{{Opportunity.Field(Territory)}}",
                "uri": "/opportunities/fields/47"
            }
        ]
      }
    },
    {
      "name" : "Activity",
      "label": "Activity",
      "type": "eloqua",
      "json": {
        "fields": [
          { "name": "type" },
          { "name": "activityDate" },
          { "name": "activityType" },
          { "name": "asset" },
          { "name": "assetType" }
        ]
      }
    }
  ]
  if(process.env.ELOQUASYNC){
  app.models.account.find(function(err, accounts){
      if (err) throw err;
      accounts.forEach(function(account){
        syncaccount(account);
      });
    });
  }

  function syncaccount(account){
    eloquaobjects.forEach(function(obj){
      app.models.objectjson.create(obj, function(err, created){
        if (err) throw err;
        console.log(obj.name + ' synced');
      });
    });

    var url = "https://secure.p02.eloqua.com/api/REST/1.0/assets/customObjects?depth=complete";
    eloquaCall.get({
      host	: url,
      oauth	: true,
      token	: account.credentials.eloqua.access_token,
      siteId: account.siteId
    }).then(function(result) {
        result = JSON.parse(result);
        result.elements.forEach(function(obj){
          console.log(obj);
          var data = {
            type : 'eloqua',
            name : obj.name,
            label: obj.name,
            json: obj
          }
           app.models.objectjson.create(data, function(err, created){
             if (err) throw err;
             console.log(obj.name + ' synced');
           });
        });
      })
      .catch(function(error) {
        throw error;
      });
  }

};
