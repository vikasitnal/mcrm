'use strict';
var _ = require('lodash');
var url = require('url');

module.exports = function(Account) {
var request = require('request-promise');
var async = require('async');
var Q = require('q');
var eloquaCall = require('./../../server/eloquaCallBasic');
var eloquaConfig = {
 "TokenUrl" : 	"https://login.eloqua.com/auth/oauth2/token/",
 "redirectUri" : "https://sales-intelligence.portqii.com/api/accounts/saveToken",
 "AppId" : 		"8b9b4b29-bd2a-4c4e-b575-8aff42f48bf2",
 "ClientSecret": "1lo~dg-babk-6hmFOJ7qU7PD7uQclSyzeZ7cityTOmAS7Cme-lVtD7uqQslkkbaTfJihw0eIMgETjrnkAmWpoG0toT6888xnn7by"
};

var jsforce = require('jsforce');
var conn = new jsforce.Connection({
});

var urlref = {
  "Latest_Acitivities": "https://secure.p02.eloqua.com/api/REST/1.0/data/activities/contact/{id}?startDate={startDate}&endDate={endDate}&type={type}"
}


      //search eloqua contacts
      Account.search = function(id, req, res, callback) {
        Account.findById(id).then(function(account){
          var url = "https://secure.p02.eloqua.com/api/REST/1.0/data/contacts?search=%27*" + req.query.query + "*%27&depth=complete&count=10&page=" + req.query.page;
            eloquaCall.get({
              host	: url,
              oauth	: true,
              token	: account.credentials.eloqua.access_token,
              siteId: account.siteId
            }).then(function(result) {
              result.result = JSON.parse(result.result);
              res.status(200).send(result.result);
            })
            .catch(function(error) {
              callback(error)

            });

        });
      };


      Account.remoteMethod(
          'search', {
              accepts: [{
                  arg: 'id',
                  type: 'string'
              }, {
                  arg: 'req',
                  type: 'object',
                  http: {
                      source: 'req'
                  }
              }, {
                  arg: 'res',
                  type: 'object',
                  http: {
                      source: 'res'
                  }
              }],
              http: {
                  path: '/:id/search',
                  verb: 'get'
              }
          }
      );






            //search eloqua users
            Account.searchusers = function(id, req, res, callback) {
              Account.findById(id).then(function(account){
                var url = "https://secure.p02.eloqua.com/API/REST/2.0/system/users?search=%27*" + req.query.query + "*%27&depth=complete&count=10&page=" + req.query.page;
                  eloquaCall.get({
        						host	: url,
        						oauth	: true,
        						token	: account.credentials.eloqua.access_token,
                    siteId: account.siteId
        					}).then(function(result) {
                    result.result = JSON.parse(result.result);
                    res.status(200).send(result.result);
                  })
                  .catch(function(error) {
                    callback(error)
                  });
              });
            };


            Account.remoteMethod(
                'searchusers', {
                    accepts: [{
                        arg: 'id',
                        type: 'string'
                    }, {
                        arg: 'req',
                        type: 'object',
                        http: {
                            source: 'req'
                        }
                    }, {
                        arg: 'res',
                        type: 'object',
                        http: {
                            source: 'res'
                        }
                    }],
                    http: {
                        path: '/:id/searchusers',
                        verb: 'get'
                    }
                }
            );




            //enable eloqua
            Account.enable = function(req, res, callback) {
              var query = req.query;
              console.log(query)
              Account.findOne({where: {siteName: req.query.siteName}}).then(function(account){
                if(account && account.id){
                  var callBackUrl 	= query.CallbackUrl;
                  var info 			= callBackUrl + '|' + query.siteName + '|' + query.siteId;
                  var clientId 		= eloquaConfig.AppId;
                  var redirectUri 	= eloquaConfig.redirectUri;
                  //rediercting for OAUTH to get token
                  res.redirect('https://login.eloqua.com/auth/oauth2/authorize?response_type=code&client_id=' + clientId + '&redirect_uri=' + redirectUri + '&scope=full&state=' + info +', method="auto",code=302');
                } else {
                  Account.create({siteId: query.siteId, siteName: query.siteName}).then(function(account){
                    var callBackUrl 	= query.CallbackUrl;
                    var info 			= callBackUrl + '|' + query.siteName + '|' + query.siteId;
                    var clientId 		= eloquaConfig.AppId;
                    var redirectUri 	= eloquaConfig.redirectUri;
                    //rediercting for OAUTH to get token
                    res.redirect('https://login.eloqua.com/auth/oauth2/authorize?response_type=code&client_id=' + clientId + '&redirect_uri=' + redirectUri + '&scope=full&state=' + info +', method="auto",code=302');
                  });
                }
              })
            };


            Account.remoteMethod(
                'enable', {
                    accepts: [ {
                        arg: 'req',
                        type: 'object',
                        http: {
                            source: 'req'
                        }
                    }, {
                        arg: 'res',
                        type: 'object',
                        http: {
                            source: 'res'
                        }
                    }],
                    http: {
                        path: '/enable',
                        verb: 'post'
                    }
                }
            );

            //save eloqua eloqua tokens
            Account.savetoken = function(req, res, callback) {
              var getData 		= req.query;
              var callBackUrl 	= getData.state.substr(0, getData.state.indexOf("|"));
              var state = getData.state.split('|');
              var siteName = state[1];
              Account.findOne({where: { siteName: siteName}}).then(function(account){
                eloquaCall.post({
                  host 	 : eloquaConfig.token,
                  username : eloquaConfig.AppId,
                  password : eloquaConfig.ClientSecret,
                  body: {
                    'grant_type' 	: 	'authorization_code',
                    'code'			: 	getData.code,
                    'redirect_uri'	:	eloquaConfig.redirectUri
                  }
                }).then(function(response) {
                    console.log(account);
                    account.credentials = {};
                    account.credentials.eloqua = response;
                    account.save();
                    eloquaCall.get({
                      host	: 'https://login.eloqua.com/id',
                      oauth	: true,
                      token	: response.access_token,
                      siteId : account.siteId
                    })
                      .then(function(response) {
                        account.credentials.eloqua.info = JSON.parse(response);
                        account.save();
                        res.redirect(callBackUrl);
                    }, callback)

                }, callback)
              });
            };


            Account.remoteMethod(
                'savetoken', {
                    accepts: [ {
                        arg: 'req',
                        type: 'object',
                        http: {
                            source: 'req'
                        }
                    }, {
                        arg: 'res',
                        type: 'object',
                        http: {
                            source: 'res'
                        }
                    }],
                    http: {
                        path: '/savetoken',
                        verb: 'get'
                    }
                }
            );

	    Account.accountBasedAnalytics = function(id, contactId, source, object, req, res, callback)
		{
           console.log('accountBasedAnalytics');
		   var query = req.query;
		   Account.findById(id, function(err, account){
             if (err) callback(err);

            var url = urlref[object];
            url  = url.replace('{id}', contactId);
            if(object == 'Latest_Acitivities')
            {
              url  = url.replace('{startDate}', query.fromdate);
              url  = url.replace('{endDate}', query.todate);
              url  = url.replace('{type}', query.type);
            }

            eloquaCall.get({
              host	: url,
              oauth	: true,
              token	: account.credentials.eloqua.access_token,
              siteId: account.siteId
            }).then(function(result) {
              if(result && result.result){
                result = JSON.parse(result.result);
                if(result.length > 0){
                  res.status(200).send({result: result});
                } else {
                  res.status(200).send({result: [] });
                }
              } else {
                res.status(200).send({result: [] });
              }
              })
              .catch(function(error) {
                callback(error)
              });
		 });

		}

           Account.remoteMethod('accountBasedAnalytics', {
              accepts: [{
                arg: 'id',
                type: 'string'
              },
              {
                arg: 'contactId',
                type: 'string'
              },
              {
                arg: 'source',
                type: 'string'
              },
              {
                arg: 'object',
                type: 'string'
              },
              {
                arg: 'req',
                type: 'object',
                http: {
                  source: 'req'
                }
              }, {
                arg: 'res',
                type: 'object',
                http: {
                  source: 'res'
                }
              }],
              http: {
                path: '/:id/accountBasedAnalytics/:source/:object/:contactId',
                verb: 'get'
              }
            });



		
    function formedQuerySearch(searchString,account,onComplete,onError) {
        var url = "https://secure.p02.eloqua.com/API/REST/1.0/data/contacts?search"+searchString;

        eloquaCall.get({
            host: url,
            oauth: true,
            token: account.credentials.eloqua.access_token,
            siteId: account.siteId
        }).then(onComplete)
        .catch(onError);
    }



    Account.accountBasedContacts = function(id, contactId, source, object, req, res, callback) {
        var resdata = {};
        var query = req.query;
        var contactData = {};
        var relatedContactData = [];






        Account.findById(id, function(err, account) {
            if (err) callback(err);
            if(req.query && req.query.sfdcaccountId){
                // console.log(account.sfdcAccountIdField);


                formedQuerySearch("=C_SFDCAccountID='"+req.query.sfdcaccountId+"'",account,
                    function(response) {
                        var result = JSON.parse(response.result);
                        var companyList = [],
                            nonGroupedCompanyList = [],
                            countSet = [];

                        if(result && result.elements){

                            var searchString = '=';

                            var originalSFDCData = result;

                            if(originalSFDCData.elements.length){



                                originalSFDCData.elements.forEach(function(item) {
                                    if(item.emailAddress){
                                        var domain = item.emailAddress.split("@")[1];
                                        if(domain){
                                            var company = domain.split('.')[0];
                                            nonGroupedCompanyList.push("@"+company);
                                        }
                                    }
                                });



                                _.mapKeys(_.countBy(nonGroupedCompanyList),function(value,key){
                                    countSet.push({
                                        value:key,count:value
                                    });
                                    return value;
                                });

                                var maxValue = Math.max.apply(null,countSet.map(function(item) {
                                    return +item.count;
                                }));


                                _.filter(countSet,{count:maxValue}).forEach(function(item) {
                                    searchString += "emailAddress='*"+item.value+"*'";
                                });

                                

                                formedQuerySearch(searchString,account,
                                function(response) {
                                    var rankingResult = response && response.result;
                                    rankingResult = JSON.parse(rankingResult);

                                    rankingResult.elements = _.uniqBy(originalSFDCData.elements.concat(rankingResult.elements),'emailAddress');
                                    rankingResult.total = rankingResult.elements.length;

                                    res.status(200).send(rankingResult);


                                },
                                function(error) {
                                    res.status(500).send(err);
                                });

                            } else {

                                res.status(200).send(originalSFDCData);
                            }
                        } else {
                            res.status(200).send(result);
                        }
                        /*if(req.query.byranking && req.query.byranking=='true'){
                        } else {
                            res.status(200).send(result);
                        }*/
                    },
                    function(error) {
                        res.status(500).send(err);
                    }
                );

                /*var cfUrl = "https://secure.p02.eloqua.com/API/REST/1.0/data/contacts?search=C_SFDCAccountID=*"+req.query.sfdcaccountId+"*";

                eloquaCall.get({
                    host: cfUrl,
                    oauth: true,
                    token: account.credentials.eloqua.access_token,
                    siteId: account.siteId
                }).then(function(result) {
                    result = JSON.parse(result.result);
                    relatedContactData = result;

                    var nonGroupedCompanyList = [];
                    var companyList = [];

                    if(req.query.byranking && req.query.byranking=='true'){
                        if(result && result.elements){
                            result.elements.forEach(function(item) {
                                if(item.emailAddress){
                                    var domain = item.emailAddress.split("@")[1];
                                    if(domain){
                                        var company = domain.split('.')[0];
                                        nonGroupedCompanyList.push(company);
                                    }
                                }
                            });
                            companyList = _.uniq(nonGroupedCompanyList);
                            var searchString = '=';

                            companyList.forEach(function(searchTerm) {
                                searchString += "emailAddress='"+searchTerm+"'";
                            });

                            console.log(searchString);

                        }
                        res.status(200).send(result);
                    } else {
                        res.status(200).send(result);
                    }
                    var contactWA = {};
                    var url = urlref[object];


                }, function(err) {
                    if (err) {
                        res.status(500).send(err);

                    } else {
                        res.status(200).send({
                            result: result
                        });

                    }
                });*/



                /*res.send({
                    elements:[]
                });*/



            }else{

                if (source == 'eloqua') {
                    var url = "https://secure.p02.eloqua.com/api/REST/1.0/data/contact/" + contactId;

                    eloquaCall.get({
                        host: url,
                        oauth: true,
                        token: account.credentials.eloqua.access_token,
                        siteId: account.siteId
                    }).then(function(result) {
                        if (result && result.result) {
                            result = JSON.parse(result.result);
                        } else {
                            result = [];
                        }
                        contactData = result;
                        var mainContactEmailAddress = contactData.emailAddress;
                        var domain = mainContactEmailAddress.split("@");

                        var cfUrl = "https://secure.p02.eloqua.com/API/REST/1.0/data/contacts?search=email=*" + domain[1] + "*";

                        console.log("******************")
                        console.log("******************")
                        console.log("*******uuuu*******")
                        console.log(cfUrl);
                        console.log("******************")
                        console.log("******************")
                        console.log("******************")



                        eloquaCall.get({
                            host: cfUrl,
                            oauth: true,
                            token: account.credentials.eloqua.access_token,
                            siteId: account.siteId
                        }).then(function(result) {
                            result = JSON.parse(result.result);
                            console.log(result);
                            relatedContactData = result;
                            res.status(200).send(result);
                            var contactWA = {};
                            var url = urlref[object];


                        }, function(err) {
                            if (err) {
                                res.status(500).send(err);

                            } else {
                                res.status(200).send({
                                    result: result
                                });

                            }
                        });
                    });
                }
            }
        });

    }


           Account.remoteMethod('accountBasedContacts', {
              accepts: [{
                arg: 'id',
                type: 'string'
              },
              {
                arg: 'contactId',
                type: 'string'
              },
              {
                arg: 'source',
                type: 'string'
              },
              {
                arg: 'object',
                type: 'string'
              },
              {
                arg: 'req',
                type: 'object',
                http: {
                  source: 'req'
                }
              }, {
                arg: 'res',
                type: 'object',
                http: {
                  source: 'res'
                }
              }],
              http: {
                path: '/:id/accountBasedContacts/:source/:object/:contactId',
                verb: 'get'
              }
            });


      Account.secondarydata = function(id, contactId, source, object, req, res, callback)
       {
        var resdata = {};
        var query = req.query;
        Account.findById(id, function(err, account){
          if (err) callback(err);
          if(source == 'eloqua')
          {
            var url = urlref[object];
            url  = url.replace('{id}', contactId);
            if(object == 'Latest_Acitivities')
            {
              url  = url.replace('{startDate}', query.fromdate);
              url  = url.replace('{endDate}', query.todate);
              url  = url.replace('{type}', query.type);
            }
              eloquaCall.get({
    						host	: url,
    						oauth	: true,
    						token	: account.credentials.eloqua.access_token,
                siteId: account.siteId
    					})
              .then(function(result) {
                if(result && result.result){
                  result = JSON.parse(result.result);
                  if(result.length > 0){
                    res.status(200).send({result: result});
                  } else {
                    res.status(200).send({result: [] });
                  }
                } else {
                  res.status(200).send({result: [] });
                }
              })
              .catch(function(error) {
                callback(error)
              });
          } else if (source == 'salesforce') {
            conn.login(account.credentials.salesforce.username, account.credentials.salesforce.password + account.credentials.salesforce.secret, function(err, userInfo) {
              if (err) {
                 callback(err);
               }
               var sfquery;
               if(query.count){
                  sfquery = "SELECT count() FROM " + object + " WHERE " + query.key + " = '" + query.value + "'";
               } else {
                 if(query.search){
                   sfquery = "FIND '"+ query.search +"*' IN ALL FIELDS RETURNING " + object + " (" + query.fields + ")" + + " WHERE " + query.key + " = '" + query.value + "'";
                 } else {
                   sfquery = "SELECT " + query.fields + " FROM " + object + " WHERE " + query.key + " = '" + query.value + "'";
                 }
                  // if(query.filterfield){
                  //   if(query.filterfieldtype == 'string'){
                  //     sfquery += " " + query.filterfield + " LIKE "+ query.filterfieldvalue;
                  //   } else if (query.filterfieldtype == 'integer') {
                  //     sfquery += " " + query.filterfield + " ";
                  //     if(query.filtertype == 'greater'){
                  //       sfquery += "> " + query.filterfieldvalue;
                  //     } else if (query.filtertype == 'lesser') {
                  //       sfquery += "< " + query.filterfieldvalue;
                  //     } else if (query.filtertype == 'equal') {
                  //       sfquery += "= " + query.filterfieldvalue;
                  //     } else if (query.filtertype == 'greater or equal') {
                  //       sfquery += ">= " + query.filterfieldvalue;
                  //     } else if (query.filtertype == 'lesser or equal') {
                  //       sfquery += "<= " + query.filterfieldvalue;
                  //     }
                  //   } else if (query.filtertype == 'boolean') {
                  //     sfquery += " " + query.filterfield + "=" + query.filterfieldvalue;
                  //   } else if (query.filtertype == 'datetime' || query.filtertype == 'date'|| query.filtertype == 'time') {
                  //     if(query.filtertype == 'greater'){
                  //       sfquery += "> " + query.filterfieldvalue;
                  //     } else if (query.filtertype == 'lesser') {
                  //       sfquery += "< " + query.filterfieldvalue;
                  //     } else if (query.filtertype == 'equal') {
                  //       sfquery += "= " + query.filterfieldvalue;
                  //     } else if (query.filtertype == 'greater or equal') {
                  //       sfquery += ">= " + query.filterfieldvalue;
                  //     } else if (query.filtertype == 'lesser or equal') {
                  //       sfquery += "<= " + query.filterfieldvalue;
                  //     }
                  //   }
                  // }
                  if(query.offset){
                    sfquery += "OFFSET " + query.offset + "LIMIT " + query.limit;
                  }
               }
               console.log(sfquery)

              conn.query(sfquery, function(err, records) {
                if (err) { callback(err); }
                res.status(200).send(records);
              });
            });
          }
        });
      };


            Account.remoteMethod(
                'secondarydata', {
                    accepts: [{
                        arg: 'id',
                        type: 'string'
                    },
                    {
                        arg: 'contactId',
                        type: 'string'
                    },
                    {
                        arg: 'source',
                        type: 'string'
                    },
                    {
                        arg: 'object',
                        type: 'string'
                    },
                     {
                        arg: 'req',
                        type: 'object',
                        http: {
                            source: 'req'
                        }
                    }, {
                        arg: 'res',
                        type: 'object',
                        http: {
                            source: 'res'
                        }
                    }],
                    http: {
                        path: '/:id/secondarydata/:source/:object/:contactId',
                        verb: 'get'
                    }
                }
            );



            Account.chartdata = function(id, source, object,key, value, groupby, req, res, callback) {
              var resdata = {};
              var query = req.query;
              Account.findById(id, function(err, account){
                if (err) callback(err);
                if(source == 'eloqua'){
                  var url = urlref[object];
                  url  = url.replace('{id}', contactId);
                  if(object == 'Activities'){
                    url  = url.replace('{startDate}', req.query.startDate);
                    url  = url.replace('{endDate}', req.query.endDate);
                    url  = url.replace('{type}', req.query.type);
                  }

                    eloquaCall.get({
          						host	: url,
          						oauth	: true,
          						token	: account.credentials.eloqua.access_token,
                      siteId: account.siteId
          					})
                    .then(function(result) {
                      if(result && result.result){
                        result = JSON.parse(result.result);
                        if(result.length > 0){
                          res.status(200).send({result: result});
                        } else {
                          res.status(200).send({result: [] });
                        }
                      } else {
                        res.status(200).send({result: [] });
                      }
                    })
                    .catch(function(error) {
                      callback(error)
                    });
                } else if (source == 'salesforce') {
                  if(req.query.fieldtype == 'picklist'){
                    var sfquery = "SELECT " + groupby + ", COUNT(Id) FROM " + object + " WHERE " + key + " = '" + value + "' GROUP BY " + groupby;
                  } else if (req.query.fieldtype == 'datetime') {
                    if(req.query.interval == 'year'){
                      var sfquery = "SELECT Calendar_Year(" + groupby + "), COUNT(Id) FROM " + object + " WHERE " + key + " = '" + value + "' GROUP BY Calendar_Year(" + groupby + ")";
                    } else if (req.query.interval == 'month') {
                      var sfquery = "SELECT Calendar_Month(" + groupby + "), COUNT(Id) FROM " + object + " WHERE " + key + " = '" + value + "' GROUP BY Calendar_Month(" + groupby + ")";
                    } else if (req.query.interval == 'day') {
                      var sfquery = "SELECT Day_Only(" + groupby + "), COUNT(Id) FROM " + object + " WHERE " + key + " = '" + value + "' GROUP BY Day_Only(" + groupby + ")";
                    }
                  }
                  conn.login(account.credentials.salesforce.username, account.credentials.salesforce.password + account.credentials.salesforce.secret, function(err, userInfo) {
                    if (err) {
                       callback(err);
                     }
                    conn.query(sfquery, function(err, records) {
                      if (err) { callback(err); }
                      res.status(200).send(records);
                    });
                  });
                }
              });
            };


                  Account.remoteMethod(
                      'chartdata', {
                          accepts: [{
                              arg: 'id',
                              type: 'string'
                          },
                          {
                              arg: 'source',
                              type: 'string'
                          },
                          {
                              arg: 'object',
                              type: 'string'
                          },
                          {
                              arg: 'key',
                              type: 'string'
                          },
                          {
                              arg: 'value',
                              type: 'string'
                          },
                          {
                              arg: 'groupby',
                              type: 'string'
                          },
                           {
                              arg: 'req',
                              type: 'object',
                              http: {
                                  source: 'req'
                              }
                          }, {
                              arg: 'res',
                              type: 'object',
                              http: {
                                  source: 'res'
                              }
                          }],
                          http: {
                              path: '/:id/chartdata/:source/:object/:key/:value/:groupby',
                              verb: 'get'
                          }
                      }
                  );




            Account.secondaryproperty = function(id, source, object, key, value, req, res, callback) {
              var resdata = {};
              var query = req.query;
              Account.findById(id, function(err, account){
                if (err) callback(err);
                if(source == 'eloqua'){
                  var url = urlref[object];
                  url  = url.replace('{id}', contactId);
                  if(object == 'Activities'){
                    url  = url.replace('{startDate}', req.query.startDate);
                    url  = url.replace('{endDate}', req.query.endDate);
                    url  = url.replace('{type}', req.query.type);
                  }

                    eloquaCall.get({
          						host	: url,
          						oauth	: true,
          						token	: account.credentials.eloqua.access_token,
                      siteId: account.siteId
          					})
                    .then(function(result) {
                      if(result && result.result){
                        result = JSON.parse(result.result);
                        if(result.length > 0){
                          res.status(200).send({result: result});
                        } else {
                          res.status(200).send({result: [] });
                        }
                      } else {
                        res.status(200).send({result: [] });
                      }
                    })
                    .catch(function(error) {
                      callback(error)
                    });
                } else if (source == 'salesforce') {
                  console.log(object, key, value, query);
                  conn.login(account.credentials.salesforce.username, account.credentials.salesforce.password + account.credentials.salesforce.secret, function(err, userInfo) {
                    if (err) {
                       callback(err);
                     }
                    var sfquery = "SELECT " + query.fields + " FROM " + object + " WHERE " + key + " = '" + value + "'";
                    conn.query(sfquery, function(err, records) {
                      if (err) { callback(err); }
                      if(object == 'Account'){
                        console.log("Accounts", records);
                      }
                      res.status(200).send(records);
                    });
                  });
                }
              });
            };


                  Account.remoteMethod(
                      'secondaryproperty', {
                          accepts: [{
                              arg: 'id',
                              type: 'string'
                          },
                          {
                              arg: 'source',
                              type: 'string'
                          },
                          {
                              arg: 'object',
                              type: 'string'
                          },
                          {
                              arg: 'key',
                              type: 'string'
                          },
                          {
                              arg: 'value',
                              type: 'string'
                          },
                           {
                              arg: 'req',
                              type: 'object',
                              http: {
                                  source: 'req'
                              }
                          }, {
                              arg: 'res',
                              type: 'object',
                              http: {
                                  source: 'res'
                              }
                          }],
                          http: {
                              path: '/:id/secondaryproperty/:source/:object/:key/:value',
                              verb: 'get'
                          }
                      }
                  );



    function getContactDetail(contactId,account,res,callback) {
        var url = "https://secure.p02.eloqua.com/api/REST/1.0/data/contact/" + contactId;
        var resdata = {
            eloqua:[],
            salesforce:[]
        };


        eloquaCall.get({
                host: url,
                oauth: true,
                token: account.credentials.eloqua.access_token,
                siteId: account.siteId
            }).then(function(result) {
                
                result.result = JSON.parse(result.result);
                resdata.eloqua = result.result;

                conn.login(account.credentials.salesforce.username, account.credentials.salesforce.password + account.credentials.salesforce.secret, function(err, userInfo) {
                    if (err) {
                        callback(err);
                    } else {
                        conn.sobject("Contact")
                            .find({
                                email: result.emailAddress
                            })
                            .execute(function(err, records) {
                                if (err) {
                                    callback(err);
                                }
                                if (records && records.length > 0) {

                                    resdata.salesforce = records[0];
                                }
                                res.status(200).send(resdata);
                            });
                    }
                });
            })
            .catch(function(error) {
                callback(error);
            });
    }

      //signup method for appuser
    Account.findcontact = function(id, contactId, req, res, callback) {
        var resdata = {};
        var urltype = req.query.type;

        Account.findById(id).then(function(account) {
            var url;


            var params = JSON.parse(req.query.email);

            if (params && params.email) {


                var url = "https://secure.p02.eloqua.com/api/REST/1.0/data/contacts?search=%27*" + params.email + "*%27&depth=complete&count=10&page=" + 1;
                eloquaCall.get({
                        host: url,
                        oauth: true,
                        token: account.credentials.eloqua.access_token,
                        siteId: account.siteId
                    }).then(function(response) {
                        var searchedContactData = JSON.parse(response.result);
                        if(searchedContactData && searchedContactData.elements && searchedContactData.elements.length){

                            var contactId = searchedContactData.elements[0].id;
                            getContactDetail(contactId,account,res,callback);

                        } else {

                            getContactDetail(contactId,account,res,callback);
                        }
                    })
                    .catch(function(error) {
                        callback(error)

                    });
                

            } else {

                if (urltype == 'id') {
                    getContactDetail(contactId,account,res,callback);
                } else if (urltype == 'sfdcaccountId') {
                    url = "https://secure.p02.eloqua.com/API/REST/1.0/data/contacts?search=" + account.sfdcAccountIdField + "=" + contactId;
                }

            }
            
        });
    };


      Account.remoteMethod(
          'findcontact', {
              accepts: [{
                  arg: 'id',
                  type: 'string'
              },
              {
                  arg: 'contactId',
                  type: 'string'
              }, {
                  arg: 'req',
                  type: 'object',
                  http: {
                      source: 'req'
                  }
              }, {
                  arg: 'res',
                  type: 'object',
                  http: {
                      source: 'res'
                  }
              }],
              http: {
                  path: '/:id/findcontact/:contactId',
                  verb: 'get'
              }
          }
      );




      //signup method for appuser
      Account.eloquaobjectfields = function(id, object, req, res, callback) {
        Account.findById(id).then(function(account){

          var url = "https://secure.p02.eloqua.com/api/bulk/2.0/" + object + "/imports";

          eloquaCall.get({
            host	: url,
            oauth	: true,
            token	: account.credentials.eloqua.access_token,
            siteId: account.siteId
          }).then(function(result) {
              result.result = JSON.parse(result.result);
              res.status(200).send(result.result);
            })
            .catch(function(error) {
              callback(error)

            });

        });
      };


      Account.remoteMethod(
          'eloquaobjectfields', {
              accepts: [
                {
                    arg: 'id',
                    type: 'string'
                },
                {
                    arg: 'object',
                    type: 'string'
                },
               {
                  arg: 'req',
                  type: 'object',
                  http: {
                      source: 'req'
                  }
              }, {
                  arg: 'res',
                  type: 'object',
                  http: {
                      source: 'res'
                  }
              }],
              http: {
                  path: '/:id/eloqua/objectfields/:object',
                  verb: 'get'
              }
          }
      );

      //signup method for appuser
      Account.contactfields = function(id, objname, req, res, callback) {
        Account.findById(id).then(function(account){
         var url = "https://secure.p02.eloqua.com/api/REST/1.0/assets/" + objname + "/fields?search=%27*" + req.query.query + "*%27&depth=complete";
          console.log("url "+url);

          eloquaCall.get({
            host	: url,
            oauth	: true,
            token	: account.credentials.eloqua.access_token,
            siteId: account.siteId
          }).then(function(result) {
              result.result = JSON.parse(result.result);
              res.status(200).send(result.result);
            })
            .catch(function(error) {
              callback(error)

            });

        });
      };


      Account.remoteMethod(
          'contactfields', {
              accepts: [
                {
                    arg: 'id',
                    type: 'string'
                },
                 {
                    arg: 'objname',
                    type: 'string'
                },
               {
                  arg: 'req',
                  type: 'object',
                  http: {
                      source: 'req'
                  }
              }, {
                  arg: 'res',
                  type: 'object',
                  http: {
                      source: 'res'
                  }
              }],
              http: {
                  path: '/:id/eloqua/contactfields/:objname',
                  verb: 'get'
              }
          }
      );




      //signup method for appuser
      Account.salesforcecontactfields = function(id, req, res, callback) {
        Account.findById(id).then(function(account){
          Account.app.models.objectjson.findOne({"where": {"name": "Contact"}}, function(err, result){
            if (err) throw err;
            res.status(200).send(result.result);
          });
        });
      };


      Account.remoteMethod(
          'salesforcecontactfields', {
              accepts: [
                {
                    arg: 'id',
                    type: 'string'
                },
               {
                  arg: 'req',
                  type: 'object',
                  http: {
                      source: 'req'
                  }
              }, {
                  arg: 'res',
                  type: 'object',
                  http: {
                      source: 'res'
                  }
              }],
              http: {
                  path: '/:id/salesforce/contactfields',
                  verb: 'get'
              }
          }
      );



            //signup method for appuser
            Account.salesforceobjectfields = function(id, object, req, res, callback) {
              Account.findById(id).then(function(account){
                Account.app.models.objectjson.findOne({"where": {"name": object}}, function(err, result){
                  if (err) throw err;
                  res.status(200).send(result.result);
                });
              });
            };


            Account.remoteMethod(
                'salesforceobjectfields', {
                    accepts: [
                      {
                          arg: 'id',
                          type: 'string'
                      },
                      {
                          arg: 'object',
                          type: 'string'
                      },
                     {
                        arg: 'req',
                        type: 'object',
                        http: {
                            source: 'req'
                        }
                    }, {
                        arg: 'res',
                        type: 'object',
                        http: {
                            source: 'res'
                        }
                    }],
                    http: {
                        path: '/:id/salesforce/:object/fields',
                        verb: 'get'
                    }
                }
            );


};
