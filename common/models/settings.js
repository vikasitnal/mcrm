'use strict';
var async = require('async');
var _ = require('lodash');
module.exports = function(Settings) {


        Settings.reset = function(id, req, res, callback) {
          Settings.findById(id).then(function(settings){
            settings.backupjson = {tabs: []};
            Settings.app.models.account.findById(settings.accountId).then(function(account){
              Settings.app.models.tab.find({where: {settingsId: settings.id }}).then(function(tabs){
                async.forEach(tabs, function(tab, cb){
                  var currenttab = {
                    name: tab.name,
                    description: tab.description
                  };
                  Settings.app.models.widget.find({where: {tabId: tab.id }}).then(function(widgets){
                    console.log(widgets);
                    currenttab.widgets = widgets;
                    settings.backupjson.tabs.push(currenttab);
                    tab.widgets.destroyAll(function(destroyed){
                      tab.remove();
                      cb();
                    });
                  });

                }, function(){
                  async.forEach(account.defaultjson.tabs, function(tab, cb2){
                    tab.settingsId = settings.id;
                    Settings.app.models.tab.create(tab).then(function(newtab){
                      newtab.widgets.create(tab.widgets).then(function(newwidgets){
                        cb2();
                      });
                    });
                  }, function(){
                    settings.save();
                    res.status(204).send();
                  });
                });
              });
            });
          });
        };


        Settings.remoteMethod(
            'reset', {
                accepts: [{
                    arg: 'id',
                    type: 'string'
                }, {
                    arg: 'req',
                    type: 'object',
                    http: {
                        source: 'req'
                    }
                }, {
                    arg: 'res',
                    type: 'object',
                    http: {
                        source: 'res'
                    }
                }],
                http: {
                    path: '/:id/reset',
                    verb: 'get'
                }
            }
        );



                Settings.revert = function(id, req, res, callback) {
                  Settings.findById(id).then(function(settings){
                    Settings.app.models.account.findById(settings.accountId).then(function(account){
                      Settings.app.models.tab.find({where: {settingsId: settings.id }}).then(function(tabs){
                        async.forEach(tabs, function(tab, cb){
                          tab.widgets.destroyAll(function(destroyed){
                            tab.remove();
                            cb();
                          });
                        }, function(){
                          async.forEach(settings.backupjson.tabs, function(tab, cb2){
                            tab.settingsId = settings.id;
                            Settings.app.models.tab.create(tab).then(function(newtab){
                              var widgets = _.map(tab.widgets, '__data')
                              newtab.widgets.create(widgets).then(function(newwidgets){
                                cb2();
                              });
                            });
                          }, function(){
                            settings.save();
                            res.status(204).send();
                          });
                        });
                      });
                    });
                  });
                };


                Settings.remoteMethod(
                    'revert', {
                        accepts: [{
                            arg: 'id',
                            type: 'string'
                        }, {
                            arg: 'req',
                            type: 'object',
                            http: {
                                source: 'req'
                            }
                        }, {
                            arg: 'res',
                            type: 'object',
                            http: {
                                source: 'res'
                            }
                        }],
                        http: {
                            path: '/:id/revert',
                            verb: 'get'
                        }
                    }
                );


};
