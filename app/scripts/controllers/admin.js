'use strict';

/**
 * @ngdoc function
 * @name profilerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the profilerApp
 */
angular.module('profilerApp')
  .controller('AdminCtrl', function ($scope, $rootScope, $uibModal, $uibModalStack, Account, $state, $stateParams, Settings , $mdSidenav, $location, Objectjson) {
   $scope.connectiontypes = ['eloqua', 'salesforce']
   $scope.urlquery = $location.search();
   $scope.userId = $scope.urlquery.userId;
   $scope.siteId = $scope.urlquery.siteId;

   $scope.geteloquafields = function(){
     Account.contactfields({id: $scope.accountId, page: 1,  query: "",objname: 'contact'},function(result){
       $scope.eloquacontactfileds = result.elements;
     });
   }
   $scope.getAccount = function(siteId){
     Account.findOne({where:{ siteId: siteId}}, function(account){
        $scope.account = account;
        $scope.accountId = account.id;
        $scope.getEloquaUsers();
        $scope.geteloquafields();
     });
   }




   $scope.getAccount($scope.siteId);

   $scope.alreadyAdded = false;
   $scope.loadingEloqua = true;

   $scope.saveAccount = function(account){
   Account.prototype$updateAttributes($scope.account.id, $scope.account, function(account){
       $scope.account = account;
        });
   }

   $scope.query = {
     query: "",
     page: 1,
     limit: 10,
     order: "email"
   }

   $scope.eloquaquery = {
     query: "",
     page: 1,
     limit: 10
   }

   $scope.getAcl = function(){

     var skip = ($scope.query.page - 1) * $scope.query.limit;
     var query = {"email": {"regexp": $scope.query.query }};
     Settings.find({ filter: {skip: skip, limit: $scope.query.limit, order: $scope.query.order, where: query }}, function(result){
       Settings.count({ skip: skip, limit: $scope.query.limit, order: $scope.query.order }, function(result){
         $scope.aclcount = result.count;
       });
       $scope.acldata = result;
       $scope.acldata.forEach( function (data)
       {
          data.email = data.email;
          data.title = data.email;
          if(data.email.length >= 24)
          {
            data.email = data.email.substr(0,22) + '...'
          }
       });
     });

   }

   $scope.getAcl();

   $scope.searchusers = function(query)
   {
     if(query.length > 3){
       $scope.getEloquaUsers();

     }

   }

   $scope.searchacl = function(query){
     if(query.length > 3 || query.length == 0){
       $scope.getAcl();
     }
   }

   $scope.adduser = function(user){
    $scope.flag = 0;
    $scope.alreadyAdded = false;
     $scope.acldata.forEach( function (data)
     {
      console.log(data)
       if(data.userId == user.id)
       {
          $scope.flag = 1;
          $scope.alreadyAdded = true;
       }
     });
    if($scope.flag == 0)
    {
     var newsettings = {
       email: user.emailAddress,
       userId: user.id,
       siteId: 1234,
       created: new Date(),
       accountId: $scope.accountId
     };
     Settings.create(newsettings, function(added){
       console.log(user.emailAddress + " added");
        $scope.getAcl();
       Settings.reset({id: added.id }, function(){
         $scope.getAcl();
         console.log("default view created");
       });
     });
    }
    else
    {
      console.log(user.emailAddress , " already added");
    }
   }

   $scope.removeuser = function(settings){
     console.log(settings);
     Settings.removeById({id: settings.id}, function(removed){
       console.log(settings.email + "removed");
       $scope.getAcl();
     });
   }


   $scope.getEloquaUsers = function(){
      $scope.loadingEloqua = true;
     Account.searchusers({id: $scope.accountId, query: $scope.eloquaquery.query, page: $scope.eloquaquery.page}, function(users){
       $scope.eloquausers = users.elements;
       $scope.eloquauserscount  = users.total;
       $scope.loadingEloqua = false;
     });
   }



   $scope.fields = {
     "salesforce": [
       {"name": "username", "type" : "text"},
       {"name": "password", "type": "password"},
       {"name": "secret", "type": "text"}
     ]
   };



});
