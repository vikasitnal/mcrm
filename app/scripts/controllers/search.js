'use strict';

/**
 * @ngdoc function
 * @name profilerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the profilerApp
 */
angular.module('profilerApp')
  .controller('SearchCtrl', function ($scope, $rootScope, $uibModal, $uibModalStack, $state, $stateParams,$location, Account) {
    var urlquery = $location.search();
    $scope.userId = urlquery.userId;
    $scope.siteId = urlquery.siteId;
    $scope.getAccount = function(siteId){
      Account.findOne({where:{ siteId: siteId}}, function(account){
         $scope.account = account;
         $scope.accountId = account.id;
         $scope.searchuser($scope.query, $scope.page);

      });
    }
    $scope.page = urlquery.page;
    $scope.total = 0;
    $scope.query = urlquery.query;
    $scope.maxSize = 10;
     $scope.searching = true;

    $scope.searchuser = function(query, page){
      Account.search({id: $scope.accountId, query: query, page: page}, function(result){
        $scope.searching = true;
        $scope.page = page;
        $scope.users = result.elements;
        $scope.total = result.total;
        console.log("$scope.users ",$scope.users);
        $scope.searching = false;
      });
    }
    $scope.profile = function(id){
      $state.go('home.tabs', {id: id, type: 'id', accountId: $scope.accountId, siteId: urlquery.siteId, userId: urlquery.userId})
    }
    $scope.getAccount($scope.siteId);

  });
