'use strict';

/**
 * @ngdoc function
 * @name profilerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the profilerApp
 */
angular.module('profilerApp')
  .controller('TabsCtrl', function ($scope, $rootScope, $uibModal, $uibModalStack, Account, $state, $stateParams, Settings, Tab, Widget, Objectjson, $location, $mdDialog, jsonPath) {

    $scope.sortBy = function(propertyName)
    {
    $scope.reverse = ($scope.sortType === propertyName) ? !$scope.reverse : false;
    $scope.sortType = propertyName;
  };
  $rootScope.salesforceobjectfields = {};
  $rootScope.secondarydata = {};
  $rootScope.salesforcefields = [];
  $rootScope.salesforcechilds = {};
  var month = new Array();
  month[0] = "Jan";
  month[1] = "Feb";
  month[2] = "Mar";
  month[3] = "Apr";
  month[4] = "May";
  month[5] = "Jun";
  month[6] = "Jul";
  month[7] = "Aug";
  month[8] = "Sep";
  month[9] = "Oct";
  month[10] = "Nov";
  month[11] = "Dec";

	$scope.urlquery = $location.search();
    $scope.urltype = $scope.urlquery.type;
    if($scope.urltype == 'id'){
      $scope.contactId = $stateParams.id;
    } else if($scope.urltype == 'email') {
      $scope.email = $stateParams.id;
    } else if ($scope.urltype == 'sfdcaccountId') {
      $scope.sfdcaccountId = $stateParams.id;
    }
    $scope.accountId = $stateParams.accountId;

	$scope.getSettings = function(accountId){
      Account.findById({"id": accountId}, function(account){
        Settings.findOne({ "filter": {"where": { "siteId": $scope.siteId, "userId": $scope.userId}}}, function(settings){
          $scope.account = account;
          if(settings && settings.id){
            $rootScope.settings = settings;
          }
        }, function(error){
          var newsettings = {
             "properties": [
               {}
             ],
             "accountId": $scope.accountId,
             "userId": $scope.userId,
             "siteId": $scope.siteId
           }
          Settings.create(newsettings, function(settings){
            $rootScope.settings = settings;
            $scope.resetdefault();
          });
        });
      });
    }

    $scope.getAccount = function(accountId){
      Account.findById({"id": accountId}, function(account){
         $scope.account = account;
         $scope.getSettings($scope.accountId);
      });
    }


    $scope.getAccount($scope.accountId);
    $scope.userId = $scope.urlquery.userId;
    $scope.siteId = $scope.urlquery.siteId;
    $rootScope.tabindex = 0;
    $rootScope.parentobjects = ['Contact', 'Lead'];
    $rootScope.parentobjectsReference = {
      'Contact': 'Email',
      'Lead': 'Email'
    };
    $rootScope.loadingSalesForce = true;
    $rootScope.loadingEloqua = true;
    $rootScope.loadingTabs = true;
    $scope.parentdataavailable = false;
    $scope.dataLoaded = false;

    $rootScope.eloquaParentobjects = ['Contact', 'Account' , 'Analytics' , 'Contact Activities' , 'Account Activities'];

    $rootScope.eloquaActivityTypes = ["emailOpen","emailSend","emailClickThrough","webVisit","formSubmit",
                "campaignMembership"
            ] ;

    $scope.filters = [
      {
        filterBy: 'By Type',
        type: [
          { name: 'Email Opens' , type : 'emailOpens'},
          { name: 'Email Sends' , type : 'emailSends'},
          { name: 'Email Click', type: 'emailClick'},
          { name: 'Form Submition', type:'formSubmited'},
          { name: 'Web Visit' , type:'webVisites'},
          { name: 'Campaign Membership',type:'campaignMemberships'},
        ]
      }
    ];


    $scope.customDate = false;
    $scope.nameSearch = false;
    $scope.emailOpens = true;
    $scope.emailSends = true;
    $scope.emailClick= true;
    $scope.formSubmited = true;
    $scope.campaignMemberships = true;
    $scope.webVisites = true;
    $scope.customDateForLatestActivitis = false;
    $scope.nameSearchForLatestActivities = false;
    $scope.customDateForActivitiesGraph = false;
    $scope.nameSearchForActivitiesGraph = false;
    var endDate = new Date();

	console.log('month ',month[endDate.getMonth()]);
    var startDate = new Date(endDate.getTime() - (365 * 24 * 60 * 60 * 1000));

    $scope.todate = Math.floor(endDate.getTime() / 1000);
    $scope.fromdate = Math.floor(startDate.getTime() / 1000);

	$scope.activityStartDate = $scope.fromdate;
	$scope.activityEndDate = $scope.todate;
	$rootScope.filterByCustomDate = function(startDate , endDate)
    {

      if(!angular.isUndefined(startDate) && !angular.isUndefined(endDate))
      {
		  $rootScope.showAccountCustomDate = false;
		  $scope.todate = endDate/1000;
		  $scope.fromdate =startDate/1000;
		  $rootScope.fromdate = $scope.fromdate;
		  $rootScope.todate = $scope.todate;
          if(startDate/1000 >= $scope.activityStartDate)
		  {
		   $scope.accountActivityGraph = [];
           $rootScope.eloquaActivityTypes.forEach(function(type,index)
			{
				 var monthdata = new Array();
				 monthdata[0] = { x: "January", y: 0 };
				 monthdata[1] = { x:"February", y: 0};
				 monthdata[2] = { x: "March", y: 0 };
				 monthdata[3] = { x:"April", y: 0 };
				 monthdata[4] = { x:"May", y: 0 };
				 monthdata[5] = { x:"June", y: 0 };
				 monthdata[6] = { x:"July", y: 0 };
				 monthdata[7] = { x:"August", y: 0 };
				 monthdata[8] = { x:"September", y: 0 };
				 monthdata[9] = { x:"October", y: 0 };
				 monthdata[10] = { x:"November", y: 0 };
				 monthdata[11] = { x:"December", y: 0 };
				 $scope.accountActivityGraph.push({key: type, values: monthdata });
			});
             console.log('in loacal controller');
			 if($scope.accountWidgetPresent)
			 {
	           $scope.filterActivityData();
			 }
			 if( $scope.contactWidgetPresent)
			 {
			    $scope.FilterContactActivity('Custom date');
			 }
		  }
		  else
		  {
			if($scope.globalAccountWidget != '')
			{
			   $scope.flagForaccountBasedActivity = true;
			   $scope.accountActivityGraph = [];
			   $rootScope.eloquaActivityTypes.forEach(function(type,index)
				{
					 var monthdata = new Array();
					 monthdata[0] = { x: "January", y: 0 };
					 monthdata[1] = { x:"February", y: 0};
					 monthdata[2] = { x: "March", y: 0 };
					 monthdata[3] = { x:"April", y: 0 };
					 monthdata[4] = { x:"May", y: 0 };
					 monthdata[5] = { x:"June", y: 0 };
					 monthdata[6] = { x:"July", y: 0 };
					 monthdata[7] = { x:"August", y: 0 };
					 monthdata[8] = { x:"September", y: 0 };
					 monthdata[9] = { x:"October", y: 0 };
					 monthdata[10] = { x:"November", y: 0 };
					 monthdata[11] = { x:"December", y: 0 };
					 $scope.accountActivityGraph.push({key: type, values: monthdata });
				});
			  $scope.getSecondaryData($scope.globalAccountWidget);
			  console.log('in loop');
			}
			if($scope.globalContactWidget != '')
			{
				console.log('in api call');
				$scope.flagForcontactBasedActivity = true;
				$scope.getSecondaryData($scope.globalContactWidget);
			}
		  }
      }
    }

	$scope.getMaxDate = function () {
        return new Date(
            endDate.getFullYear(),
            endDate.getMonth(),
            endDate.getDate());
    }

    $rootScope.maxDate = $scope.getMaxDate();

	$scope.eloquaAccountDataOnLoad = [];
	$rootScope.fromdate = $scope.fromdate;
	$rootScope.todate = $scope.todate;
	$rootScope.showDateFilter = false;

    $scope.filterByDate = function(type , widget)
    {
		console.log('in date function ');
        if(type == 'Custom Date' && widget.json.internalName == 'Activity Breakdown')
        {
          $scope.customDate = true;
          $scope.nameSearch = false;
        }
        else if(type == 'Custom Date' && widget.json.internalName == 'Latest Activities')
        {
           $scope.customDateForLatestActivitis = true;
           $scope.nameSearchForLatestActivities = false;
        }
        else if(type == 'Custom Date' && widget.json.internalName == 'Activities Graph')
        {
           $scope.customDateForActivitiesGraph = true;
           $scope.nameSearchForActivitiesGraph = false;
        }
        else if(type == '30 Days')
        {
             endDate = new Date();
             startDate = new Date(endDate.getTime() - (30 * 24 * 60 * 60 * 1000));
             $scope.todate = Math.floor(endDate.getTime() / 1000);
             $scope.fromdate = Math.floor(startDate.getTime() / 1000);

            filterByDateContectDate(widget);
        }
        else if(type == '6 Months')
        {
            endDate = new Date();
            startDate = new Date(endDate.getTime() - (182 * 24 * 60 * 60 * 1000));
            $scope.todate = Math.floor(endDate.getTime() / 1000);
            $scope.fromdate = Math.floor(startDate.getTime() / 1000);
            filterByDateContectDate(widget);
        }
        else if(type == '1 Year')
        {
            endDate = new Date();
            startDate = new Date(endDate.getTime() - (365 * 24 * 60 * 60 * 1000));
            $scope.todate = Math.floor(endDate.getTime() / 1000);
            $scope.fromdate = Math.floor(startDate.getTime() / 1000);
            filterByDateContectDate(widget);

        }
    }

	$scope.globalAccountWidget = '';
	$scope.globalContactWidget = '';
	$rootScope.showAccountCustomDate = false;
	$rootScope.filterByDate = function(type)
    {
	  $scope.flagForaccountBasedActivity = true;

      if(type == '30 Days')
        {
		   $scope.accountActivityGraph = [];

		      $scope.accountActivityGraph = [];
			   $rootScope.eloquaActivityTypes.forEach(function(type,index)
				{
					 var monthdata = new Array();
					 monthdata[0] = { x: "January", y: 0 };
					 monthdata[1] = { x:"February", y: 0};
					 monthdata[2] = { x: "March", y: 0 };
					 monthdata[3] = { x:"April", y: 0 };
					 monthdata[4] = { x:"May", y: 0 };
					 monthdata[5] = { x:"June", y: 0 };
					 monthdata[6] = { x:"July", y: 0 };
					 monthdata[7] = { x:"August", y: 0 };
					 monthdata[8] = { x:"September", y: 0 };
					 monthdata[9] = { x:"October", y: 0 };
					 monthdata[10] = { x:"November", y: 0 };
					 monthdata[11] = { x:"December", y: 0 };
					 $scope.accountActivityGraph.push({key: type, values: monthdata });
				});
             endDate = new Date();
			 startDate = new Date(endDate.getTime() - (30 * 24 * 60 * 60 * 1000));
			 $scope.todate = Math.floor(endDate.getTime() / 1000);
             $scope.fromdate = Math.floor(startDate.getTime() / 1000);
			 $rootScope.fromdate = $scope.fromdate;
	         $rootScope.todate = $scope.todate;
			 var getAllDays = [];
			 for(var i = 0 ;startDate<endDate ; i++)
			 {
				 getAllDays.push(startDate.getMonth()+1 +'/'+startDate.getDate());
				  console.log('month ' , startDate.getMonth()+1 , '/' ,startDate.getDate());
				  startDate.setDate(startDate.getDate()+1);
			 }
			 $scope.contactActivityGraph = []
		   $rootScope.eloquaActivityTypes.forEach(function(type,index)
			{
				 var monthdata = new Array();
				 for(var j = 0 ; j < getAllDays.length ; j++)
				 {
					monthdata[j] = { x: getAllDays[j], y: 0 };
				 }
				 $scope.contactActivityGraph.push({key: type, values: monthdata });
			});

			 if($scope.accountWidgetPresent)
			 {
	           $scope.filterActivityData();
			 }
			 if( $scope.contactWidgetPresent)
			 {
			    $scope.FilterContactActivity(type);
			 }

			 $rootScope.showAccountCustomDate = false;
        }
        else if(type == '6 Months')
        {
		   $scope.accountActivityGraph = [];
           $rootScope.eloquaActivityTypes.forEach(function(type,index)
			{
				 var monthdata = new Array();
				 monthdata[0] = { x: "January", y: 0 };
				 monthdata[1] = { x:"February", y: 0};
				 monthdata[2] = { x: "March", y: 0 };
				 monthdata[3] = { x:"April", y: 0 };
				 monthdata[4] = { x:"May", y: 0 };
				 monthdata[5] = { x:"June", y: 0 };
				 monthdata[6] = { x:"July", y: 0 };
				 monthdata[7] = { x:"August", y: 0 };
				 monthdata[8] = { x:"September", y: 0 };
				 monthdata[9] = { x:"October", y: 0 };
				 monthdata[10] = { x:"November", y: 0 };
				 monthdata[11] = { x:"December", y: 0 };
				 $scope.accountActivityGraph.push({key: type, values: monthdata });
			});
            endDate = new Date();
            startDate = new Date(endDate.getTime() - (182 * 24 * 60 * 60 * 1000));
            $scope.todate = Math.floor(endDate.getTime() / 1000);
            $scope.fromdate = Math.floor(startDate.getTime() / 1000);
			$rootScope.fromdate = $scope.fromdate;
	        $rootScope.todate = $scope.todate;
			 var getAllMonths = [];
			 for(var i = 0 ;startDate<endDate ; i++)
			 {
				 getAllMonths.push(month[startDate.getMonth()]);
				  console.log('month ' , month[startDate.getMonth()]);
				  startDate.setMonth(startDate.getMonth()+1);
			 }
		   $scope.contactActivityGraph = []
		   $rootScope.eloquaActivityTypes.forEach(function(type,index)
			{
				 var monthdata = new Array();
				 for(var j = 0 ; j < getAllMonths.length ; j++)
				 {
					monthdata[j] = { x: getAllMonths[j], y: 0 };
				 }
				 $scope.contactActivityGraph.push({key: type, values: monthdata });
			});


			if($scope.accountWidgetPresent)
			 {
	           $scope.filterActivityData();
			 }
			 if( $scope.contactWidgetPresent)
			 {
			    $scope.FilterContactActivity(type);
			 }
			$rootScope.showAccountCustomDate = false;
        }
        else if(type == '1 Year')
        {
			$scope.accountActivityGraph = [];
           $rootScope.eloquaActivityTypes.forEach(function(type,index)
			{
				 var monthdata = new Array();
				 monthdata[0] = { x: "January", y: 0 };
				 monthdata[1] = { x:"February", y: 0};
				 monthdata[2] = { x: "March", y: 0 };
				 monthdata[3] = { x:"April", y: 0 };
				 monthdata[4] = { x:"May", y: 0 };
				 monthdata[5] = { x:"June", y: 0 };
				 monthdata[6] = { x:"July", y: 0 };
				 monthdata[7] = { x:"August", y: 0 };
				 monthdata[8] = { x:"September", y: 0 };
				 monthdata[9] = { x:"October", y: 0 };
				 monthdata[10] = { x:"November", y: 0 };
				 monthdata[11] = { x:"December", y: 0 };
				 $scope.accountActivityGraph.push({key: type, values: monthdata });
			});
            endDate = new Date();
            startDate = new Date(endDate.getTime() - (365 * 24 * 60 * 60 * 1000));
            $scope.todate = Math.floor(endDate.getTime() / 1000);
            $scope.fromdate = Math.floor(startDate.getTime() / 1000);
			$rootScope.fromdate = $scope.fromdate;
			$rootScope.todate = $scope.todate;
			if($scope.accountWidgetPresent)
			 {
	           $scope.filterActivityData();
			 }
			 if($scope.contactWidgetPresent)
			 {
			    $scope.FilterContactActivity(type);
			 }
			$rootScope.showAccountCustomDate = false;
        }
		else if(type == 'Custom Date')
		{
			$rootScope.showAccountCustomDate = true;
		}

    }




    $scope.showNameSearch = function(widget)
    {
      if(widget.json.internalName == 'Activity Breakdown')
      {
        $scope.customDate = false;
        $scope.nameSearch = true;
      }
      else if(widget.json.internalName == 'Latest Activities')
      {
        $scope.customDateForLatestActivitis = false;
        $scope.nameSearchForLatestActivities = true;
      }
      else if(widget.json.internalName == 'Activities Graph')
      {
        $scope.customDateForLatestActivitis = false;
        $scope.nameSearchForLatestActivities = true;
      }
    }

    $scope.filterByName = function(widget ,action , text)
    {
      if(text == '')
      {
         text = undefined;
      }
      if(!angular.isUndefined(text))
      {
        activityFilterByName(widget , action , text);
      }
    }

    function activityFilterByName(widget , action , text)
    {
       if(widget.json.internalName == 'Activity Breakdown')
          {
             widget.name = widget.json.internalName;
             $scope.widgetdata[widget.id] = {};
             $rootScope.eloquaActivityTypes.forEach(function(type){
             var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
             widget.json.params.forEach(function(param){
              if(param.name == 'type'){
                 query[param.name] = type;
               } else {
                  if(param.name == 'fromdate')
                  {
                      query[param.name] = $scope.fromdate;
                  }
                  else if(param.name == 'todate')
                  {
                      query[param.name] = $scope.todate;
                  }
                }
              });
             Account.secondarydata(query, function(result)
             {
                $scope.filterArray = [];
                result.result.forEach(function(activity)
                {
                  activity.details.forEach(function(details){
                  if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                   if(!angular.isUndefined(text) && !action)
                   {
                    if(activity.name.toLowerCase().match(text) || activity.name.match(text))
                    {
                      $scope.filterArray.push(activity);
                    }
                    else
                    {
                      return;
                    }
                  }
                  else
                  {
                    if(!(activity.name.toLowerCase().match(text) || activity.name.match(text)))
                    {
                      $scope.filterArray.push(activity);
                    }
                    else
                    {
                      return;
                    }
                  }

                  }
                 });
                });
                $scope.widgetdata[widget.id][type] = $scope.filterArray;
                $scope.widgetdata[widget.id][type].forEach(function(activity)
                {
                  activity.details.forEach(function(details){
                  if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                  }
                  if(details[Object.keys(details)[0]] == "Collection")
                  {
                      activity.formSubmitionData = details[Object.keys(details)[1]];
                      activity.formSubmitionData = activity.formSubmitionData.split("elqdelimiter");
                  }
                 });
                });
             });
          });
        }
        else if(widget.json.internalName == 'Latest Activities'){
           widget.name = widget.json.internalName;
           $scope.widgetdata[widget.id] = [];
           $rootScope.eloquaActivityTypes.forEach(function(type){
             var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
             widget.json.params.forEach(function(param){
               if(param.name == 'type'){
                 query[param.name] = type;
               } else {
                 if(param.name == 'fromdate')
                  {
                      query[param.name] = $scope.fromdate;
                  }
                  else if(param.name == 'todate')
                  {
                      query[param.name] = $scope.todate;
                  }
               }

             });
             Account.secondarydata(query, function(result){

                $scope.filterArray = [];
                result.result.forEach(function(activity)
                {
                  activity.details.forEach(function(details){
                  if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                   if(!angular.isUndefined(text) && !action)
                   {
                    if(activity.name.toLowerCase().match(text) || activity.name.match(text))
                    {
                      $scope.filterArray.push(activity);
                    }
                    else
                    {
                      return;
                    }
                  }
                  else
                  {
                    if(!(activity.name.toLowerCase().match(text) || activity.name.match(text)))
                    {
                      $scope.filterArray.push(activity);
                    }
                    else
                    {
                      return;
                    }
                  }
                  }
                 });
                });

               $scope.widgetdata[widget.id] = $scope.widgetdata[widget.id].concat($scope.filterArray);
               $scope.widgetdata[widget.id].forEach(function(activity)
               {
                activity.activityType = activity.activityType;
                activity.activityDate = activity.activityDate;
                activity.show = true;
                if(activity.activityType == 'emailOpen')
                {
                     activity.image = "/img/emailOpen.PNG";
                }
                else if(activity.activityType == 'emailSend')
                {
                      activity.image = "/img/emailSends.PNG";
                }
                else if(activity.activityType == 'emailClickThrough')
                {
                        activity.image =  "/img/emailClicked.PNG";
                }
                else if(activity.activityType == 'webVisit')
                {
                         activity.image =   "/img/webVisited.PNG";
                }
                else if(activity.activityType == 'formSubmit')
                {
                        activity.image = "/img/formSubmition.PNG";
                }
                else if(activity.activityType == 'campaignMembership')
                {
                        activity.image =  "/img/externalActivities.PNG";
                }
                activity.details.forEach(function(details){
                  if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.subjectLine = details[Object.keys(details)[1]];
                  }
                  else if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                  }

                });

               });

             });
           });
         }   else if(widget.json.internalName == 'Activities Graph'){
              widget.name = widget.json.internalName;
              $scope.widgetdata[widget.id] = [];
              $rootScope.eloquaActivityTypes.forEach(function(type, index){
                var monthdata = new Array();
                monthdata[0] = { x: "January", y: 0 };
                monthdata[1] = { x:"February", y: 0};
                monthdata[2] = { x: "March", y: 0 };
                monthdata[3] = { x:"April", y: 0 };
                monthdata[4] = { x:"May", y: 0 };
                monthdata[5] = { x:"June", y: 0 };
                monthdata[6] = { x:"July", y: 0 };
                monthdata[7] = { x:"August", y: 0 };
                monthdata[8] = { x:"September", y: 0 };
                monthdata[9] = { x:"October", y: 0 };
                monthdata[10] = { x:"November", y: 0 };
                monthdata[11] = { x:"December", y: 0 };
                $scope.widgetdata[widget.id].push({key: type, values: monthdata });
                var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
                widget.json.params.forEach(function(param){
                  if(param.name == 'type'){
                    query[param.name] = type;
                  } else {
                    if(param.name == 'fromdate')
                     {
                         query[param.name] = $scope.fromdate;
                     }
                     else if(param.name == 'todate')
                     {
                         query[param.name] = $scope.todate;
                     }
                  }

                });
                Account.secondarydata(query, function(result){

                   $scope.filterArray = [];
                   result.result.forEach(function(activity)
                   {
                     activity.details.forEach(function(details){
                     if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
                       || details[Object.keys(details)[0]] == 'URL')
                     {
                       activity.name = details[Object.keys(details)[1]];
                      if(!angular.isUndefined(contains) && angular.isUndefined(notContains))
                      {
                       if(activity.name.toLowerCase().match(contains) || activity.name.match(contains))
                       {
                         var key;
                         if(widget.json.interval == 'month'){
                           key = new Date(activity.activityDate*1000).getMonth();
                         } else if (widget.json.interval == 'year') {
                           key = new Date(activity.activityDate*1000).getYear();
                         } else if (widget.json.interval == 'day') {
                           key = new Date(activity.activityDate*1000).getDay();
                         }
                         $scope.widgetdata[widget.id][index].values[parseInt(key)].y++;

                       }
                       else
                       {
                         return;
                       }
                     }
                     else if(!angular.isUndefined(contains) && !angular.isUndefined(notContains))
                     {
                       if((activity.name.toLowerCase().match(contains) || activity.name.match(contains))
                         && !(activity.name.toLowerCase().match(notContains) || activity.name.match(notContains)))
                       {
                        var key;
                         if(widget.json.interval == 'month'){
                           key = new Date(activity.activityDate*1000).getMonth();
                         } else if (widget.json.interval == 'year') {
                           key = new Date(activity.activityDate*1000).getYear();
                         } else if (widget.json.interval == 'day') {
                           key = new Date(activity.activityDate*1000).getDay();
                         }
                         $scope.widgetdata[widget.id][index].values[parseInt(key)].y++;

                       }
                       else
                       {
                         return;
                       }
                     }
                     else
                     {
                       if(!(activity.name.toLowerCase().match(notContains) || activity.name.match(notContains)))
                       {
                         var key;
                         if(widget.json.interval == 'month'){
                           key = new Date(activity.activityDate*1000).getMonth();
                         } else if (widget.json.interval == 'year') {
                           key = new Date(activity.activityDate*1000).getYear();
                         } else if (widget.json.interval == 'day') {
                           key = new Date(activity.activityDate*1000).getDay();
                         }
                         $scope.widgetdata[widget.id][index].values[parseInt(key)].y++;

                       }
                       else
                       {
                         return;
                       }
                     }
                     }
                    });
                   });



                });
              });
            }
    }


    $scope.filterByCustomDate = function(widget , startDate , endDate)
    {

      if(!angular.isUndefined(startDate) && !angular.isUndefined(endDate))
      {
        filterByDateContectDate(widget, startDate/1000 , endDate/1000);
      }
    }



    function filterByDateContectDate(widget)
    {

      if (widget.json.internalName == 'Activity Breakdown')
      {
           widget.name = widget.json.internalName;
           $scope.widgetdata[widget.id] = {};
           $rootScope.eloquaActivityTypes.forEach(function(type){
             console.log(type);
             var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
             widget.json.params.forEach(function(param){
               if(param.name == 'type'){
                 query[param.name] = type;
               } else {
                  if(param.name == 'fromdate')
                  {
                      query[param.name] = $scope.fromdate;
                  }
                  else if(param.name == 'todate')
                  {
                      query[param.name] = $scope.todate;
                  }
                }

             });
             Account.secondarydata(query, function(result){
               $scope.widgetdata[widget.id][type] = result.result;
                $scope.widgetdata[widget.id][type].forEach(function(activity)
                {
                  activity.details.forEach(function(details){
                  if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];

                  }
                  if(details[Object.keys(details)[0]] == "Collection")
                  {
                      activity.formSubmitionData = details[Object.keys(details)[1]];
                      activity.formSubmitionData = activity.formSubmitionData.split("elqdelimiter");

                   }
                  });
                });
             });
          });
        }
        else if(widget.json.internalName == 'Latest Activities'){
           widget.name = widget.json.internalName;
           $scope.widgetdata[widget.id] = [];
           $rootScope.eloquaActivityTypes.forEach(function(type){
             var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
             widget.json.params.forEach(function(param){
               if(param.name == 'type'){
                 query[param.name] = type;
               } else {
                 if(param.name == 'fromdate')
                  {
                      query[param.name] = $scope.fromdate;
                  }
                  else if(param.name == 'todate')
                  {
                      query[param.name] = $scope.todate;
                  }
               }

             });
             Account.secondarydata(query, function(result){
               $scope.widgetdata[widget.id] = $scope.widgetdata[widget.id].concat(result.result);
               $scope.widgetdata[widget.id].forEach(function(activity)
               {
                activity.activityType = activity.activityType;
                activity.activityDate = activity.activityDate;
                activity.show = true;
                if(activity.activityType == 'emailOpen')
                {
                     activity.image = "/img/emailOpen.PNG";
                }
                else if(activity.activityType == 'emailSend')
                {
                      activity.image = "/img/emailSends.PNG";
                }
                else if(activity.activityType == 'emailClickThrough')
                {
                        activity.image =  "/img/emailClicked.PNG";
                }
                else if(activity.activityType == 'webVisit')
                {
                         activity.image =   "/img/webVisited.PNG";
                }
                else if(activity.activityType == 'formSubmit')
                {
                        activity.image = "/img/formSubmition.PNG";
                }
                else if(activity.activityType == 'campaignMembership')
                {
                        activity.image =  "/img/externalActivities.PNG";
                }
                activity.details.forEach(function(details){
                  if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.subjectLine = details[Object.keys(details)[1]];
                  }
                  else if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                  }

                });

               });

             });
           });
         }else if(widget.json.internalName == 'Activities Graph'){
              widget.name = widget.json.internalName;
              $scope.widgetdata[widget.id] = [];
              $rootScope.eloquaActivityTypes.forEach(function(type, index){
                var monthdata = new Array();
                monthdata[0] = { x: "January", y: 0 };
                monthdata[1] = { x:"February", y: 0};
                monthdata[2] = { x: "March", y: 0 };
                monthdata[3] = { x:"April", y: 0 };
                monthdata[4] = { x:"May", y: 0 };
                monthdata[5] = { x:"June", y: 0 };
                monthdata[6] = { x:"July", y: 0 };
                monthdata[7] = { x:"August", y: 0 };
                monthdata[8] = { x:"September", y: 0 };
                monthdata[9] = { x:"October", y: 0 };
                monthdata[10] = { x:"November", y: 0 };
                monthdata[11] = { x:"December", y: 0 };
                $scope.widgetdata[widget.id].push({key: type, values: monthdata });
                var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
                widget.json.params.forEach(function(param){
                  if(param.name == 'type'){
                    query[param.name] = type;
                  } else {
                    if(param.name == 'fromdate')
                     {
                         query[param.name] = $scope.fromdate;
                     }
                     else if(param.name == 'todate')
                     {
                         query[param.name] = $scope.todate;
                     }
                  }

                });
                Account.secondarydata(query, function(result){
                   result.result.forEach(function(activity)
                   {

                     var key;
                     if(widget.json.interval == 'month'){
                       key = new Date(activity.activityDate*1000).getMonth();
                     } else if (widget.json.interval == 'year') {
                       key = new Date(activity.activityDate*1000).getYear();
                     } else if (widget.json.interval == 'day') {
                       key = new Date(activity.activityDate*1000).getDay();
                     }
                     $scope.widgetdata[widget.id][index].values[parseInt(key)].y++;

                   });




                });
              });
            }
    }

    $scope.latestEmailOpen = true;
    $scope.latestEmailSend = true;
    $scope.latestEmailClick = true;
    $scope.latestFormSubmit = true;
    $scope.latestWebVisit = true;
    $scope.latestCampainMember = true;
    $scope.checkedType = function(widget , type)
    {
      if (widget.json.internalName == 'Activity Breakdown')
      {
        if(type == 'emailOpens')
        {
          $scope.emailOpens = !$scope.emailOpens;
        }
        else if(type == 'emailSends')
        {
          $scope.emailSends = !$scope.emailSends;
        }
        else if(type == 'emailClick')
        {
          $scope.emailClick = !$scope.emailClick;
        }
        else if(type == 'formSubmited')
        {
          $scope.formSubmited = !$scope.formSubmited;
        }
        else if(type == 'campaignMemberships')
        {
          $scope.campaignMemberships = !$scope.campaignMemberships;
        }
        else if(type=='webVisites')
        {
           $scope.webVisites = ! $scope.webVisites;
        }
      }
      else if(widget.json.internalName == 'Latest Activities')
      {
         var activitytypes = '';
          widget.json.params.forEach(function(param){
               if(param.name == 'type'){
                activitytypes = type;
               }
             });

           if(activitytypes == 'emailOpens')
                {
                     $scope.latestEmailOpen = !$scope.latestEmailOpen;
                }
                else if(activitytypes == 'emailSends')
                {
                      $scope.latestEmailSend = !$scope.latestEmailSend;
                }
                else if(activitytypes == 'emailClick')
                {
                        $scope.latestEmailClick = !$scope.latestEmailClick;
                }
                else if(activitytypes == 'webVisites')
                {
                         $scope.latestWebVisit = !$scope.latestWebVisit;
                }
                else if(activitytypes == 'formSubmited')
                {
                        $scope.latestFormSubmit = !$scope.latestFormSubmit;
                }
                else if(activitytypes == 'campaignMemberships')
                {
                        $scope.latestCampainMember = !$scope.latestCampainMember;
                }
          }
    }

     $rootScope.eloquaControlsAccountBased =  [
                  {
                    internalName: 'Activities Graph',
                    objectname: 'accountBasedActivityGraph',
                    image: 'images/activity_graph.svg',
                    type: 'eloqua',
                    urlkey: 'Latest_Acitivities',
                    params: [
                      { name: "fromdate", value: $scope.fromdate},
                      { name : "todate", value: $scope.todate},
                      { name : "type", value: 'emailOpen'}
                    ]
                  },
                  { internalName: 'Activity Breakdown',
                     objectname: 'accountBasedActivityBreakdown',
                     image: 'images/Activity.svg',
                     type: 'eloqua',
                     urlkey: 'Latest_Acitivities',
                     params: [
                       { name: "fromdate", value: $scope.fromdate},
                       { name : "todate", value: $scope.todate},
                       { name : "type", value: 'emailOpen'}
                     ] },
                     { internalName: 'Latest Activities',
                        objectname: 'accountBasedLatestActivity',
                        image : 'images/latest_activity.svg',
                        type: 'eloqua',
                        urlkey: 'Latest_Acitivities',
                        params:
                        [
                         { name: "fromdate", value: $scope.fromdate},
                         { name : "todate", value: $scope.todate},
                         { name : "type", value: 'emailOpen'}
                         ]
                       }
                      ];


     $rootScope.eloquaControls =  [
               {
                  internalName: 'Activities Graph',
                  objectname: '',
                  image: 'images/activity_graph.svg',
                  type: 'eloqua',
                  urlkey: 'Latest_Acitivities',
                  params: [
                    { name: "fromdate", value: $scope.fromdate},
                    { name : "todate", value: $scope.todate},
                    { name : "type", value: 'emailOpen'}
                  ],
				options: {
					legend: {
					  display: true,
					  position: 'bottom',
					  labels: {
						fontColor: "#000080",
					  }
					}
				   }
				  },
                  { internalName: 'Activity Breakdown',
                     objectname: '',
                     image: 'images/Activity.svg',
                     type: 'eloqua',
                     urlkey: 'Latest_Acitivities',
                     params: [
                       { name: "fromdate", value: $scope.fromdate},
                       { name : "todate", value: $scope.todate},
                       { name : "type", value: 'emailOpen'}
                     ] },
                     { internalName: 'Latest Activities',
                        objectname: '',
                        image : 'images/latest_activity.svg',
                        type: 'eloqua',
                        urlkey: 'Latest_Acitivities',
                        params:
                        [
                         { name: "fromdate", value: $scope.fromdate},
                         { name : "todate", value: $scope.todate},
                         { name : "type", value: 'emailOpen'}
                         ]},
                        // { internalName: 'Activity Drill Down',
                        // type: 'eloqua',
                           // objectname: '',
                           // image : 'images/tab.svg',
                           // params: []
                         // }
                        ];

						 $rootScope.eloquaAnalystics =  [
						   { internalName: 'Opportunities',
							  objectname: '',
							  params: [] },
							{ internalName: 'Events',
							   objectname: '',
							   params: [] } ,
						   { internalName: 'Leads',
							  objectname: '',
							  params: [] } ];
	       $scope.accountActivityGraph = [];
		  $rootScope.eloquaActivityTypes.forEach(function(type,index)
			{
				 var monthdata = new Array();
				 monthdata[0] = { x: "January", y: 0 };
				 monthdata[1] = { x:"February", y: 0};
				 monthdata[2] = { x: "March", y: 0 };
				 monthdata[3] = { x:"April", y: 0 };
				 monthdata[4] = { x:"May", y: 0 };
				 monthdata[5] = { x:"June", y: 0 };
				 monthdata[6] = { x:"July", y: 0 };
				 monthdata[7] = { x:"August", y: 0 };
				 monthdata[8] = { x:"September", y: 0 };
				 monthdata[9] = { x:"October", y: 0 };
				 monthdata[10] = { x:"November", y: 0 };
				 monthdata[11] = { x:"December", y: 0 };
                 $scope.accountActivityGraph.push({key: type, values: monthdata });
			});


    $rootScope.parentOpen = {};
    $rootScope.parentsecondaryopen = {};
    $rootScope.parentanalyticsopen = {};
    $scope.sortableOptions = {

     };

$scope.selected = [];

  $scope.options = {
   rowSelection: false,
    multiSelect: true,
    autoSelect: true,
    decapitate: false,
    largeEditDialog: false,
    boundaryLinks: true,
    limitSelect: true,
    pageSelect: true
  };

  $scope.query = {
    order: 'Name',
    limit: 5,
    page: 1
  };

     $scope.getListInfo = function(){
     var skip = ($scope.query.page - 1) * $scope.query.limit;
     Settings.find({where: {email: {regexp: /$scope.query.query/}}, skip: skip, limit: $scope.query.limit, order: $scope.query.order }, function(result){
       $scope.acldata = result;
     });
   }

     $scope.getuser = function(contactId){
       Account.findcontact({
            id: $scope.accountId,
            contactId: contactId,
            type: $scope.urltype,
            email: $scope.urlquery

        }, function(user){
         $scope.eloquauser = user.eloqua;
         $scope.contactId = user.eloqua.id;
         if(user.salesforce){
           $scope.salesforceuser = user.salesforce;
           $rootScope.salesforceuseravailable = true;
            $rootScope.parentobjects.forEach(function(objectname, index){
              $rootScope.getsalesforcefields(objectname, index);
            });
           $rootScope.$watchCollection('secondarydata', function(secondarydata){
             var secondarylength = Object.keys($rootScope.secondarydata);
             if(secondarylength.length >= $scope.parentobjects.length){
               $scope.getTabs($rootScope.settings.id);
             }
           });
         } else {
           $rootScope.loadingSalesForce  = false;
           $scope.getTabs($rootScope.settings.id);
         }
       })
     }


     $rootScope.eloquaquery = "";
     $rootScope.eloquafieldspage = 1;
     $rootScope.eloquafields = {
     };
     $scope.geteloquafields = function(query, page, objname){
       Account.contactfields({id: $scope.accountId, query: query, page: page, objname: objname},function(result){
         $rootScope.eloquafields[objname] = result.elements;
         $rootScope.eloquafields[objname].forEach(function(field){
             field.name = field.name;
             field.label = field.name;
             field.internalName = field.internalName;
             field.fullName = field.name + ' ('+ field.internalName + ' )';
             if(field.fullName.length > 26)
             {
              field.fullName = field.fullName.substr(0,22) +'...';
             }

         });
       });
       $rootScope.loadingEloqua = false;
     }

     $rootScope.eloquaParentobjects.forEach(function(objname){
      if(objname == 'Analytics' || objname == 'Contact Activities' || objname == 'Account Activities')
      {
         $rootScope.loadingEloqua = false;
      }
      else
      {
         $scope.geteloquafields("", 1, objname);
      }
      });





     $rootScope.getsalesforcefields = function(objectname, index){
       if(objectname){
         Objectjson.findOne({ "filter": {"where": {"type": "salesforce", "name": objectname }}}, function(object){
           $rootScope.salesforceobjectfields[object.name] = object.json.fields;
           $scope.getSalesforceParentData(objectname, index);
           if(object.json.childRelationships){
             $rootScope.salesforcechilds[object.name] = object.json.childRelationships;
             $rootScope.salesforcechilds[object.name].forEach(function(field)
             {
                field.name = field.childSObject
                field.fullName = field.childSObject.replace(/([a-z](?=[A-Z]))/g, '$1 ')  + '( ' +field.childSObject+' )';
                field.title = field.name ;
                if(field.fullName.length > 28)
                {
                  field.fullName = field.fullName.substr(0,25) + '...';
                }
             });
           }
           $rootScope.salesforceobjectfields[object.name].forEach(function(field)
           {
             field.objectname = object.name;
             field.id = field.name;
             field.label = field.label;
             field.name = field.name;
             field.parentName = field.label + " ( " + field.name + " )";
             field.title = field.parentName;
             field.chieldName = field.parentName;
             if(field.parentName.length > 30)
             {
               field.parentName = field.parentName.substr(0 ,27) + "..."
             }

             if(field.chieldName.length > 24)
             {
               field.chieldName = field.chieldName.substr(0 , 22) + "..."
             }

           });
         });
         $rootScope.loadingSalesForce  = false;
       }
     }





    $scope.resetdefault = function(){
       Settings.reset({id: $rootScope.settings.id }, function(){
         $scope.getTabs($rootScope.settings.id);
       });
     }



      $scope.revert = function(){
        Settings.revert({id: $rootScope.settings.id }, function(){
          $scope.getTabs($rootScope.settings.id);
        });
      }

       $scope.deleteWidgets = false;
       $scope.deleteTabs = false;
       $scope.widgetIndexForDelete = '';
       $scope.getIndexForDelete = function(index)
       {
         $scope.deleteWidgets = true;
         $scope.deleteTabs = false;
         $scope.widgetIndexForDelete = index;
       }

       $scope.deleteWidget = function()
       {
         $scope.removecolumn($scope.widgetIndexForDelete);
       }

       $scope.tab = '';
       $scope.getIndexForDeleteTab = function(tab , index)
       {
         $scope.deleteWidgets = false;
         $scope.deleteTabs = true;
         $scope.tabIndexForDelete = index;
         $scope.tab = tab;
       }

       $scope.deleteTab = function()
       {
         $scope.removeTab($scope.tab ,$scope.tabIndexForDelete);
       }

       $scope.removeTab = function(tab , index)
        {

          tab.widgets.forEach( function (widget ,index)
          {
             $scope.removecolumn(index);
          });

            Tab.removeById({id: tab.id} ,function (result) {
              $rootScope.alltabs.splice(index , 1);
            });

        }

   $scope.fieldselectsettings = {
       idProperty: "name",
       displayProp: "name"
     }

     $scope.tabchanged = function(index){

       $rootScope.tabindex = index;
     }

     $scope.onKeydown = function(event){
         event.stopPropagation();
      };


     $scope.multiselectevents = {
       onSelectionChanged : function(){
         $rootScope.alltabs[$rootScope.tabindex].widgets.forEach(function(widget){
           $rootScope.saveWidget(widget);
           $scope.getSecondaryData(widget);
         });
       }
     }



     $scope.getTabs = function(settingsId){
       Tab.find({filter: { "where": {"settingsId": settingsId}, "include": "widgets"}}, function(alltabs){
         $rootScope.alltabs = alltabs;
         $rootScope.alltabs.forEach(function(tab){
           tab.widgets.forEach(function(widget){
             if(widget.type == 'group box 1' || widget.type == 'group box 2' || widget.type == 'group box 3'){
               widget.json.properties.forEach(function(item){
                 item.forEach(function(property){
                     $scope.getfielddata(property.property);
                 });
               });
             } else {
               $scope.getSecondaryData(widget);
             }
           });
         });
       });
        $rootScope.loadingTabs = false;
     }

     $scope.getfielddata = function(property){
       if( property.source == 'salesforce' && $rootScope.secondarydata[property.objname] && $rootScope.secondarydata[property.objname].Id){
         var query = {id: $scope.accountId,  fields: property.fieldname, source: 'salesforce', object: property.objname, key: 'Id', value: $rootScope.secondarydata[property.objname].Id };
         query.salesforcecontactId = $scope.salesforceuser.Id;
         Account.secondaryproperty(query , function(result){
           if(result && result.records)
           {
             property.value = jsonPath(result.records[0], '$.' + property.fieldname);
           }
         });
       }
     }

     $scope.widgetdata = {};


     $scope.getSalesforceParentData = function(object, index){
          var query = {id: $scope.accountId, source: 'salesforce', object: object, key: 'Email', value: $scope.salesforceuser.Email };
           var queryfields = _.map($rootScope.salesforceobjectfields[object], 'name');
           query.fields = queryfields.join(',');
           query.salesforcecontactId = $scope.salesforceuser.Id;
           Account.secondaryproperty(query , function(result){
             if(result && result.records)
             {
               $rootScope.secondarydata[object] = result.records[0];
             }
           });
        }


     $scope.FilterContactActivity = function(type)
	 {
		     $scope.EmailOpenContact = [];
		     $scope.EmailSendContact = [];
		     $scope.EmailClickContact = [];
		     $scope.FormSubmitContact = [];
		     $scope.WebVisitContact = [];
		     $scope.CampainMemberContact = [];
		     $scope.latestActivitesContact = [];
		     $scope.eloquaActivityTypes = [];
			 if(type != '30 Days'&& type != '6 Months')
			 {
			 $scope.contactActivityGraph = [];
             $rootScope.eloquaActivityTypes.forEach(function(type , index)
		     {
			     var monthdata = new Array();
				 monthdata[0] = { x: "Jan", y: 0 };
				 monthdata[1] = { x:"Feb", y: 0};
				 monthdata[2] = { x: "Mar", y: 0 };
				 monthdata[3] = { x:"Apr", y: 0 };
				 monthdata[4] = { x:"May", y: 0 };
				 monthdata[5] = { x:"Jun", y: 0 };
				 monthdata[6] = { x:"Jul", y: 0 };
				 monthdata[7] = { x:"Aug", y: 0 };
				 monthdata[8] = { x:"Sept", y: 0 };
				 monthdata[9] = { x:"Oct", y: 0 };
				 monthdata[10] = { x:"Nov", y: 0 };
				 monthdata[11] = { x:"Dec", y: 0 };
				 $scope.contactActivityGraph.push({key: type, values: monthdata });
		      });
			 }
		   $scope.eloquaContactDataOnLoad.forEach(function(activity)
			{

			if(activity.activityDate <= $scope.todate && activity.activityDate >= $scope.fromdate)
			{
		     if(type == '30 Days')
			 {
				var key = new Date(activity.activityDate*1000).getMonth()+1+ '/'+ new Date(activity.activityDate*1000).getDate();
				//console.log('key before ' , $scope.contactActivityGraph);
				$scope.contactActivityGraph.forEach(function(value)
				{
					value[Object.keys(value)[1]].forEach(function(newValue)
					{
							console.log(value , ' ' , activity.activityType)
							if(newValue[Object.keys(newValue)[0]] == key && value[Object.keys(value)[0]] == activity.activityType)
							{
								newValue[Object.keys(newValue)[1]]++;
							}
					});


				});
			 }
			 else if(type == '6 Months')
			 {
				var key = month[new Date(activity.activityDate*1000).getMonth()];
				console.log('key before ' , $scope.contactActivityGraph);
				$scope.contactActivityGraph.forEach(function(value)
				{
					value[Object.keys(value)[1]].forEach(function(newValue)
					{
							console.log(value , ' ' , activity.activityType)
							if(newValue[Object.keys(newValue)[0]] == key && value[Object.keys(value)[0]] == activity.activityType)
							{
								newValue[Object.keys(newValue)[1]]++;
							}
					});


				});
			 }
			 else
			 {
				 var key = new Date(activity.activityDate*1000).getMonth();
				 $scope.contactActivityGraph[activity.index].values[parseInt(key)].y++;
			 }

			 activity.activityType = activity.activityType;
			 activity.activityDate = activity.activityDate;
			if(activity.activityType == 'emailOpen')
			{
				 activity.image = "/img/emailOpen.PNG";
			}
			else if(activity.activityType == 'emailSend')
			{
				  activity.image = "/img/emailSends.PNG";
			}
			else if(activity.activityType == 'emailClickThrough')
			{
					activity.image =  "/img/emailClicked.PNG";
			}
			else if(activity.activityType == 'webVisit')
			{
					 activity.image =   "/img/webVisited.PNG";
			}
			else if(activity.activityType == 'formSubmit')
			{
					activity.image = "/img/formSubmition.PNG";
			}
			else if(activity.activityType == 'campaignMembership')
			{
					activity.image =  "/img/externalActivities.PNG";
			}
			activity.details.forEach(function(details)
			{
			  if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
				|| details[Object.keys(details)[0]] == 'URL')
			  {
				activity.subjectLine = details[Object.keys(details)[1]];
			  }
			  else if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
				|| details[Object.keys(details)[0]] == 'URL')
			  {
				activity.name = details[Object.keys(details)[1]];
			  }

			  activity.emailAddress = activity.emailAddress;
			 if(details[Object.keys(details)[0]] == 'EmailClickedThruLink')
			  {
				 activity.link = details[Object.keys(details)[1]];
			  }
			  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailOpen')
			  {
				activity.name = details[Object.keys(details)[1]];


				  $scope.EmailOpenContact.push(activity);
			  }
			  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailSend')
			  {
				activity.name = details[Object.keys(details)[1]];
				$scope.EmailSendContact.push(activity);
			  }
			  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailClickThrough')
			  {

				activity.name = details[Object.keys(details)[1]];
				$scope.EmailClickContact.push(activity);
			  }
			  if(details[Object.keys(details)[0]] == 'URL' )
			  {
				activity.name = details[Object.keys(details)[1]];
				$scope.WebVisitContact.push(activity);
			  }
			  if(details[Object.keys(details)[0]] == 'FormName' )
			  {
				activity.name = details[Object.keys(details)[1]];
				$scope.FormSubmitContact.push(activity);
			  }
			  if(details[Object.keys(details)[0]] == "Collection")
			  {
				  activity.formSubmitionData = details[Object.keys(details)[1]];
				  activity.formSubmitionData = activity.formSubmitionData.split("elqdelimiter");
			  }

			});
			$scope.latestActivitesContact.push(activity);
			 console.log('loading....');
			}
		   });
	}


     $scope.filterActivityData = function()
	 {
		    $scope.latestActivites = [];
			$scope.emailOpend = [];
			$scope.emailSended = [];
			$scope.emailClicked = [];
			$scope.webVisited = [];
			$scope.formSubmits = [];
			$scope.campaignMember = [];
			var index = '';
			var contact = '';
			$scope.eloquaAccountDataOnLoad.forEach(function(activity)
			{
			    if(activity.activityDate <= $scope.todate && activity.activityDate >= $scope.fromdate)
				{
                 var key = new Date(activity.activityDate*1000).getMonth();
		         $scope.accountActivityGraph[activity.index].values[parseInt(key)].y++;
				 activity.activityType = activity.activityType;
                 activity.activityDate = activity.activityDate;

                if(activity.activityType == 'emailOpen')
                {
                     activity.image = "/img/emailOpen.PNG";
                }
                else if(activity.activityType == 'emailSend')
                {
                      activity.image = "/img/emailSends.PNG";
                }
                else if(activity.activityType == 'emailClickThrough')
                {
                        activity.image =  "/img/emailClicked.PNG";
                }
                else if(activity.activityType == 'webVisit')
                {
                         activity.image =   "/img/webVisited.PNG";
                }
                else if(activity.activityType == 'formSubmit')
                {
                        activity.image = "/img/formSubmition.PNG";
                }
                else if(activity.activityType == 'campaignMembership')
                {
                        activity.image =  "/img/externalActivities.PNG";
                }
                activity.details.forEach(function(details)
				{
                  if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.subjectLine = details[Object.keys(details)[1]];
                  }
                  else if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                  }

				  //activity.emailAddress = contact.emailAddress;
				 if(details[Object.keys(details)[0]] == 'EmailClickedThruLink')
				  {
					 activity.link = details[Object.keys(details)[1]];
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailOpen')
				  {
					activity.name = details[Object.keys(details)[1]];


					  $scope.emailOpend.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailSend')
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.emailSended.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailClickThrough')
				  {

					activity.name = details[Object.keys(details)[1]];
					$scope.emailClicked.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'URL' )
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.webVisited.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'FormName' )
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.formSubmits.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == "Collection")
				  {

				    activity.formSubmitionData = details[Object.keys(details)[1]];
					if(activity.formSubmitionData != null)
					  {
						  activity.formSubmitionData = activity.formSubmitionData.split("elqdelimiter");
					  }

				  }

                });
                $scope.latestActivites.push(activity);
				}
               });
			//});
	 }


     $scope.flagForaccountBasedActivity = true;
	 $scope.flagForcontactBasedActivity = true;
	 $scope.accountWidgetPresent = false;
	 $scope.contactWidgetPresent = false;
     $scope.getSecondaryData = function(widget)
	 {
       if(widget.type == 'list box')
       {
         if(widget.json.source == 'salesforce' && $scope.salesforceuser){
           widget.name = 'list - ' + widget.json.relationshipName;
           var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.childSObject, key: widget.json.objectname, value: $rootScope.secondarydata[widget.json.parent][widget.json.valuefield] };
           var queryfields = _.map(widget.json.fields, 'id');
           query.fields = queryfields.join(',');
           query.salesforcecontactId = $scope.salesforceuser.Id;
           Account.secondarydata(query, function(result){
             $scope.widgetdata[widget.id] = result.records;
           });
         }
         else if(widget.json.source == 'eloqua')
         {
           widget.name = widget.json.internalName;
           var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
           widget.json.params.forEach(function(param){
             query[param.name] = param.value;
           });
           Account.secondarydata(query, function(result){
             $scope.widgetdata[widget.id] = result;

           });
         }
       }
       else if (widget.type == 'count box')
       {
         widget.name = 'Count - ' + widget.json.relationshipName;
         var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.childSObject, key: widget.json.objectname, count: true, value: $rootScope.secondarydata[widget.json.parent][widget.json.valuefield]  };
         if(widget.json.source == 'salesforce' && $scope.salesforceuser){
           query.salesforcecontactId = $scope.salesforceuser.Id;
           Account.secondarydata(query, function(result){
             $scope.widgetdata[widget.id] = result.totalSize;
           });
         }
       }
       else if (widget.type == 'pie chart' || widget.type == 'line chart' || widget.type == 'bar chart') {
        if(widget.type == 'pie chart')
        {
          widget.name = widget.type + ' - ' + widget.json.fieldinfo.objname + " by " + widget.json.fieldinfo.name;
        } else {
          var fields = _.map(widget.json.fieldinfo, 'name');
          widget.name = widget.type + ' - ' + widget.json.fieldinfo[0].objname + " by " + fields.join(", ");
        }
         if(widget.json.source == 'salesforce' && $scope.salesforceuser)
         {
           if(widget.type == 'bar chart' || widget.type == 'line chart'){
             $scope.widgetdata[widget.id] = [];
             widget.json.options.chart.x = function(d){return d.expr0;};
             widget.json.options.chart.y = function(d){return d.expr1;};
             widget.json.options.chart.xAxis.axisLabel = 'Date';
             widget.json.options.chart.yAxis.axisLabel = 'Count';

             widget.json.fieldinfo.forEach(function(fieldinfo, index){
               var query = {id: $scope.accountId, source: widget.json.source, object: fieldinfo.objname, key: fieldinfo.objectname, value: $rootScope.secondarydata[fieldinfo.parent][fieldinfo.valuefield], groupby: fieldinfo.name, fieldtype: fieldinfo.type };
               query.interval = widget.json.interval;
               Account.chartdata(query, function(result){
                 if (widget.type == 'bar chart' || widget.type == 'line chart') {
                   $scope.widgetdata[widget.id][index] = {key: fieldinfo.name, values: result.records };
                 }
               });
             });
           } else {
             var query = {id: $scope.accountId, source: widget.json.source, object: widget.json.fieldinfo.objname, key: widget.json.fieldinfo.objectname, value: $rootScope.secondarydata[widget.json.fieldinfo.parent][widget.json.fieldinfo.valuefield] , groupby: widget.json.fieldinfo.name, fieldtype: widget.json.fieldinfo.type };
             Account.chartdata(query, function(result){
               if(widget.type == 'pie chart'){
                 $scope.widgetdata[widget.id] = result.records;
                 widget.json.options.chart.x = function(d){return d[widget.json.fieldinfo.name];};
                 widget.json.options.chart.y = function(d){return d.expr0;};
               }
             });
           }
         }
       }
       else if (widget.type == 'eloqua')
	   {
       if((widget.objectname == 'accountBasedLatestActivity' || widget.objectname == 'accountBasedActivityBreakdown' || widget.objectname == 'accountBasedActivityGraph' ) && $scope.flagForaccountBasedActivity)
		{
		   $scope.accountWidgetPresent = true;
		   $rootScope.loadingEloqua = true;
		   $scope.eloquaAccountDataOnLoad = [];
		   $rootScope.showDateFilter = true;
		   $scope.globalAccountWidget = widget;
		   $scope.latestActivites = [];
		   $scope.emailOpend = [];
		   $scope.emailSended = [];
		   $scope.emailClicked = [];
		   $scope.webVisited = [];
		   $scope.formSubmits = [];
		   $scope.campaignMember = [];

      var query = {
          id: $scope.accountId,
          contactId: $scope.contactId,
          source: widget.json.source,
          object: widget.json.urlkey,
          sfdcaccountId: $scope.urlquery.sfdcaccountId,
          byranking: $scope.urlquery.byranking
      };

      
	      Account.accountBasedContacts(query, function(result)
		  {
			$rootScope.loadingEloqua = true;
		    var totalContact =  result.elements.length;
			var count = 1;
		   result.elements.forEach( function(contact)
		   {
			$rootScope.eloquaActivityTypes.forEach(function(type,index)
			{
			  $rootScope.loadingEloqua = true;
			  var query = {id: $scope.accountId, contactId: contact.id, source: widget.json.source, object: widget.json.urlkey };
				 widget.json.params.forEach(function(param){
				 if(param.name == 'type'){
					query[param.name] = type;
				   } else if(param.name == 'fromdate'){
					   query[param.name] = $scope.fromdate;
				   }
				   else if(param.name == 'todate')
				   {
					 query[param.name]= $scope.todate;
				   }
				 });
				 $rootScope.loadingEloqua = true;
			 Account.accountBasedAnalytics(query, function(response)
			 {

				//console.log(count++ , '  ' , totalContact*6);
				if(count++ == totalContact*6)
				{
					$rootScope.loadingEloqua = false;
				}
				else{
					console.log('loading...');
				}
               response.result.forEach(function(activity)
               {
				 activity.index = index;
				 $scope.eloquaAccountDataOnLoad.push(activity);
                 var key = new Date(activity.activityDate*1000).getMonth();
		         $scope.accountActivityGraph[index].values[parseInt(key)].y++;
				 activity.activityType = activity.activityType;
                 activity.activityDate = activity.activityDate;
                if(activity.activityType == 'emailOpen')
                {
                     activity.image = "/img/emailOpen.PNG";
                }
                else if(activity.activityType == 'emailSend')
                {
                      activity.image = "/img/emailSends.PNG";
                }
                else if(activity.activityType == 'emailClickThrough')
                {
                        activity.image =  "/img/emailClicked.PNG";
                }
                else if(activity.activityType == 'webVisit')
                {
                         activity.image =   "/img/webVisited.PNG";
                }
                else if(activity.activityType == 'formSubmit')
                {
                        activity.image = "/img/formSubmition.PNG";
                }
                else if(activity.activityType == 'campaignMembership')
                {
                        activity.image =  "/img/externalActivities.PNG";
                }
                activity.details.forEach(function(details)
				{
                  if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.subjectLine = details[Object.keys(details)[1]];
                  }
                  else if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                  }

				  activity.emailAddress = contact.emailAddress;
				 if(details[Object.keys(details)[0]] == 'EmailClickedThruLink')
				  {
					 activity.link = details[Object.keys(details)[1]];
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailOpen')
				  {
					activity.name = details[Object.keys(details)[1]];


					  $scope.emailOpend.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailSend')
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.emailSended.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailClickThrough')
				  {

					activity.name = details[Object.keys(details)[1]];
					$scope.emailClicked.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'URL' )
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.webVisited.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'FormName' )
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.formSubmits.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == "Collection")
				  {
					  activity.formSubmitionData = details[Object.keys(details)[1]];
					  if(activity.formSubmitionData != null)
					  {
					     activity.formSubmitionData = activity.formSubmitionData.split("elqdelimiter");
					  }

				  }

                });
                $scope.latestActivites.push(activity);
               });
              });

			 });

			});
		   });

		   $scope.flagForaccountBasedActivity = false;
		  }
		else if ( (widget.json.internalName == 'Activity Breakdown' || widget.json.internalName == 'Activities Graph'
		  || widget.json.internalName == 'Latest Activities' && $scope.flagForcontactBasedActivity) && (widget.objectname != 'accountBasedLatestActivity' && widget.objectname != 'accountBasedActivityBreakdown' && widget.objectname != 'accountBasedActivityGraph'))
		  {
		     $scope.contactWidgetPresent = true;
		   widget.name = widget.name;
		   $rootScope.showDateFilter = true;
		   widget.name = widget.name;
		   $scope.globalContactWidget = widget;
		   $scope.eloquaContactDataOnLoad = [];
           $scope.EmailOpenContact = [];
		   $scope.EmailSendContact = [];
		   $scope.EmailClickContact = [];
		   $scope.FormSubmitContact = [];
		   $scope.WebVisitContact = [];
		   $scope.CampainMemberContact = [];
		   $scope.latestActivitesContact = [];
		   $scope.contactActivityGraph = [];


           $rootScope.eloquaActivityTypes.forEach(function(type , index)
		   {
			   // var startDate1 = startDate;
			   // var endDate1 = endDate;

			   // console.log(startDate , endDate);
			   // for(var i = 0 ; startDate1 < endDate1 ; i++)
			    // {
				 // console.log(monthdata);
				  // monthdata[i] = { x: month[startDate1.getMonth()], y: 0 };
				  // startDate1.setMonth(startDate1.getMonth()+1);
			    // }

			     var monthdata = new Array();

				 monthdata[0] = { x: "Jan", y: 0};
				 monthdata[1] = { x:"Feb", y: 0 };
				 monthdata[2] = { x: "Mar", y: 0 };
				 monthdata[3] = { x:"Apr", y: 0 };
				 monthdata[4] = { x:"May", y: 0 };
				 monthdata[5] = { x:"Jun", y: 0 };
				 monthdata[6] = { x:"Jul", y: 0 };
				 monthdata[7] = { x:"Aug", y: 0 };
				 monthdata[8] = { x:"Sep", y: 0 };
				 monthdata[9] = { x:"Oct", y: 0 };
				 monthdata[10] = { x:"Nov", y: 0 };
				 monthdata[11] = { x:"Dec", y: 0 };
				 $scope.contactActivityGraph.push({key: type, values: monthdata });

				 var query = {id: $scope.accountId, contactId: $scope.contactId, source: widget.json.source, object: widget.json.urlkey };
				 widget.json.params.forEach(function(param){
				   if(param.name == 'type'){
					 query[param.name] = type;
				   } else if(param.name == 'fromdate'){
					   query[param.name] = $scope.fromdate;
				   }
				   else if(param.name == 'todate')
				   {
					 query[param.name]= $scope.todate;
				   }

				 });
             Account.secondarydata(query, function(result)
			 {
			   result.result.forEach(function(activity)
                {
				 activity.index = index;
				 $scope.eloquaContactDataOnLoad.push(activity);
				 var key = new Date(activity.activityDate*1000).getMonth();
				 $scope.contactActivityGraph[index].values[parseInt(key)].y++;

				 activity.activityType = activity.activityType;
                 activity.activityDate = activity.activityDate;
                if(activity.activityType == 'emailOpen')
                {
                     activity.image = "/img/emailOpen.PNG";
                }
                else if(activity.activityType == 'emailSend')
                {
                      activity.image = "/img/emailSends.PNG";
                }
                else if(activity.activityType == 'emailClickThrough')
                {
                        activity.image =  "/img/emailClicked.PNG";
                }
                else if(activity.activityType == 'webVisit')
                {
                         activity.image =   "/img/webVisited.PNG";
                }
                else if(activity.activityType == 'formSubmit')
                {
                        activity.image = "/img/formSubmition.PNG";
                }
                else if(activity.activityType == 'campaignMembership')
                {
                        activity.image =  "/img/externalActivities.PNG";
                }
                activity.details.forEach(function(details)
				{
                  if(details[Object.keys(details)[0]] == 'SubjectLine' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.subjectLine = details[Object.keys(details)[1]];
                  }
                  else if(details[Object.keys(details)[0]] == 'EmailName' || details[Object.keys(details)[0]] == 'FormName'
                    || details[Object.keys(details)[0]] == 'URL')
                  {
                    activity.name = details[Object.keys(details)[1]];
                  }

				  activity.emailAddress = activity.emailAddress;
				 if(details[Object.keys(details)[0]] == 'EmailClickedThruLink')
				  {
					 activity.link = details[Object.keys(details)[1]];
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailOpen')
				  {
					activity.name = details[Object.keys(details)[1]];


					  $scope.EmailOpenContact.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailSend')
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.EmailSendContact.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'EmailName' && activity.activityType == 'emailClickThrough')
				  {

					activity.name = details[Object.keys(details)[1]];
					$scope.EmailClickContact.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'URL' )
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.WebVisitContact.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == 'FormName' )
				  {
					activity.name = details[Object.keys(details)[1]];
					$scope.FormSubmitContact.push(activity);
				  }
				  if(details[Object.keys(details)[0]] == "Collection")
				  {
					  activity.formSubmitionData = details[Object.keys(details)[1]];
					  activity.formSubmitionData = activity.formSubmitionData.split("elqdelimiter");
				  }

                });
                $scope.latestActivitesContact.push(activity);
               });
			   console.log('contactActivityGraph ' , $scope.contactActivityGraph);
              });

             });
            $scope.flagForcontactBasedActivity = false;
         }
       }
     }

	 $scope.getuser($scope.contactId || $scope.email);
      $scope.addcolumn = function(tabindex){
        $rootScope.column = {};
        var modalInstance = $uibModal.open({
                        templateUrl: 'views/partial/addcolumn.html',
                        size: 'md',
                        controller : 'AddColumnCtrl'
                    });
       }



       $scope.addtab = function(){
         $rootScope.tab = {};
         var modalInstance = $uibModal.open({
                         templateUrl: 'views/partial/addtab.html',
                         size: 'md',
                         controller : 'AddTabCtrl'
                     });
        }


       $scope.removecolumn = function(index){
         Widget.removeById({id: $rootScope.alltabs[$rootScope.tabindex].widgets[index].id}, function(removed){
           $rootScope.alltabs[$rootScope.tabindex].widgets.splice(index, 1);
         });
       };

       $scope.$on('angular-resizable.resizeEnd', function(e, args){
         if(args.width){
           $rootScope.alltabs[$rootScope.tabindex].widgets[args.id].width = args.width;
         }
         if(args.height){
           $rootScope.alltabs[$rootScope.tabindex].widgets[args.id].height = args.height;
         }
         $rootScope.saveWidget($rootScope.alltabs[$rootScope.tabindex].widgets[args.id]);
       });

       $scope.dropped = function(index, item, external, type, tabindex){
         item.width = 400;
         item.height = 200;
         item.name = 'untitled';
         item.json = {};

         if(item.type == 'group box 1'){
          item.name = 'Group box one column';
           item.json.properties = [[]];
           $rootScope.saveWidget(item);
         }else if(item.type == 'group box 2'){
          item.name = 'Group box two column';
           item.json.properties = [[],[]];
           $rootScope.saveWidget(item);
         }else if (item.type == 'group box 3') {
           item.name =  'Group box three column';
           item.json.properties = [[],[],[]];
           $rootScope.saveWidget(item);
         } else if(item.type == 'list box') {
           item.name =  'List box';
           $rootScope.saveWidget(item);
         }
         else if (item.type == 'eloqua') {
          item.width = 600;
         item.height = 500;
           item.name = item.internalName;
           item.json = {
             internalName: item.internalName,
             urlkey: item.urlkey,
             params: item.params,
             source: item.type
           };
           if(item.internalName == 'Activities Graph'){
			   item.width = 800;
               item.height = 400;
             item.json.interval = 'month';
             item.json.options = {
               chart: {
                   type: 'multiBarChart',
                   height: 280,
                   margin : {
                       top: .1,
                       right: 20,
                       bottom: 100,
                       left: 45
                   },
				  showControls:false,
				   legend:
					{
					  margin:
					  {
						top: 110
					  },

					},
                   clipEdge: true,
                   duration: 500,
                   stacked: true,
				   reduceXTicks:false,
                   xAxis: {
                       axisLabel: '',
                       showMaxMin: false
                   },
                   yAxis: {
                       axisLabel: '',
                       axisLabelDistance: -40
                   },

                },


			 }
           }
		   console.log('item ' , item);
           $rootScope.saveWidget(item);
         }
         else if (item.type == 'count box') {
           item.name =  'Count box';
           $rootScope.saveWidget(item);
         } if (item.type == 'line chart'){
               item.name =  'Line chart';
               item.json = {};
               item.json.interval = 'year';
               item.json.options = {
                       chart: {
                           type: 'lineChart',
                           height: 450,
                           margin : {
                               top: 20,
                               right: 20,
                               bottom: 40,
                               left: 55
                           },

                           xAxis: {
                               axisLabel: ''
                           },
                           yAxis: {
                               axisLabel: '',
                               axisLabelDistance: -10
                           }
                       },
                       title: {
                           enable: true,
                           text: ''
                       }
                   };
                   $rootScope.saveWidget(item);
             } else if (item.type == 'bar chart') {
                 item.name =  'Bar chart';
                 item.json = {};
                 item.json.interval = 'year';
                 item.json.options = {
                   chart: {
                       type: 'multiBarChart',
                       height: 450,
                       margin : {
                           top: 20,
                           right: 20,
                           bottom: 45,
                           left: 45
                       },
                       clipEdge: true,
                       duration: 500,
                       stacked: true,
                       xAxis: {
                           axisLabel: '',
                           showMaxMin: false
                       },
                       yAxis: {
                           axisLabel: '',
                           axisLabelDistance: -20
                       }
                   }
               };
               $rootScope.saveWidget(item);
             } else if (item.type == 'pie chart') {
               item.name =  'Pie chart';
               item.json = {};
               item.json.options =   {
                   chart: {
                       type: 'pieChart',
                       height: 500,
                       showLabels: true,
                       duration: 5,
                       labelThreshold: 0.01,
                       labelSunbeamLayout: true,
                       xAxis: {
                           axisLabel: ''
                       },
                       yAxis: {
                           axisLabel: ''
                       },
                       legend: {
                           margin: {
                               top: 5,
                               right: 35,
                               bottom: 5,
                               left: 0
                           }
                       }
                   }
               };
               $rootScope.saveWidget(item);
             } else if (item.type == 'tab') {
               var tab = {
                 name: "Tab",
                 description: "untitled"
               };
               $rootScope.savetab(tab);
             }
       }

	  $scope.droppedontabset = function(index, item, external, type, tabindex){
	  if (item.type == 'tab') {
			  var tab = {
				name: "Tab",
				description: "untitled"
			  };
			  $rootScope.savetab(tab);
			}
	  }

       $scope.droppedOnWidget = function(index, property, external, type, widget, subcolumnindex){
         if(widget.type == 'group box 2' || widget.type == 'group box 3' || widget.type == 'group box 1'){
           if(property.internalName){
             var internameArray = property.internalName.split('_');
             if(internameArray && internameArray[1]){
              property.internalName = internameArray[1].substring(0,1).toLowerCase()+internameArray[1].substring(1);
             }
             property.source = "eloqua";
             var data = {property: property, width: 30, height: 30};
             widget.json.properties[subcolumnindex].push(data);
           } else {
             property.source = "salesforce";
             var data = {property: property, width: 30, height: 30 };
             widget.json.properties[subcolumnindex].push(data);
             widget.json.properties.forEach(function(item){
               item.forEach(function(property){
                 $scope.getfielddata(property.property);
               });
             });
           }
         } else if (widget.type == 'count box') {
           if(property.internalName){
             property.source = "eloqua";
           } else {
             property.source = "salesforce";
           }
           widget.json = property;
           $scope.getSecondaryData(widget);
         } else if (widget.type == 'list box') {
           if(property.internalName){
             property.source = "eloqua";
           } else {
             property.source = "salesforce";
             property.fields = [$rootScope.salesforceobjectfields[property.childSObject][0],$rootScope.salesforceobjectfields[property.childSObject][1],$rootScope.salesforceobjectfields[property.childSObject][2]];
           }
           widget.json = property;
           $scope.getSecondaryData(widget);
         } else if (widget.type == 'pie chart' || widget.type == 'line chart' || widget.type == 'bar chart') {
            widget.json.source = property.source;
            if(widget.type == 'line chart' || widget.type == 'bar chart'){
              if(widget.json.fieldinfo){
                widget.json.fieldinfo.push(property);
              } else {
                widget.json.fieldinfo = [property];
              }
            } else {
              widget.json.fieldinfo = property;
            }
            $scope.getSecondaryData(widget);
         }
         $rootScope.saveWidget(widget);
       }
  });
