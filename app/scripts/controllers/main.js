'use strict';

/**
 * @ngdoc function
 * @name profilerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the profilerApp
 */
angular.module('profilerApp')
  .controller('MainCtrl', function ($scope, $rootScope, $uibModal, $uibModalStack, Account, $state, $stateParams, Settings , $mdSidenav, $location) {
   $scope.toggleLeft = buildToggler('left');
   $scope.toggleRight = buildToggler('right');
   $scope.toggleMain = buildToggler('mainView');

    $scope.resetdefault = function(){
        console.log("in main.js ")
        $scope.$broadcast ('resetdefault');

    }

     $scope.onDragstart = function(list, event) {
           var img = new Image();
         img.src = "images/groupbox1_chart.svg";
         event.dataTransfer.setDragImage(img, 0, 0);
    };
    function buildToggler(componentId) {
      return function() {
        $mdSidenav(componentId).toggle();
      };
    }
    $scope.expand = function (scope) {
      scope.expand();
    };
    $scope.collapse = function (scope) {
      console.log('collapse');
      scope.collapse();
    };

    $scope.showList = false;
     $scope.contactOpen = true;
     $scope.toggleList = function () {
       $scope.showList = !$scope.showList;
       $scope.contactOpen = !$scope.contactOpen;
     }



     $scope.clickItem = function(node) {
       node.open = !node.open;
      }

     $scope.edit = false;
     $scope.delete = false;
     $scope.deletePanel = false;
     $scope.editName = false;

     $scope.showHideIcon = function () {

        $scope.delete = !$scope.delete;
        $scope.deletePanel = !$scope.deletePanel;
        $scope.editName  = !$scope.editName;
     }
    $scope.collapseAll = function () {
  $scope.$broadcast('angular-ui-tree:collapse-all');
};

$scope.expandAll = function () {
  $scope.$broadcast('angular-ui-tree:expand-all');
};
    $scope.urlquery = $location.search();
    $scope.contactId = $stateParams.id;
    $scope.accountId = $stateParams.accountId;
    $scope.userId = $scope.urlquery.userId;
    $scope.siteId = $scope.urlquery.siteId;
    $scope.sortableOptions = {

     };
});
