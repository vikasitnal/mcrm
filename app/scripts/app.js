'use strict';

/**
 * @ngdoc overview
 * @name profilerApp
 * @description
 * # profilerApp
 *
 * Main module of the application.
 */
angular
  .module('profilerApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap',
    'ui.sortable',
    'angularResizable',
    'nvd3',
    'ng-mfb',
    'lbServices',
    'dndLists',
    'angularjs-dropdown-multiselect',
    'ui.tree',
    'ng-scroll-bar',
    'ngMaterial',
    'ngMessages',
    'md.data.table',
    'ngJSONPath',
	'gm.datepickerMultiSelect',
	'angular.drag.resize'

  ]).filter('capitalize', function() {
  return function(input, scope) {
    if (input!=null){
      input = input.toLowerCase();
      return input.substring(0,1).toUpperCase()+input.substring(1);
    }
  }
})
  .config(function ($routeProvider, $stateProvider, $urlRouterProvider, $mdAriaProvider ) {
    $mdAriaProvider.disableWarnings();

      $urlRouterProvider.otherwise('/?page=1&query=&siteId=10&userId=1');
      $stateProvider
          .state('home', {
              url: '/home/{id}/{accountId}',
              templateUrl: 'views/main.html',
              controller:'MainCtrl',
              controllerAs: 'main'
          })
          .state('admin', {
              url: '/admin?siteId&userId',
              templateUrl: 'views/admin.html',
              controller:'AdminCtrl',
              controllerAs: 'admin'
          })
          .state('home.tabs', {
              url: '/tabs?siteId&userId&type',
              templateUrl: 'views/tabs.html',
              controller:'TabsCtrl',
              controllerAs: 'tabs'
          })
          .state('search', {
              url: '/?query&page&siteId&userId',
              templateUrl: 'views/search.html',
              controller:'SearchCtrl',
              controllerAs: 'search'
          });


  })
  .run(function($rootScope, $state, Settings, Tab, Widget) {
     $rootScope.search = function(query, page){
       console.log(query, page);
      $state.go('search', {query: query, page: page});
     }

     $rootScope.saveSettings = function(){
        Settings.prototype$updateAttributes($rootScope.settings.id, $rootScope.settings, function(settings){
          $rootScope.settings = settings;
        });
     }

     $rootScope.savetab = function(tab){
      console.log("save tab")
       if(tab.id)
       {

       Tab.prototype$updateAttributes(tab.id,tab , function(tab){
       });
       }
       else
       {
          tab.settingsId = $rootScope.settings.id;
       Tab.create(tab, function(tab){
         tab.widgets = [];
         $rootScope.alltabs.push(tab);
       });
       }

     }

     $rootScope.saveWidget = function(widget){
       if(widget.id){
         Widget.prototype$updateAttributes(widget.id, widget, function(widget){
           console.log('widget saved');
         });
       } else {
         widget.tabId = $rootScope.alltabs[$rootScope.tabindex].id;
         widget.order = $rootScope.alltabs[$rootScope.tabindex].widgets.length;
         Widget.create(widget, function(widget){
           $rootScope.alltabs[$rootScope.tabindex].widgets.push(widget);
         });
       }
     }


  });
